.. _pep 8: https://www.python.org/dev/peps/pep-0008/
.. _typing: https://docs.python.org/3/library/typing.html
.. _pep 484: https://www.python.org/dev/peps/pep-0484/
.. _sphinx: http://www.sphinx-doc.org/en/stable/
.. _google vs numpy: http://www.sphinx-doc.org/en/stable/ext/napoleon.html#google-vs-numpy

============
Contributing
============

Recommended development setup
-----------------------------

Using a **dedicated Python development environment**, such as ``pyus-dev`` is
more than encouraged.
It can easily be set up with ``virtualenv`` (or using Anaconda code).

The package can be set up for local development with the following::

    $ git clone https://gitlab.com/pyus/pyus.git
    $ pip install --upgrade -e pyus

**Note:** the ``-e`` will install the ``pyus`` package as a symlink.
The package will be editable and the changes to the source files will be
immediately available to the package on our system.
Instead of installing ``pyus`` in your Python environment, this will create a
``pyus.egg-link`` file (symbolic link) in your Python environment as well as a
``pyus.egg-link`` folder under ``pyus``.

You can also install it with extra packages (see ``setup.py`` for more
details)::

    $ pip install --upgrade -e pyus[doc,pkg,dev]

Make sure to sync with remote ``develop`` branch (you need to be under
``pyus/`` root directory)::

    $ git pull origin develop

Then create a new branch, e.g. ``feature/my_new_branch``, from ``develop``::

    $ git checkout -b feature/my_new_branch develop

Push changes from your commit into your branch (see `commit guidelines`_)::

    $ git push origin feature/my_new_branch

Once you feel that you are done with your contribution, open a
`Merge Request <https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.
html>`_ using the GitLab web interface from ``feature/my_new_branch`` to
``develop``.

Notes:
    - update README, history, doc & co
    - add some examples

Commit guidelines
-----------------
No explicit commit guidelines are set so far.
However the commit summary should start with a verb (present tense).

The `SciPy commit message guide <https://docs.scipy.org/doc/numpy/dev/gitwash/
development_workflow.html#writing-the-commit-message>`_ could be a good start.

Coding conventions
------------------
PyUS follows the standard Python guidelines for code style, `PEP 8`_.
Most IDEs and text editors have settings that can help you follow `PEP 8`_, for
example by translating tabs by four spaces.

Concerning the line length and the 79 characters rule (`PEP 8`_), check the
`Kenneth Reitz's Code Style™ <http://docs.python-requests.org/en/latest/dev/
contributing/#kenneth-reitz-s-code-style>`_.

You can use ``pycodestyle`` and/or ``pyflakes`` packages to check that your
code is aaa compliant to `PEP 8`_.

Documentation
-------------
We try to use the Python typing_ module as much as possible.
Details can be found in the `PEP 484`_.

Still to decide whether will use `Google <http://google.github.io/styleguide/
pyguide.html?showone=Comments#Comments>`_ or `NumPy <https://github.com/numpy/
numpy/blob/master/doc/HOWTO_DOCUMENT.rst.txt>`_ docstrings: `Google vs NumPy`_

See also:
    - `Example Google Style Python Docstrings <http://www.sphinx-doc.org/en/stable/ext/example_google.html#example-google>`_
    - `Example NumPy Style Python Docstrings <http://www.sphinx-doc.org/en/stable/ext/example_numpy.html#example-numpy>`_

Sphinx_ seems to support type hints and type annotations (`Type Annotations
<http://www.sphinx-doc.org/en/stable/ext/napoleon.html#type-annotations>`_).
Hence you should not duplicate the argument type in docstrings.

Google docstrings examples with and without `PEP 484`_ type annotations::

    def function_with_types_in_docstring(param1, param2):
        """Example function with types documented in the docstring.

        `PEP 484`_ type annotations are supported. If attribute, parameter, and
        return types are annotated according to `PEP 484`_, they do not need to be
        included in the docstring:

        Args:
            param1 (int): The first parameter.
            param2 (str): The second parameter.

        Returns:
            bool: The return value. True for success, False otherwise.

        .. _PEP 484:
            https://www.python.org/dev/peps/pep-0484/

        """

    def function_with_pep484_type_annotations(param1: int, param2: str) -> bool:
        """Example function with PEP 484 type annotations.

        Args:
            param1: The first parameter.
            param2: The second parameter.

        Returns:
            The return value. True for success, False otherwise.

        """

Numpy docstrings examples with and without `PEP 484`_ type annotations.
Note that the return type must be duplicated in the docstring when using type
annotations::

    def function_with_types_in_docstring(param1, param2):
        """Example function with types documented in the docstring.

        `PEP 484`_ type annotations are supported. If attribute, parameter, and
        return types are annotated according to `PEP 484`_, they do not need to be
        included in the docstring:

        Parameters
        ----------
        param1 : int
            The first parameter.
        param2 : str
            The second parameter.

        Returns
        -------
        bool
            True if successful, False otherwise.

        .. _PEP 484:
            https://www.python.org/dev/peps/pep-0484/

        """

    def function_with_pep484_type_annotations(param1: int, param2: str) -> bool:
        """Example function with PEP 484 type annotations.

        The return type must be duplicated in the docstring to comply
        with the NumPy docstring style.

        Parameters
        ----------
        param1
            The first parameter.
        param2
            The second parameter.

        Returns
        -------
        bool
            True if successful, False otherwise.

        """

In PyCharm, you can set the docstring format (Plain, Epytext, reStructuredText,
NumPy, Google) in::

    File --> Settings --> Tools --> Python Integrated Tools

For converting and/or generating docstrings, see
`Pyment <https://github.com/dadadel/pyment>`_.

The ``restview`` Python package can be used for a live browser ``.rst``
renderer.

Few notes for sphinx_ set-up:
    - doc/Makefile and doc/conf.py using the ``sphinx-quickstart``
    - **Important to check**:
        - `Autodoc <http://www.sphinx-doc.org/en/stable/ext/autodoc.html#module-sphinx.ext.autodoc>`_
        - Automatic API documentation ``sphinx-apidoc``: see `Invocation of sphinx-apidoc <http://www.sphinx-doc.org/en/stable/invocation.html#invocation-apidoc>`_
    - Check ``numpydoc`` (pygsp) or ``sphinx.ext.napoleon``
    - Examples: ``tqdm``, ``pygsp``, ``requests``, ``scipy``, ``numpy``, etc.

Making a release
----------------
**TODO:** see `PyGSP - Making a release <https://github.com/epfl-lts2/pygsp/
blob/master/CONTRIBUTING.rst#making-a-release>`_

Tests
-----
**TODO**
