#!/usr/bin/env python3
from pyus.probe import L11Probe

probe = L11Probe()

print('Some probe details')
print('  Name: ', probe.name)
print('  Center frequency [Hz]: ', probe.center_frequency)
print('  Element number [─]: ', probe.element_number)
print('  Bandwidth [%]: ', probe.bandwidth)
