#!/usr/bin/env python3
import os
import pyus.utils.picmus

# PICMUS data settings
#   Enter a suitable path for downloading PICMUS data
data_path = os.path.join(os.pardir, os.pardir, 'datasets', 'picmus17')
signal_selection = ['rf']
pht_selection = ['in_vitro_type1']
transmission_selection = ['transmission_1']
pw_number_selection = [1, 3]

# Download data
pyus.utils.picmus.download_2017(
    export_path=data_path, signal_selection=signal_selection,
    pht_selection=pht_selection, transmission_selection=transmission_selection,
    pw_number_selection=pw_number_selection
)
