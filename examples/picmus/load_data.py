#!/usr/bin/env python3
import os
import pyus.utils.picmus

# PICMUS data settings
data_path = os.path.join(os.pardir, os.pardir, 'datasets', 'picmus17')
dataset_name = 'dataset_rf_in_vitro_type1_transmission_1_nbPW_3.hdf5'

# Load data
data, settings = pyus.utils.picmus.load_data(
    path=os.path.join(data_path, dataset_name)
)

# Outputs
data_shape = data.shape
print('Data shape: ', data.shape)
print('  Frame number: ', data_shape[0])
print('  Event number: ', data_shape[1])
print('  Channel number: ', data_shape[2])
print('  Sample number: ', data_shape[3])
print('PICMUS settings')
for key, val in settings.items():
    print('  {}: {}'.format(key, val))
