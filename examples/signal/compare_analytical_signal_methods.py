#!/usr/bin/env python3
import os
import numpy as np
import matplotlib.pyplot as plt

import pyus.utils.picmus
from pyus.signal.hilbert import PyHilbertFIR, PyHilbertFFT

###############################################################################
# Load some data
###############################################################################
data_path = os.path.join(os.pardir, os.pardir, 'datasets', 'picmus17')
dataset_name = 'dataset_rf_in_vitro_type1_transmission_1_nbPW_1.hdf5'
data, settings = pyus.utils.picmus.load_data(
    path=os.path.join(data_path, dataset_name), dtype=np.float32
)
dtype = data.dtype

###############################################################################
# Create Hilbert objects
###############################################################################
data_shape = data.shape
hilbert_fir = PyHilbertFIR(data_shape=data_shape, filter_size=21, dtype=dtype)
hilbert_fft = PyHilbertFFT(data_shape=data_shape, dtype=dtype)

###############################################################################
# Compute analytical signals
###############################################################################
data_iq_fir = hilbert_fir(data)
data_iq_fft = hilbert_fft(data)

###############################################################################
# Plots
###############################################################################
elem_ind = 64

sig_fir = data_iq_fir.squeeze()[elem_ind]
sig_fft = data_iq_fft.squeeze()[elem_ind]
env_fir = np.abs(sig_fir)
env_fft = np.abs(sig_fft)

# Compare real and imag parts
fig = plt.figure()
ax1 = fig.add_subplot(211)
ax1.plot(sig_fir.real, label='FIR')
ax1.plot(sig_fft.real, label='FFT')
ax1.legend()
ax1.grid()
ax1.set_title('Real part')
ax2 = fig.add_subplot(212, sharex=ax1)
ax2.plot(np.abs(sig_fir.real - sig_fft.real))
ax2.grid()
ax2.set_title('Error')

fig = plt.figure()
ax1 = fig.add_subplot(211)
ax1.plot(sig_fir.imag, label='FIR')
ax1.plot(sig_fft.imag, label='FFT')
ax1.legend()
ax1.grid()
ax1.set_title('Imaginary part')
ax2 = fig.add_subplot(212, sharex=ax1)
ax2.plot(np.abs(sig_fir.imag - sig_fft.imag))
ax2.grid()
ax2.set_title('Error')

# Compare resulting envelopes
fig = plt.figure()
ax1 = fig.add_subplot(211)
ax1.plot(env_fir, label='FIR')
ax1.plot(env_fft, label='FFT')
ax1.legend()
ax1.grid()
ax1.set_title('Envelope')
ax2 = fig.add_subplot(212, sharex=ax1)
ax2.plot(np.abs(env_fir - env_fft))
ax2.grid()
ax2.set_title('Error')

plt.show()
