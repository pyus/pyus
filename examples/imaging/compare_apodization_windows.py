#!/usr/bin/env python3
import time

import matplotlib.pyplot as plt
import numpy as np
from pyus.probe import L11Probe
from pyus.imaging.windows import Selfridge2D, Tukey, Hanning, Hamming, Boxcar
from pyus.utils.grid import grid_interval

###############################################################################
# Parameters
###############################################################################
dtype = np.float32
probe = L11Probe()
mean_sound_speed = 1540.
selected_element = 64
f_number = 1.75  # as selected by PICMUS
x_min, x_max = (-probe.width / 2, +probe.width / 2)
y_min, y_max = (0., 0.)
# z_min, z_max = (0, probe.width)
z_min, z_max = (0, 5e-2)

###############################################################################
# Create domain grid
###############################################################################
wavelength = mean_sound_speed / probe.center_frequency

#   Grid domain axes
step = 0.25 * wavelength
x_axis = grid_interval(start=x_min, stop=x_max, step=step, dtype=dtype)
y_axis = grid_interval(start=y_min, stop=y_max, step=step, dtype=dtype)
z_axis = grid_interval(start=z_min, stop=z_max, step=step, dtype=dtype)

#   Domain meshgrid
# x_mg, y_mg, z_mg = np.meshgrid(x_axis, y_axis, z_axis, indexing='ij')
# im_pos = np.stack([x_mg, y_mg, z_mg])
image_axes = (x_axis, y_axis, z_axis)
image_axes = tuple(ax.astype(dtype=dtype) for ax in image_axes)
image_shape = tuple(ax.size for ax in image_axes)
im_mg = np.array(np.meshgrid(*image_axes, indexing='ij'))
im_pos = im_mg.reshape((3, np.prod(image_shape)))

###############################################################################
# Create apodization windows
###############################################################################
# Standard apodization windows
hanning_win = Hanning(f_number=f_number, dtype=dtype)
hamming_win = Hamming(f_number=f_number, dtype=dtype)
boxcar_win = Boxcar(f_number=f_number, dtype=dtype)
tukey25_win = Tukey(f_number=f_number, dtype=dtype)  # by default alpha=0.25
tukey50_win = Tukey(f_number=f_number, alpha=0.5, dtype=dtype)

# Obliquity apodization
selfr2d_win = Selfridge2D(
    element_width=probe.element_width, wavelength=wavelength, dtype=dtype
)

###############################################################################
# Compute standard apodizations with same F-number and baffle 2D
###############################################################################
#   Element coordinates
el_pos = np.float32(probe.element_positions[selected_element])[:, np.newaxis]

# Compute apodization
# Reshape because now input is (3,N) so output is (N) and must be reshaped
boxcar_apod = boxcar_win(im_pos=im_pos, el_pos=el_pos).reshape(image_shape)
hanning_apod = hanning_win(im_pos=im_pos, el_pos=el_pos).reshape(image_shape)
hamming_apod = hamming_win(im_pos=im_pos, el_pos=el_pos).reshape(image_shape)
tukey25_apod = tukey25_win(im_pos=im_pos, el_pos=el_pos).reshape(image_shape)
tukey50_apod = tukey50_win(im_pos=im_pos, el_pos=el_pos).reshape(image_shape)
selfr2d_apod = selfr2d_win(im_pos=im_pos, el_pos=el_pos).reshape(image_shape)

boxcar_apod_cpp = boxcar_win._cy_call(im_pos=im_pos, el_pos=el_pos).reshape(image_shape)
hanning_apod_cpp = hanning_win._cy_call(im_pos=im_pos, el_pos=el_pos).reshape(image_shape)
hamming_apod_cpp = hamming_win._cy_call(im_pos=im_pos, el_pos=el_pos).reshape(image_shape)
tukey25_apod_cpp = tukey25_win._cy_call(im_pos=im_pos, el_pos=el_pos).reshape(image_shape)
tukey50_apod_cpp = tukey50_win._cy_call(im_pos=im_pos, el_pos=el_pos).reshape(image_shape)
selfr2d_apod_cpp = selfr2d_win._cy_call(im_pos=im_pos, el_pos=el_pos).reshape(image_shape)

# start_time = time.time()
# boxcar_apod_py = boxcar_win(im_pos=im_pos, el_pos=el_pos)
# print('elapsed time = {}'.format(time.time() - start_time))
#
# start_time = time.time()
# boxcar_apod_cpp = boxcar_win._cy_call(im_pos=im_pos, el_pos=el_pos)
# print('elapsed time = {}'.format(time.time() - start_time))

print(np.allclose(boxcar_apod, boxcar_apod_cpp))
print(np.allclose(hanning_apod, hanning_apod_cpp))
print(np.allclose(hamming_apod, hamming_apod_cpp))
print(np.allclose(tukey25_apod, tukey25_apod_cpp))
print(np.allclose(tukey50_apod, tukey50_apod_cpp))
print(np.allclose(selfr2d_apod, selfr2d_apod_cpp))

###############################################################################
# Plots
###############################################################################
# Figure 1: apodization maps
FIG_SIZE = (10, 10/1.6)
axis_scale = 1e3
extent = [x_min, x_max, z_max, z_min]
extent = [val * axis_scale for val in extent]
_apod_list = [
    boxcar_apod, hanning_apod, hamming_apod, tukey25_apod, tukey50_apod,
    selfr2d_apod
]
apod_list = [a.squeeze() for a in _apod_list]
fnum_str = 'F# {}'.format(f_number)
apod_names = [
    'Boxcar ' + fnum_str, 'Hanning ' + fnum_str, 'Hamming ' + fnum_str,
    'Tukey (25%) ' + fnum_str, 'Tukey (50%) ' + fnum_str, 'Sefridge 2D'
]
fig, axes = plt.subplots(
    nrows=2, ncols=3, sharex=True, sharey=True, figsize=FIG_SIZE
)
for ax, apod, name in zip(axes.ravel(), apod_list, apod_names):
    ax.imshow(apod.T, extent=extent, interpolation=None)
    ax.set_title(name)
#   Axes labeling
xlabel, ylabel = ('x [mm]', 'z [mm]')
for row_axes in axes:
    row_axes[0].set_ylabel(ylabel)
for ax in axes[-1]:
    ax.set_xlabel(xlabel)
fig.tight_layout()

# Figure 2: apodization at specific depths
depth_indexes = (np.array([1/3, 2/3]) * (z_max - z_min) / step).astype(np.int)
fig, axes = plt.subplots(
    nrows=depth_indexes.size, ncols=1, sharex=True, sharey=True,
    figsize=FIG_SIZE
)
for ax, ind in zip(axes.ravel(), depth_indexes):
    for apod, name in zip(apod_list, apod_names):
        ax.plot(axis_scale * x_axis, apod[:, ind], label=name)
    ax.set_title('z = {:.2f} mm'.format(axis_scale * z_axis[ind]))
    ax.grid()
    ax.legend()
axes[-1].set_xlabel(xlabel)
fig.tight_layout()

###############################################################################
# Plots
###############################################################################
# Figure 1: apodization maps
FIG_SIZE = (10, 10/1.6)
axis_scale = 1e3
extent = [x_min, x_max, z_max, z_min]
extent = [val * axis_scale for val in extent]
_apod_list = [boxcar_apod_cpp, hanning_apod_cpp, hamming_apod_cpp, tukey25_apod_cpp,
             tukey50_apod_cpp, selfr2d_apod_cpp]
apod_list = [a.squeeze() for a in _apod_list]
apod_names = ['Boxcar', 'Hanning', 'Hamming', 'Tukey 25%', 'Tukey 50%',
              'Sefridge 2D']
fig, axes = plt.subplots(
    nrows=2, ncols=3, sharex=True, sharey=True, figsize=FIG_SIZE
)
for ax, apod, name in zip(axes.ravel(), apod_list, apod_names):
    ax.imshow(apod.T, extent=extent, interpolation=None)
    ax.set_title(name)
#   Axes labeling
xlabel, ylabel = ('x [mm]', 'z [mm]')
for row_axes in axes:
    row_axes[0].set_ylabel(ylabel)
for ax in axes[-1]:
    ax.set_xlabel(xlabel)
fig.tight_layout()

# Figure 2: apodization at specific depths
depth_indexes = (np.array([1/3, 2/3]) * (z_max - z_min) / step).astype(np.int)
fig, axes = plt.subplots(
    nrows=depth_indexes.size, ncols=1, sharex=True, sharey=True,
    figsize=FIG_SIZE
)
for ax, ind in zip(axes.ravel(), depth_indexes):
    for apod, name in zip(apod_list, apod_names):
        ax.plot(axis_scale * x_axis, apod[:, ind], label=name)
    ax.set_title('z = {:.2f} mm'.format(axis_scale * z_axis[ind]))
    ax.grid()
    ax.legend()
axes[-1].set_xlabel(xlabel)
fig.tight_layout()

plt.show()

