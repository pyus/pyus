#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
from pyus.probe import L11Probe
from pyus.imaging.delays import PlaneWave2D, DivergingWave2D
from pyus.utils.grid import grid_interval

###############################################################################
# Parameters
###############################################################################
dtype = np.float32
probe = L11Probe()
mean_sound_speed = 1540.
selected_element = 64
f_number = 1.75  # as selected by PICMUS
x_min, x_max = (-probe.width / 2, +probe.width / 2)
y_min, y_max = (0., 0.)
# z_min, z_max = (0, probe.width)
z_min, z_max = (0, 5e-2)

###############################################################################
# Create domain grid
###############################################################################
wavelength = mean_sound_speed / probe.center_frequency

#   Grid domain axes
step = 0.25 * wavelength
x_axis = grid_interval(start=x_min, stop=x_max, step=step, dtype=dtype)
y_axis = grid_interval(start=y_min, stop=y_max, step=step, dtype=dtype)
z_axis = grid_interval(start=z_min, stop=z_max, step=step, dtype=dtype)

#   Domain meshgrid
# x_mg, y_mg, z_mg = np.meshgrid(x_axis, y_axis, z_axis, indexing='ij')
# im_pos = np.stack([x_mg, y_mg, z_mg])
image_axes = (x_axis, y_axis, z_axis)
image_axes = tuple(ax.astype(dtype=dtype) for ax in image_axes)
image_shape = tuple(ax.size for ax in image_axes)
im_mg = np.array(np.meshgrid(*image_axes, indexing='ij'))
im_pos = im_mg.reshape((3, np.prod(image_shape)))

###############################################################################
# Create delay functors
###############################################################################
mean_sound_speed = 1540
# Plan wave
pw_zero = PlaneWave2D(angle=0, mean_sound_speed=mean_sound_speed, dtype=dtype)
pw_angle = PlaneWave2D(angle=7, mean_sound_speed=mean_sound_speed, dtype=dtype)
# Diverging wave
source = np.array([0, 0, -0.5 * probe.width], dtype=dtype)
dw = DivergingWave2D(
    source=source, mean_sound_speed=mean_sound_speed, dtype=dtype
)

###############################################################################
# Compute delays
###############################################################################
#   Element coordinates
el_pos = np.float32(probe.element_positions[selected_element])[:, np.newaxis]

# Compute apodization
# Reshape because now input is (3,N) so output is (N) and must be reshaped
pw_zero_val = pw_zero(im_pos=im_pos, el_pos=el_pos).reshape(image_shape)
pw_angle_val = pw_angle(im_pos=im_pos, el_pos=el_pos).reshape(image_shape)
dw_val = dw(im_pos=im_pos, el_pos=el_pos).reshape(image_shape)

pw_zero_val_cpp = pw_zero._cy_call(im_pos=im_pos, el_pos=el_pos).reshape(image_shape)
pw_angle_val_cpp = pw_angle._cy_call(im_pos=im_pos, el_pos=el_pos).reshape(image_shape)
dw_val_cpp = dw._cy_call(im_pos=im_pos, el_pos=el_pos).reshape(image_shape)

# start_time = time.time()
# boxcar_apod_py = boxcar_win(im_pos=im_pos, el_pos=el_pos)
# print('elapsed time = {}'.format(time.time() - start_time))
#
# start_time = time.time()
# boxcar_apod_cpp = boxcar_win._cy_call(im_pos=im_pos, el_pos=el_pos)
# print('elapsed time = {}'.format(time.time() - start_time))

print(np.allclose(pw_zero_val, pw_zero_val_cpp))
print(np.allclose(pw_angle_val, pw_angle_val_cpp))
print(np.allclose(dw_val, dw_val_cpp))

###############################################################################
# Plots
###############################################################################
# Figure 1: apodization maps
FIG_SIZE = (10, 10/1.6)
axis_scale = 1e3
extent = [x_min, x_max, z_max, z_min]
extent = [val * axis_scale for val in extent]
_del_list = [pw_zero_val, pw_angle_val, dw_val]
del_list = [a.squeeze() for a in _del_list]
apod_names = [
    'PW ({}°)'.format(pw_zero.angle),
    'PW ({}°)'.format(pw_angle.angle),
    'DW'
]

vmin = np.min([d.min() for d in del_list])
vmax = np.max([d.max() for d in del_list])

fig, axes = plt.subplots(
    nrows=1, ncols=3, sharex=True, sharey=True, figsize=FIG_SIZE
)
for ax, apod, name in zip(axes.ravel(), del_list, apod_names):
    ax.imshow(apod.T, extent=extent, interpolation=None, vmin=vmin, vmax=vmax)
    ax.set_title(name)
#   Axes labeling
xlabel, ylabel = ('x [mm]', 'z [mm]')
# for row_axes in axes:
#     row_axes[0].set_ylabel(ylabel)
axes[0].set_label(ylabel)
# for ax in axes[-1]:
#     ax.set_xlabel(xlabel)
for ax in axes:
    ax.set_xlabel(xlabel)
fig.tight_layout()

# Figure 2: apodization at specific depths
depth_indexes = (np.array([1/3, 2/3]) * (z_max - z_min) / step).astype(np.int)
fig, axes = plt.subplots(
    nrows=depth_indexes.size, ncols=1, sharex=True, sharey=True,
    figsize=FIG_SIZE
)
for ax, ind in zip(axes.ravel(), depth_indexes):
    for apod, name in zip(del_list, apod_names):
        ax.plot(axis_scale * x_axis, apod[:, ind], label=name)
    ax.set_title('z = {:.2f} mm'.format(axis_scale * z_axis[ind]))
    ax.grid()
    ax.legend()
axes[-1].set_xlabel(xlabel)
fig.tight_layout()

plt.show()
