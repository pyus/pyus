import os
import numpy as np
import matplotlib.pyplot as plt

import pyus.utils.picmus
import pyus.signal.utils
from pyus.imaging import CudaDelayAndSumRF
from pyus.probe import L11Probe
from pyus.utils.grid import grid_interval
from pyus.imaging.windows import Selfridge2D
from pyus.imaging.delays import PlaneWave2D
from pyus.imaging.interpolators import Bspline3Interpolator1D
from pyus.signal.hilbert import CudaHilbertFIR
import pyus.utils.plot as putils
from pycuda import gpuarray

# Initialize pycuda
import pycuda.autoinit

###############################################################################
# Data and settings
###############################################################################
probe = L11Probe()

# PICMUS data and acquisition settings loading
data_path = os.path.join(os.pardir, os.pardir, 'datasets', 'picmus17')
dataset_name = 'dataset_rf_in_vitro_type1_transmission_1_nbPW_3.hdf5'
data_rf, settings = pyus.utils.picmus.load_data(
    path=os.path.join(data_path, dataset_name), dtype=np.float32
)
initial_time = settings['initial_time']
sampling_frequency = settings['sampling_frequency']
transmit_frequency = settings['transmit_frequency']
angles = settings['angles']
mean_sound_speed = settings['mean_sound_speed']
sample_number = data_rf.shape[-1]
wavelength = mean_sound_speed / transmit_frequency
dtype = np.float32

###############################################################################
# Grid definitions
###############################################################################
# Time axis
dt = 1 / sampling_frequency
time_axis = initial_time + np.arange(sample_number, dtype=dtype) * dt

# Image domain
x_min, x_max = (-probe.width / 2, probe.width / 2)
y_min, y_max = (0., 0.)
z_min, z_max = (5e-3, 50e-3)

image_limits = ((x_min, x_max), (z_min, z_max))

x_im_axis, dx = np.linspace(
    start=x_min, stop=x_max, num=512, retstep=True, dtype=dtype,
)
dy = dx
y_im_axis = grid_interval(start=y_min, stop=y_max, step=dy, dtype=dtype)
dz = 0.5 * dx
z_im_axis = grid_interval(start=z_min, stop=z_max, step=dz, dtype=dtype)

image_axes = (x_im_axis, y_im_axis, z_im_axis)

###############################################################################
# Convert to IQ
###############################################################################
# Create analytical signal extractor
data_shape = data_rf.shape
hilbert = CudaHilbertFIR(data_shape=data_shape, filter_size=21)

# Move data on GPU
data_rf_gpu = gpuarray.to_gpu(data_rf)

# Compute analytical signal
data_iq_gpu = hilbert(data_rf_gpu)

# Get dtype of analytical signal
data_type = data_iq_gpu.dtype

###############################################################################
# Beamforming
###############################################################################
# Create associated functors
apodization = Selfridge2D(
    element_width=probe.element_width, wavelength=wavelength, dtype=dtype
)
delays = [
    PlaneWave2D(mean_sound_speed=mean_sound_speed, angle=angle, dtype=dtype)
    for angle in angles
]
interp_iq = Bspline3Interpolator1D(
    data_axis=time_axis, boundary='reflect', dtype=data_type
)

# Create Beamformer
beamformer_iq = CudaDelayAndSumRF(
    probe=probe,
    time_axis=time_axis,
    image_axes=image_axes,
    dtype=data_type
)

# Perform beamforming
image_iq_gpu = beamformer_iq.reconstruct(
    data=data_iq_gpu, interpolator=interp_iq, delays=delays,
    apodization=apodization
)

###############################################################################
# Post-processing and plot
###############################################################################
# Compute bmode conversion on GPU
bmode_gpu = pyus.signal.utils.iq2bmode(image_iq_gpu, norm='max')

# Copy data from GPU
bmode = bmode_gpu.get()

axis_scale = 1e3
db_range = 60
vmax = 0
vmin = vmax - db_range
extent = [x_min, x_max, z_max, z_min]
im_kwargs = {'cmap': 'gray', 'vmin': vmin, 'vmax': vmax, 'extent': extent}

fig = plt.figure()
ax = fig.add_subplot(111)
ax.imshow(bmode.squeeze().T, **im_kwargs)
ax.set_title('{}PWs + DAS ({} dB dynamic range)'.format(angles.size, db_range))
ax.set_xlabel('x [mm]')
ax.set_ylabel('z [mm]')
ax.yaxis.set_ticks(np.linspace(z_min, z_max, 10))
putils.format_axes(axes=ax, scale=axis_scale, decimals=1)

plt.show()
