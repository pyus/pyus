#!/bin/bash
set -e -x

# Compile wheels
# To be compliant with the PEP 513 and 571, we should bundle external libraries
# into the wheels. However we don't do it because of license/copyright issues
# and mess with CUDA related stuff. We let the user install the dependencies
# listed and detailed in the README.rst
# Instead, we ensure our setup is manylinux compatible (glibc <= 2.12), build
# the wheels and rename them as 'manylinux2010'
# This may be fixed in the future
#for PYBIN in /opt/python/cp3*/bin; do
PYBINS=(/opt/python/cp35*/bin /opt/python/cp36*/bin /opt/python/cp37*/bin /opt/python/cp38*/bin)
for PYBIN in "${PYBINS[@]}"; do
    "$PYBIN/python" tools/manylinux-check.py
    "${PYBIN}/pip" install numpy cython
    "${PYBIN}/pip" wheel -w wheelhouse --no-deps .
done

for f in wheelhouse/*-linux_x86_64.whl; do mv "$f" "${f/linux/manylinux2010}"; done

# Bundle external shared libraries into the wheels
# for whl in wheelhouse/*.whl; do
#     auditwheel repair "$whl" --plat $PLAT -w /io/wheelhouse/
# done
