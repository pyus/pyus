#!/bin/bash

# Stop at any error, show all commands
set -e -x

cmake -S pulse -B pulse/build
cmake --build pulse/build -j "$(nproc)"
