[cuda]:(https://developer.nvidia.com/cuda-10.0-download-archive)

# PULSE: Parallel ULtraSound Engine 
PULSE is the GPU-accelerated C++ backend of PyUS, that could, with some effort,
be used as a standalone C++ library for ultrasound imaging.
> The name may change in the future due to obvious name conflicts with the
> [PulseAudio](https://www.freedesktop.org/wiki/Software/PulseAudio/) library

## Requirements:
- CUDA 10.0 
- FFTW3
- CMake 3.10+
- Ubuntu 16.04+ or derivatives

## Build instructions:
### Install dependencies:
#### CUDA Toolkit
Download the [CUDA 10.0 installer][cuda] of your choice and follow the 
installation instructions. After the installation, you need to update your `PATH`
and `LD_LIBRARY_PATH` environment variables. To do so you have several options 
listed [here](https://help.ubuntu.com/community/EnvironmentVariables).
The prefered option is:
- for the `PATH` variable: create a `cuda-10.0.sh` file in `/etc/profile.d` directory 
with the following line: `export PATH=/usr/local/cuda-10.0/bin:$PATH`
- for the `LD_LIBRARY_PATH` variable: create a `cuda-10.0.conf` file in the
`/etc/ld.so.conf.d` directory with the following line: `/usr/local/cuda-10.0/lib64`
(if it does not already exist)

Restart your computer.

#### FFTW
[Download FFTW](http://www.fftw.org/download.html) and follow the 
[installation instructions](http://www.fftw.org/fftw3_doc/Installation-on-Unix.html).
During the configure step, use the `--enable-shared`, `--enable-float` and 
`--enable-openmp` flags in order to be able to build PULSE.

#### CMake
We need CMake 3.10 or above. For Ubuntu 18.04, you can use the version from the 
official repositories with `apt-get install cmake`. If you are running an older
Ubuntu, it will not be in the official repositories and you will have to install
it manually as described [here](https://cmake.org/install/). If you choose the
precompiled binaries, you will need to pick an install location. A good one is
the `~/.local` folder, used for user-space packages. You can also create a local 
folder for CMake only, and add it to your `PATH` by adding 
`export PATH="$PATH:path/to/cmake"` to your `~/.bashrc` file. You will have to 
download the precompiled binaries and extract them in the install location you
picked.

### Build PULSE
Once all the requirements are satisfied, building PULSE is quite simple, just
use the following commands:
``` bash
cd pulse
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j8
```

