#include "pulse/signal/hilbert_fft_host.h"
#include <fftw3.h>
#include <omp.h>
#include <chrono>
#include <complex>
#include <iostream>
#include <cmath>

namespace pulse {

HilbertFFTHost::HilbertFFTHost(int len_signal, int batch):
        HilbertFFT(len_signal, batch)
{}

HilbertFFTHost::~HilbertFFTHost() = default;

void HilbertFFTHost::analytic_signal(float *x_mat,
                                     std::complex<float> *x_a_mat)
{
    hilbert_fftw_omp_r2c(x_mat, x_a_mat);
}

void HilbertFFTHost::hilbert_fftw_r2c(const float* x_mat,
                               std::complex<float>* x_a_mat)
{
    /**
    * Returns analytical signals of all rows of x_mat
    */

    // Create plan for r2c fft
    fftwf_plan plan_fft = fftwf_plan_many_dft_r2c(
            plan_setup_.rank, plan_setup_.n, plan_setup_.batch, (float*)x_mat,
            plan_setup_.inembed, plan_setup_.istride, plan_setup_.idist,
            (fftwf_complex*)x_a_mat, plan_setup_.onembed, plan_setup_.ostride,
            plan_setup_.odist, FFTW_ESTIMATE);

    // Launch fft (out-of-place)
    fftwf_execute(plan_fft);

    // Cut negative frequencies
    //   Precompute division
    const float i_size = 1.f / fft_size_;
    int max_index_even = fft_size_ / 2;
    int max_index_odd = fft_size_ / 2 + 1;

    for (int batch = 0; batch < batch_; ++batch) {

        for (int sample = 0; sample < fft_size_; ++sample) {

            // Linearized thread index, corresponds to index of data
            const int thread_1D_pos = sample + batch * fft_size_;

            if (fft_size_ % 2 == 0) {
                if (sample == 0 || sample == max_index_even) {
                    x_a_mat[thread_1D_pos] *= i_size;
                }
                else if (sample < max_index_even) {
                    x_a_mat[thread_1D_pos] *= 2.f * i_size;
                }
                else {
                    x_a_mat[thread_1D_pos] = 0;
                }
            }
            else {
                if (sample == 0) {
                    x_a_mat[thread_1D_pos] *= i_size;
                }
                else if (sample < max_index_odd) {
                    x_a_mat[thread_1D_pos] *= 2.f * i_size;
                }
                else {
                    x_a_mat[thread_1D_pos] = 0;
                }
            }
        }
    }

    // Create plan for c2c fft
    fftwf_plan plan_ifft = fftwf_plan_many_dft(
            plan_setup_.rank, plan_setup_.n, plan_setup_.batch,
            (fftwf_complex*)x_a_mat, plan_setup_.inembed, plan_setup_.istride,
            plan_setup_.idist, (fftwf_complex*)x_a_mat, plan_setup_.onembed,
            plan_setup_.ostride, plan_setup_.odist, FFTW_BACKWARD,
            FFTW_ESTIMATE);

    // Launch ifft (in-place)
    fftwf_execute(plan_ifft);

    // Clean
    fftwf_destroy_plan(plan_fft);
    fftwf_destroy_plan(plan_ifft);
    fftwf_cleanup();
}


void HilbertFFTHost::hilbert_fftw_omp_r2c(const float* x_mat,
                                   std::complex<float>* x_a_mat)
{
    /**
    * Returns analytical signals of all rows of x_mat
    */

    // Init fftw with openMP
    fftwf_init_threads();
    fftwf_plan_with_nthreads(omp_get_max_threads());

    // Create plan for r2c fft
    fftwf_plan plan_fft = fftwf_plan_many_dft_r2c(
            plan_setup_.rank, plan_setup_.n, plan_setup_.batch, (float*)x_mat,
            plan_setup_.inembed, plan_setup_.istride, plan_setup_.idist,
            (fftwf_complex*)x_a_mat, plan_setup_.onembed, plan_setup_.ostride,
            plan_setup_.odist, FFTW_ESTIMATE);

    // Launch fft (out-of-place)
    fftwf_execute(plan_fft);

    // Cut negative frequencies
    //   Precompute division
    const float i_size = 1.f / fft_size_;
    int max_index_even = fft_size_ / 2;
    int max_index_odd = fft_size_ / 2 + 1;

#pragma omp parallel for collapse(2)

    for (int batch = 0; batch < batch_; ++batch) {

        for (int sample = 0; sample < fft_size_; ++sample) {

            // Linearized thread index, corresponds to index of data
            const int thread_1D_pos = sample + batch * fft_size_;

            if (fft_size_ % 2 == 0) {
                if (sample == 0 || sample == max_index_even) {
                    x_a_mat[thread_1D_pos] *= i_size;
                }
                else if (sample < max_index_even) {
                    x_a_mat[thread_1D_pos] *= 2.f * i_size;
                }
                else {
                    x_a_mat[thread_1D_pos] = 0;
                }
            }
            else {
                if (sample == 0) {
                    x_a_mat[thread_1D_pos] *= i_size;
                }
                else if (sample < max_index_odd) {
                    x_a_mat[thread_1D_pos] *= 2.f * i_size;
                }
                else {
                    x_a_mat[thread_1D_pos] = 0;
                }
            }
        }
    }

    // Create plan for c2c fft
    fftwf_plan plan_ifft = fftwf_plan_many_dft(
            plan_setup_.rank, plan_setup_.n, plan_setup_.batch,
            (fftwf_complex*)x_a_mat, plan_setup_.inembed, plan_setup_.istride,
            plan_setup_.idist, (fftwf_complex*)x_a_mat, plan_setup_.onembed,
            plan_setup_.ostride, plan_setup_.odist, FFTW_BACKWARD,
            FFTW_ESTIMATE);

    // Launch ifft (in-place)
    fftwf_execute(plan_ifft);

    // Clean
    fftwf_destroy_plan(plan_fft);
    fftwf_destroy_plan(plan_ifft);

    fftwf_cleanup_threads();
}

} // end namespace pulse
