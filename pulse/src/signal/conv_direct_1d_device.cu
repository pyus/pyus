#include "pulse/signal/conv_direct_1d_device.h"
#include "pulse/helper/math.h"
#include "helper/cuda.h"
#include <thrust/complex.h>
#include <cuda_runtime.h>
#include <iostream>

namespace pulse {

//Note: these kernels are quite simple and do not use shared memory.
//TODO: use shared memory

template<typename T>
__global__
void conv1Dfull_kernel(const T* signal,  int signal_size, int nb_signals,
                       const T* kernel, int kernel_size, int nb_kernels,
                       T* output)
{

    const int3 thread_3D_pos = make_int3(
            blockIdx.x * blockDim.x + threadIdx.x,
            blockIdx.y * blockDim.y + threadIdx.y,
            blockIdx.z * blockDim.z + threadIdx.z
    );

    // Turn 3D grid index into '2D' (batches, samples)
    const int2 thread_2D_pos = make_int2(
            thread_3D_pos.x,
            thread_3D_pos.y + thread_3D_pos.z * gridDim.y
    );

    int output_size = signal_size + kernel_size - 1;
    const int thread_1D_pos = thread_2D_pos.y * output_size + thread_2D_pos.x;

    // Make sure we don't try and access memory outside the image
    // by having any threads mapped there return early
    if (thread_2D_pos.x >= output_size || thread_2D_pos.y >= max(nb_signals, nb_kernels)) return;

    T sum = 0;
    int i = thread_2D_pos.x;
    int j_min = max(0, i - (kernel_size - 1));
    int j_max = min(signal_size - 1, i);
    int r_sig = nb_signals > 1 ? thread_2D_pos.y : 0;
    int r_ker = nb_kernels > 1 ? thread_2D_pos.y : 0;

    for (int j = j_min; j <= j_max; ++j) {
        sum += signal[j + r_sig * signal_size] * kernel[i - j + r_ker * kernel_size];
    }
    output[thread_1D_pos] = sum;
}

//! Assumes signal has a bigger size than filter
template<typename T>
__global__
void conv1Dsame_kernel(const T* signal, int signal_size, int nb_signals,
                       const T* kernel, int kernel_size, int nb_kernels,
                       T* output)
{
    const int3 thread_3D_pos = make_int3(
            blockIdx.x * blockDim.x + threadIdx.x,
            blockIdx.y * blockDim.y + threadIdx.y,
            blockIdx.z * blockDim.z + threadIdx.z
    );

    // Turn 3D grid index into '2D' (batches, samples)
    const int2 thread_2D_pos = make_int2(
            thread_3D_pos.x,
            thread_3D_pos.y + thread_3D_pos.z * gridDim.y
    );

    const int thread_1D_pos = thread_2D_pos.y * signal_size + thread_2D_pos.x;

    // Make sure we don't try and access memory outside the image
    // by having any threads mapped there return early
    if (thread_2D_pos.x >= signal_size || thread_2D_pos.y >= max(nb_signals, nb_kernels)) return;

    // Precompute some constants so we dont compute unnecessary integer divisions
    int kernel_radius = (kernel_size - 1) / 2;
    int kernel_size_2 = kernel_size / 2;
    int even_offset = (kernel_size + 1) % 2;
    T sum = 0;
    int i = thread_2D_pos.x;
    int j_min = max(0, i - kernel_radius - even_offset);
    int j_max = min(signal_size - 1, i + kernel_size_2 - even_offset);
    int r_sig = nb_signals > 1 ? thread_2D_pos.y : 0;
    int r_ker = nb_kernels > 1 ? thread_2D_pos.y : 0;

    for (int j = j_min; j <= j_max; ++j) {
        sum += signal[j + r_sig * signal_size] *
               kernel[i - j + kernel_size_2 - even_offset + r_ker * kernel_size];
    }
    output[thread_1D_pos] = sum;
}

//! Assumes signal has a bigger size than filter
template<typename T>
__global__
void conv1Dvalid_kernel(const T* signal, int signal_size, int nb_signals,
                        const T* kernel, int kernel_size, int nb_kernels,
                        T* output)
{

    const int3 thread_3D_pos = make_int3(
            blockIdx.x * blockDim.x + threadIdx.x,
            blockIdx.y * blockDim.y + threadIdx.y,
            blockIdx.z * blockDim.z + threadIdx.z
    );

    // Turn 3D grid index into '2D' (batches, samples)
    const int2 thread_2D_pos = make_int2(
            thread_3D_pos.x,
            thread_3D_pos.y + thread_3D_pos.z * gridDim.y
    );

    int output_size = signal_size - kernel_size + 1;
    const int thread_1D_pos = thread_2D_pos.y * output_size + thread_2D_pos.x;

    // Make sure we don't try and access memory outside the image
    // by having any threads mapped there return early
    if (thread_2D_pos.x >= output_size || thread_2D_pos.y >= max(nb_signals, nb_kernels)) return;

    T sum = 0;
    int i = thread_2D_pos.x;
    int r_sig = nb_signals > 1 ? thread_2D_pos.y : 0;
    int r_ker = nb_kernels > 1 ? thread_2D_pos.y : 0;

    for (int j = kernel_size - 1, k = i; j >= 0; --j) {
        sum += signal[k + r_sig * signal_size] * kernel[j + r_ker * kernel_size];
        ++k;
    }
    output[thread_1D_pos] = sum;
}


template<typename T>
void ConvolutionDirect1DDevice::convolve_full(
        const T* input_d, int input_size, int nb_inputs,
        const T* filter_d, int filter_size, int nb_filters,
        T* output_d
)
{
    // assert correct shapes (either input and filters same size or one is 1)
    if (nb_inputs != nb_filters) {
        if ((nb_inputs != 1) && (nb_filters != 1)) {
            std::cerr << "Error: Shape mismatch, only NxN, 1xN and Nx1 batched convolutions are valid" << std::endl;
        }
    }

    int nb_conv = std::max(nb_inputs, nb_filters);
    int output_size = input_size + filter_size - 1;

    int max_grid_size_y = 65535;
    int grid_size_y = std::min(nb_conv, max_grid_size_y);
    int grid_size_z = nb_conv / grid_size_y + 1;

    const dim3 blockSize(512, 1, 1);
    const dim3 gridSize(
            (output_size + blockSize.x - 1) / blockSize.x,
            (grid_size_y + blockSize.y - 1) / blockSize.y,
            (grid_size_z + blockSize.z - 1) / blockSize.z
    );

    conv1Dfull_kernel <<< gridSize, blockSize >>> (
            input_d, input_size, nb_inputs, filter_d, filter_size, nb_filters, output_d
    );

    checkCudaErrors(cudaDeviceSynchronize());
    checkCudaErrors(cudaGetLastError());
}

template<typename T>
void ConvolutionDirect1DDevice::convolve_same(
        const T* input_d, int input_size, int nb_inputs,
        const T* filter_d, int filter_size, int nb_filters,
        T* output_d
)
{
    // The output will be the same size as the bigger input
    int in_gteq_filt = input_size >= filter_size;
    const T* signal_d = in_gteq_filt ? input_d : filter_d;
    const T* kernel_d = in_gteq_filt ? filter_d : input_d;
    int signal_size = in_gteq_filt ? input_size : filter_size;
    int kernel_size = in_gteq_filt ? filter_size : input_size;
    int nb_signals = in_gteq_filt ? nb_inputs : nb_filters;
    int nb_kernels = in_gteq_filt ? nb_filters : nb_inputs;

    // assert correct shapes (either input and filters same size or one is 1)
    if (nb_signals != nb_kernels) {
        if ((nb_signals != 1) && (nb_kernels != 1)) {
            std::cerr << "Error: Shape mismatch, only NxN, 1xN and Nx1 batched convolutions are valid" << std::endl;
        }
    }

    int nb_conv = std::max(nb_signals, nb_kernels);
    int max_grid_size_y = 65535;
    int grid_size_y = std::min(nb_conv, max_grid_size_y);
    int grid_size_z = nb_conv / grid_size_y + 1;

    const dim3 blockSize(512, 1, 1);
    const dim3 gridSize(
            (signal_size + blockSize.x - 1) / blockSize.x,
            (grid_size_y + blockSize.y - 1) / blockSize.y,
            (grid_size_z + blockSize.z - 1) / blockSize.z
    );

    conv1Dsame_kernel <<< gridSize, blockSize >>> (
            signal_d, signal_size, nb_signals, kernel_d, kernel_size,
            nb_kernels, output_d
    );

    checkCudaErrors(cudaDeviceSynchronize());
    checkCudaErrors(cudaGetLastError());
}

template<typename T>
void ConvolutionDirect1DDevice::convolve_valid(
        const T* input_d, int input_size, int nb_inputs,
        const T* filter_d, int filter_size, int nb_filters,
        T* output_d
)
{
    int in_gteq_filt = input_size >= filter_size;
    const T* signal_d = in_gteq_filt ? input_d : filter_d;
    const T* kernel_d = in_gteq_filt ? filter_d : input_d;
    int signal_size = in_gteq_filt ? input_size : filter_size;
    int kernel_size = in_gteq_filt ? filter_size : input_size;
    int output_size = signal_size - kernel_size + 1;
    int nb_signals = in_gteq_filt ? nb_inputs : nb_filters;
    int nb_kernels = in_gteq_filt ? nb_filters : nb_inputs;

    // assert correct shapes (either input and filters same size or one is 1)
    if (nb_signals != nb_kernels) {
        if ((nb_signals != 1) && (nb_kernels != 1)) {
            std::cerr << "Error: Shape mismatch, only NxN, 1xN and Nx1 batched convolutions are valid" << std::endl;
        }
    }

    int nb_conv = std::max(nb_signals, nb_kernels);
    int max_grid_size_y = 65535;
    int grid_size_y = std::min(nb_conv, max_grid_size_y);
    int grid_size_z = nb_conv / grid_size_y + 1;

    const dim3 blockSize(512, 1, 1);
    const dim3 gridSize(
            (output_size + blockSize.x - 1) / blockSize.x,
            (grid_size_y + blockSize.y - 1) / blockSize.y,
            (grid_size_z + blockSize.z - 1) / blockSize.z
    );

    conv1Dvalid_kernel <<< gridSize, blockSize >>> (
            signal_d, signal_size, nb_signals, kernel_d, kernel_size, nb_kernels, output_d
    );

    checkCudaErrors(cudaDeviceSynchronize());
    checkCudaErrors(cudaGetLastError());
}

template void ConvolutionDirect1DDevice::convolve_full<float>(const float*, int, int, const float*, int, int, float*);
template void ConvolutionDirect1DDevice::convolve_same<float>(const float*, int, int, const float*, int, int, float*);
template void ConvolutionDirect1DDevice::convolve_valid<float>(const float*, int, int, const float*, int, int, float*);
template void ConvolutionDirect1DDevice::convolve_full<thrust::complex<float>>(const thrust::complex<float>*, int, int, const thrust::complex<float>*, int, int, thrust::complex<float>*);
template void ConvolutionDirect1DDevice::convolve_same<thrust::complex<float>>(const thrust::complex<float>*, int, int, const thrust::complex<float>*, int, int, thrust::complex<float>*);
template void ConvolutionDirect1DDevice::convolve_valid<thrust::complex<float>>(const thrust::complex<float>*, int, int, const thrust::complex<float>*, int, int, thrust::complex<float>*);

} // end namespace pulse
