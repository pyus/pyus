#include "pulse/signal/hilbert_fir_device.h"
#include <thrust/device_vector.h>
#include <thrust/complex.h>
#include <helper/cuda.h>

namespace pulse {

HilbertFIRDevice::HilbertFIRDevice(
        int len_signal, int batch, int n_taps, float beta
): HilbertFIR(len_signal, batch, n_taps, beta)
{
    fir_filter_d_ = thrust::device_vector<thrust::complex<float>>(
            (thrust::complex<float>*)fir_filter_.data(),
            (thrust::complex<float>*)fir_filter_.data() + n_taps_
    );
}

HilbertFIRDevice::HilbertFIRDevice(
        int len_signal, int batch,
        std::vector<std::complex<float>> fir_filter
): HilbertFIR(len_signal, batch, fir_filter)
{
    fir_filter_d_ = thrust::device_vector<thrust::complex<float>>(
            (thrust::complex<float>*)fir_filter_.data(),
            (thrust::complex<float>*)fir_filter_.data() + n_taps_
    );
}

HilbertFIRDevice::~HilbertFIRDevice() = default;

__global__
void convolveFIR(const float* signal, int signal_size, int nb_signals,
                 const thrust::complex<float>* kernel, int kernel_size,
                 thrust::complex<float>* output);

//! Assumes signal has a bigger size than filter
//! Assumes filter size < 512
void HilbertFIRDevice::analytic_signal(float* x_mat,
                                       std::complex<float>* x_a_mat)
{
    if (len_signal_ < n_taps_) {
        throw std::runtime_error("Signal length must be larger than filter length");
    }
    if (n_taps_ >= 512) {
        throw std::runtime_error("Filter must be smaller than 512 taps");
    }

    auto filter_ptr = thrust::raw_pointer_cast(fir_filter_d_.data());

    int max_grid_size_y = 65535;
    int grid_size_y = std::min(batch_, max_grid_size_y);
    int grid_size_z = batch_ / grid_size_y + 1;

    const dim3 blockSize(512, 1, 1);
    const dim3 gridSize(
            (len_signal_ + blockSize.x - 1) / blockSize.x,
            (grid_size_y + blockSize.y - 1) / blockSize.y,
            (grid_size_z + blockSize.z - 1) / blockSize.z
    );

    convolveFIR <<< gridSize, blockSize, n_taps_ * sizeof(thrust::complex<float>) >>> (
            x_mat, len_signal_, batch_,
            filter_ptr, n_taps_,
            (thrust::complex<float>*)x_a_mat
    );

    checkCudaErrors(cudaDeviceSynchronize());
    checkCudaErrors(cudaGetLastError());
}

//! Assumes signal has a bigger size than filter
//! Assumes filter size < blockSize.x
__global__
void convolveFIR(const float* signal, int signal_size, int nb_signals,
                 const thrust::complex<float>* kernel, int kernel_size,
                 thrust::complex<float>* output)
{
    extern __shared__ thrust::complex<float> filter[];

    int t = threadIdx.x;
    if (t < kernel_size) {
        filter[t] = kernel[t];
    }
    __syncthreads();

    const int3 thread_3D_pos = make_int3(
            blockIdx.x * blockDim.x + threadIdx.x,
            blockIdx.y * blockDim.y + threadIdx.y,
            blockIdx.z * blockDim.z + threadIdx.z
    );

    // Turn 3D grid index into '2D' (batches, samples)
    const int2 thread_2D_pos = make_int2(
            thread_3D_pos.x,
            thread_3D_pos.y + thread_3D_pos.z * gridDim.y
    );

    const int thread_1D_pos = thread_2D_pos.y * signal_size + thread_2D_pos.x;

    // Make sure we don't try and access memory outside the image
    // by having any threads mapped there return early
    if (thread_2D_pos.x >= signal_size || thread_2D_pos.y >= nb_signals) return;

    // Precompute some constants so we dont compute unnecessary integer divisions
    int kernel_radius = (kernel_size - 1) / 2;
    int kernel_size_2 = kernel_size / 2;
    int even_offset = (kernel_size + 1) % 2;
    thrust::complex<float> sum = 0;
    int i = thread_2D_pos.x;
    int j_min = max(0, i - kernel_radius - even_offset);
    int j_max = min(signal_size - 1, i + kernel_size_2 - even_offset);
    int r_sig = nb_signals > 1 ? thread_2D_pos.y : 0;

    for (int j = j_min; j <= j_max; ++j) {
        sum += signal[j + r_sig * signal_size] *
               filter[i - j + kernel_size_2 - even_offset];
    }
    output[thread_1D_pos] = sum;
}

} // end namespace pulse
