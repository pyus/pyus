#include "pulse/signal/hilbert_fft_device.h"
#include "pulse/helper/math.h"
#include "../helper/cuda.h"
#include <cuda_runtime.h>
#include <cufft.h>

namespace pulse {

HilbertFFTDevice::HilbertFFTDevice(int len_signal, int batch):
        HilbertFFT(len_signal, batch)
{
    // Create cufft plans
    cufftPlanMany(&cufft_handle_r2c_, plan_setup_.rank, plan_setup_.n,
                  plan_setup_.inembed, plan_setup_.istride, plan_setup_.idist,
                  plan_setup_.onembed, plan_setup_.ostride, plan_setup_.odist,
                  CUFFT_R2C, plan_setup_.batch);

    cufftPlanMany(&cufft_handle_c2c_, plan_setup_.rank, plan_setup_.n,
                  plan_setup_.inembed, plan_setup_.istride, plan_setup_.idist,
                  plan_setup_.onembed, plan_setup_.ostride, plan_setup_.odist,
                  CUFFT_C2C, plan_setup_.batch);

    // In case we need a huge amount of data on device memory, we may want to
    // create only one plan and destroy and reconfigure it before the launches
    // of fft and ifft
}

HilbertFFTDevice::~HilbertFFTDevice()
{
    // Destroy cufft handle
    cufftDestroy(cufft_handle_r2c_);
    cufftDestroy(cufft_handle_c2c_);
}

void HilbertFFTDevice::analytic_signal(float *x_mat,
                                       std::complex<float> *x_a_mat)
{
    hilbert_cufft_r2c(x_mat, (cufftComplex*) x_a_mat);
}

// Forward declaration of kernel
__global__ void
cut_neg_freq(cufftComplex* x, int fft_size, int batch);


/*
 * Since we always deal with real input, we can use R2C fft:
 * Using real to complex fft, due to conjugate symmetry, cufft does not
 * compute Fourrier coeffs for negative frequencies so it implicitly cuts
 * negative frequencies -> no need to do it afterwards.
 * However, we still need to multiply by 2 all coeffs but first and last
 * and to divide them by the total fft size so there will be a kernel launch
 * anyway. Moreover, it implies the use of a different cufft plan for fft and
 * ifft, and more device allocation to store 2 plans + real data +
 * analytical signal, since R2C works best with out-of-place fft,
 * instead of allocation of one plan + analytical signal and in-place fft for
 * C2C. However, the strided copy used is the previous function to copy real
 * data to complex container is very slow, so the r2c approach is way faster
 * but uses more memory (this could be managed for huge data by creating only
 * one plan and destroying it and reconfiguring it during execution. This adds
 * a time overhead but for big data, it does not impact much the execution time)
 */
void HilbertFFTDevice::hilbert_cufft_r2c(cufftReal* d_x_mat,
                                         cufftComplex* d_x_a_mat)
{
    /**
    * Returns analytical signals of all rows of x_mat
    */

    // Launch fft (out-of-place)
    cufftExecR2C(cufft_handle_r2c_, d_x_mat, d_x_a_mat);

    // Cut negative frequencies
    const dim3 blockSize(512, 1, 1);
    const dim3 gridSize((fft_size_ + blockSize.x - 1) / blockSize.x,
                        (batch_ + blockSize.y - 1) / blockSize.y,
                        1);

    cut_neg_freq <<< gridSize, blockSize >>> (d_x_a_mat, fft_size_, batch_);

    checkCudaErrors(cudaDeviceSynchronize());
    checkCudaErrors(cudaGetLastError());

    // Launch ifft (in-place)
    cufftExecC2C(cufft_handle_c2c_, d_x_a_mat, d_x_a_mat, CUFFT_INVERSE);

    // In case of huge amount of data, use this to save space on device memory
//    cufftDestroy(cufft_handle_r2c_);
//    cufftPlanMany(&cufft_handle_r2c_, plan_setup_.rank, plan_setup_.n,
//                  plan_setup_.inembed, plan_setup_.istride, plan_setup_.idist,
//                  plan_setup_.onembed, plan_setup_.ostride, plan_setup_.odist,
//                  CUFFT_C2C, plan_setup_.batch);
//    cufftExecC2C(cufft_handle_r2c_, d_x_a_mat, d_x_a_mat, CUFFT_INVERSE);
}

// Uses complex to complex in place fft and ifft
void HilbertFFTDevice::hilbert_cufft_c2c(const float* d_x_mat,
                                         cufftComplex* d_x_a_mat)
{
    /**
    * Returns analytical signals of all rows of x_mat
    */

    // Strided copy from real device data to complex device container (slow)
    checkCudaErrors(
            cudaMemcpy2D(
                    (void*) d_x_a_mat,       // dst
                    sizeof(cufftComplex),    // dst pitch
                    (void*) d_x_mat,         // src
                    sizeof(float),           // src pitch
                    sizeof(float),           // width (cols in bytes)
                    fft_size_ * batch_,      // Height (nb rows)
                    cudaMemcpyHostToDevice)  // kind
    );

    // Launch fft (in-place)
    cufftExecC2C(cufft_handle_c2c_, d_x_a_mat, d_x_a_mat, CUFFT_FORWARD);

    // Cut negative frequencies
    const dim3 blockSize(512, 1, 1);
    const dim3 gridSize((fft_size_ + blockSize.x - 1) / blockSize.x,
                        (batch_ + blockSize.y - 1) / blockSize.y,
                        1);

    cut_neg_freq <<< gridSize, blockSize >>> (d_x_a_mat, fft_size_, batch_);

    checkCudaErrors(cudaDeviceSynchronize());
    checkCudaErrors(cudaGetLastError());

    // Launch ifft (in-place)
    cufftExecC2C(cufft_handle_c2c_, d_x_a_mat, d_x_a_mat, CUFFT_INVERSE);
}

// Kernel
__global__ void
cut_neg_freq(cufftComplex* x, int fft_size, int batch)
{
    /**
     * Cut negative frequencies and divide by size
     */

    // Thread position in grid
    const int2 thread_2D_pos = make_int2(
            blockIdx.x * blockDim.x + threadIdx.x,
            blockIdx.y * blockDim.y + threadIdx.y
            );

    if (thread_2D_pos.x >= fft_size || thread_2D_pos.y >= batch)
        return;

    // Linearized thread index, corresponds to index of data
    const int thread_1D_pos = thread_2D_pos.x + thread_2D_pos.y * fft_size;

    // Precompute division
    const float i_size = 1.f / fft_size;

    if (fft_size % 2 == 0) {
        int max_index = fft_size / 2;
        if (thread_2D_pos.x == 0 || thread_2D_pos.x == max_index) {
            x[thread_1D_pos] *= i_size;
        }
        else if (thread_2D_pos.x < max_index) {
            x[thread_1D_pos] *= 2.f * i_size;
        }
        else {
            x[thread_1D_pos] = {0, 0};
        }
    }
    else {
        int max_index = fft_size / 2 + 1;
        if (thread_2D_pos.x == 0) {
            x[thread_1D_pos] *= i_size;
        }
        else if (thread_2D_pos.x < max_index) {
            x[thread_1D_pos] *= 2.f * i_size;
        }
        else {
            x[thread_1D_pos] = {0, 0};
        }
    }
}

} // end namespace pulse
