#include "pulse/signal/bmode.h"

namespace pulse {

Bmode::Bmode(Size3D size_image):size_image_(size_image)
{}

Bmode::~Bmode()
{
    delete hilbert_;
}

} // end namespace pulse
