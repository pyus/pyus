#include "pulse/signal/hilbert_fft.h"

namespace pulse {

HilbertFFT::HilbertFFT(int len_signal, int batch): Hilbert(len_signal, batch)
{
    // Set up fft plan
    plan_setup_.rank = 1;
    plan_setup_.n = &fft_size_;
    plan_setup_.istride = 1;
    plan_setup_.ostride = 1;
    plan_setup_.idist = fft_size_;
    plan_setup_.odist = fft_size_;
    plan_setup_.inembed = new int[1]();
    plan_setup_.onembed = new int[1]();
    plan_setup_.batch = batch_;
}

HilbertFFT::~HilbertFFT() = default;

} // end namespace pulse
