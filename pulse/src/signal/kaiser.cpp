#include "pulse/signal/kaiser.h"
#include "pulse/helper/math.h"
#include <vector>
#include <cmath>

namespace pulse {

// Compute the zeroth order modified Bessel function of the first kind
// at x using the series expansion
float bessel_i0(float x)
{
//    const double eps = 0.000001;

    //  initialize the series term for m=0 and the result
    float besselValue = 0;
    float term = 1;
    float m = 0;

    //  accumulate terms as long as they are significant
    while(term  > EPS_1 * besselValue)
    {
        besselValue += term;

        //  update the term
        ++m;
        term *= (x * x) / (4 * m * m);
    }

    return besselValue;
}

std::vector<float> kaiser_window(int M, float beta)
{
    auto win = std::vector<float>(M);

    //  Pre-compute the shared denominator in the Kaiser equation.
    const float oneOverDenom = 1.f / bessel_i0(beta);

    int N = M - 1;
    float one_over_N = 1.f / N;

    for (int n = 0; n <= N; ++n) {
        float K = (2.f * n * one_over_N) - 1.f;
        float arg = std::sqrt(1.f - (K * K));

        win[n] = bessel_i0(beta * arg) * oneOverDenom;
    }

    return win;
}

} // end namespace pulse
