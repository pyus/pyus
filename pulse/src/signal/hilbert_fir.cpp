#include <pulse/helper/math.h>
#include <algorithm>
#include "pulse/signal/hilbert_fir.h"
#include "pulse/signal/kaiser.h"
#include <iostream>

namespace pulse {

HilbertFIR::HilbertFIR(int len_signal, int batch, int n_taps, float beta):
        Hilbert(len_signal, batch), len_signal_(len_signal), n_taps_(n_taps)
{
    // Create Kaiser window
    std::vector<float> kaiser = kaiser_window(n_taps, beta);

    fir_filter_ = std::vector<std::complex<float>>(n_taps, 0);
    std::complex<float> i(0, 1.f);
    float fc = 1.f;
    float t_start = (1.f - n_taps) / 2.f;
//    float t_stop = n_taps / 2.f;
    float sum = 0.f;

    // Construct ideal hilbert filter truncated to desired length and multiply
    // it with tapered window
    for (int k=0; k < n_taps; ++k) {
        float t = fc / 2.f * (t_start + k);
        fir_filter_[k] = sinc(t) * std::exp(i * PI * t) * kaiser[k];
        sum += fir_filter_[k].real();
    }

    std::transform(fir_filter_.begin(), fir_filter_.end(), fir_filter_.begin(),
                   [sum](std::complex<float> fir) { return fir / sum; }
    );
}

HilbertFIR::HilbertFIR(
        int len_signal, int batch,
        std::vector<std::complex<float>> fir_filter
): Hilbert(len_signal, batch), fir_filter_(fir_filter),
   len_signal_(len_signal), n_taps_(fir_filter.size())
{}


HilbertFIR::~HilbertFIR() = default;

} // end namespace pulse
