#include "pulse/signal/hilbert.h"

namespace pulse {

Hilbert::Hilbert(int len_signal, int batch):
        fft_size_(len_signal), batch_(batch)
{}

Hilbert::~Hilbert() = default;

} // end namespace pulse
