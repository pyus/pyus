#include "pulse/signal/bmode_host.h"
#include "pulse/signal/hilbert_fft_host.h"
#include <algorithm>

namespace pulse {

struct abs_functor : public std::unary_function<std::complex<float>, float>
{
    abs_functor(){}

    float operator()(const std::complex<float>& hilbert) const {

        return abs(hilbert);
    }
};

struct abs_log_compression : public std::unary_function<std::complex<float>, float>
{
    abs_log_compression(float normalization) : normalization_(normalization) {}

    float operator()(const std::complex<float>& hilbert) const {

        float env = abs(hilbert);
        return 20.f * log10f(env / normalization_);
    }

private:
    const float normalization_;
};

struct log_compression : public std::unary_function<float, float>
{
    log_compression(float normalization) : normalization_(normalization) {}

    float operator()(const float& env) const {

        return 20.f * log10f(env / normalization_);
    }

private:
    const float normalization_;
};

BmodeHost::BmodeHost(Size3D size_image): Bmode(size_image)
{
    hilbert_ = new HilbertFFTHost(size_image.z, size_image.x * size_image.y);
    envelope_ = std::vector<std::complex<float>>(size_image.len);
}

BmodeHost::~BmodeHost() = default;

void BmodeHost::RF2Bmode(float* rf_image)
{
    // Compute envelope
    hilbert_->analytic_signal(rf_image, envelope_.data());

    // Compute rf_image = abs(envelope)
    std::transform(envelope_.begin(), envelope_.end(), rf_image, abs_functor());

    // Compute normalization_factor = max(abs(envelope))
    float normalization_factor = *std::max_element(rf_image, rf_image + size_image_.len);

    // Compute 20 * log10(abs(envelope) / normalization_factor)
    std::transform(rf_image, rf_image + size_image_.len, rf_image,
                   log_compression(normalization_factor));
}

void BmodeHost::RF2Bmode(float* rf_image, float normalization_factor)
{
    // Compute envelope
    hilbert_->analytic_signal(rf_image, envelope_.data());

    // Compute 20 * log10(abs(envelope) / normalization_factor)
    std::transform(envelope_.begin(), envelope_.end(), rf_image,
                   abs_log_compression(normalization_factor));
}

} // end namespace pulse
