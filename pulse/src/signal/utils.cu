#include "pulse/signal/utils.h"
#include <cuda_runtime_api.h>
#include <thrust/device_ptr.h>
#include <thrust/reduce.h>
#include <thrust/transform.h>
#include <thrust/transform_reduce.h>
#include <thrust/iterator/transform_iterator.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/discard_iterator.h>
#include <thrust/logical.h>
#include <thrust/complex.h>
#include <sstream>
#include <string>
#include <algorithm>
#include <complex>
#include <chrono>
#include <pulse/helper/math.h>
#include <vector>
#include <pulse/signal/hilbert_fir_device.h>
#include <pulse/signal/hilbert_fir_host.h>


namespace pulse {

template<typename T>
struct modulus {
    __host__ __device__
    T operator()(const thrust::complex<T>& x) {return abs(x);}
};

template <typename T, typename U>
void magnitude(T input, size_t size, U output)
{
    thrust::transform(input, input + size, output, modulus<float>());
}

void magnitude_host(std::complex<float>* input, size_t size, float* output)
{
    magnitude((thrust::complex<float>*)input, size, output);
}

void magnitude_device(thrust::complex<float>* input, size_t size, float* output)
{
auto in_devPtr = thrust::device_pointer_cast(input);
auto out_devPtr = thrust::device_pointer_cast(output);

magnitude(in_devPtr, size, out_devPtr);
}


//template<typename T>
//struct compress20log10 {
//    const T normFactor_;
//    const T invNormFactor_;
//
//    compress20log10(T normFactor) : normFactor_(normFactor),
//                                    invNormFactor_(1 / normFactor) {};
//
//    __host__ __device__
//    T operator()(const T& env)
//    {
//        T _env = env > 0.f ? env : EPS_1;
//        return 20 * log10(_env / normFactor_);
////        return 20 * log10(_env * invNormFactor_);
//    }
//};

template<typename T>
struct compress20log10 {
    const T* normFactor_;

    compress20log10(T* normFactor) : normFactor_(normFactor) {};

    __host__ __device__
    T operator()(const T& env, const size_t batch_idx)
    {
        T _env = env > 0.f ? env : EPS_1;
        return 20 * log10(_env / normFactor_[batch_idx]);
    }
};

//template<typename T>
//struct variance {
//    const T mean_;
//    func(T mean) : mean_(mean) {};
//
//    __host__ __device__
//
//    T operator()(const T& env)
//    {
//        return (env - mean_) * (env - mean_);
//    }
//};

template<typename T>
struct is_zero {
    __host__ __device__
    bool operator()(T v) const { return v == T(0);}
};

//template<typename T>
//void env2bmode(
//        T env, size_t len_env, std::string& norm, float norm_factor, T bmode
//)
//{
//    // lowercase input string
//    std::transform(norm.begin(), norm.end(), norm.begin(),
//                   [](unsigned char c) {return std::tolower(c);});
//
////    std::chrono::duration<double> elapsed_seconds;
////    auto start = std::chrono::system_clock::now();
////    auto end = std::chrono::system_clock::now();
//
//    if (norm == std::string("max")) {
//        float max = thrust::reduce(
//                env, env + len_env, 0.f, thrust::maximum<float>()
//        );
//        norm_factor = max;
//    }
//    else if (norm == std::string("std")) {
//        float mean = thrust::reduce(
//                env, env + len_env, 0.f, thrust::plus<float>()
//        ) / len_env;
//
//        float var = thrust::transform_reduce(
//                env, env + len_env, variance<float>(mean), 0.f,
//                thrust::plus<float>()
//        ) / len_env;
//
//        float std_dev = std::sqrt(var);
//        norm_factor = std_dev;
//    }
//    else if (norm == std::string("factor")) {
//        if (norm_factor == 0.f) {
//            std::stringstream err_ss;
//            err_ss << "Normalization factor cannot be 0";
//            throw std::runtime_error(err_ss.str());
//        }
//    }
//    else {
//        std::stringstream err_ss;
//        err_ss << "Unsupported normalization " << norm;
//        throw std::runtime_error(err_ss.str());
//    }
//
////    end = std::chrono::system_clock::now();
////    elapsed_seconds = end - start;
////    std::cout << "custom elapsed time: " << elapsed_seconds.count() << "s" << std::endl;
//
////    start = std::chrono::system_clock::now();
//
//    // Normalize and log compress envelope
//    thrust::transform(
//            env, env + len_env, bmode,
//            compress20log10<float>(norm_factor)
//    );
//
////    end = std::chrono::system_clock::now();
////    elapsed_seconds = end - start;
////    std::cout << " transform elapsed time: " << elapsed_seconds.count() << "s" << std::endl;
//}

template<typename T>
void env2bmode(
        T env, size_t len_env, size_t nb_batches, std::string& norm,
        T norm_factors, T bmode
)
{
    // lowercase input string
    std::transform(norm.begin(), norm.end(), norm.begin(),
                   [](unsigned char c) {return std::tolower(c);});

//    std::chrono::duration<double> elapsed_seconds;
//    auto start = std::chrono::system_clock::now();
//    auto end = std::chrono::system_clock::now();

    int N = nb_batches;
    int K = len_env;

    auto keys_first = thrust::make_transform_iterator(
            thrust::make_counting_iterator(0),
            thrust::placeholders::_1 / K
    );
    auto keys_last = thrust::make_transform_iterator(
            thrust::make_counting_iterator(N * K),
            thrust::placeholders::_1 / K
    );

    // Normalization
    if (norm == std::string("max")) {

        // Reduce by key warning messages generated by nvcc compiler:
        // 'calling a __host__ function("thrust::detail::aligned_reinterpret_cast<int *, void *> ")
        // from a __host__ __device__ function [...] is not allowed'
        // This is a bug (https://github.com/thrust/thrust/issues/934) and has
        // been fixed in https://github.com/thrust/thrust/commit/a6d41f66efde6bf8d7220aa6013b5ffa8cd95122
        // This fix is not included in the CUDA 10.0 release so we have
        // to ignore the warnings
        thrust::reduce_by_key(
                keys_first,
                keys_last,
                env,
                thrust::make_discard_iterator(),
                norm_factors,
                thrust::equal_to<int>(),
                thrust::maximum<float>()
        );
    }
    else if (norm == std::string("factor")) {
        if (thrust::any_of(
                norm_factors, norm_factors + nb_batches, is_zero<float>()
        )) {
            std::stringstream err_ss;
            err_ss << "Normalization factor cannot be 0";
            throw std::runtime_error(err_ss.str());
        }
    }
    else {
        std::stringstream err_ss;
        err_ss << "Unsupported normalization " << norm;
        throw std::runtime_error(err_ss.str());
    }

//    end = std::chrono::system_clock::now();
//    elapsed_seconds = end - start;
//    std::cout << "custom elapsed time: " << elapsed_seconds.count() << "s" << std::endl;

//    start = std::chrono::system_clock::now();

    // Log compression
    thrust::transform(
            env, env + len_env * nb_batches,
            keys_first, bmode,
            compress20log10<float>(thrust::raw_pointer_cast(norm_factors))
    );

//    end = std::chrono::system_clock::now();
//    elapsed_seconds = end - start;
//    std::cout << " transform elapsed time: " << elapsed_seconds.count() << "s" << std::endl;
}

void env2bmode_host(
        float* env, size_t len_env, size_t nb_batches, std::string norm,
        float* norm_factors, float* bmode
)
{
    env2bmode(env, len_env, nb_batches, norm, norm_factors, bmode);
}

void env2bmode_device(
        float* env, size_t len_env, size_t nb_batches, std::string norm,
        float* norm_factors, float* bmode
)
{
    auto env_devPtr = thrust::device_pointer_cast(env);
    auto bmode_devPtr = thrust::device_pointer_cast(bmode);
    auto norm_dev = thrust::device_vector<float>(norm_factors, norm_factors + nb_batches);

    env2bmode(env_devPtr, len_env, nb_batches, norm, norm_dev.data(), bmode_devPtr);
}

template <typename T, typename U>
void iq2bmode(
        T iq_data, size_t len_data, size_t nb_batches, std::string norm,
        U norm_factors, U bmode
)
{
    // Compute the modulus to get the envelope
    magnitude(iq_data, len_data * nb_batches, bmode);

    // Compute log compression
    env2bmode(bmode, len_data, nb_batches, norm, norm_factors, bmode);
}

void iq2bmode_host(
        std::complex<float>* iq_data, size_t len_data, size_t nb_batches,
        std::string norm, float* norm_factors, float* bmode
)
{
    iq2bmode(iq_data, len_data, nb_batches, norm, norm_factors, bmode);
}

void iq2bmode_device(
        thrust::complex<float>* iq_data, size_t len_data, size_t nb_batches,
        std::string norm, float* norm_factors, float* bmode
)
{
    auto iq_devPtr = thrust::device_pointer_cast(iq_data);
    auto bmode_devPtr = thrust::device_pointer_cast(bmode);
    auto norm_dev = thrust::device_vector<float>(norm_factors, norm_factors + nb_batches);

    iq2bmode(iq_devPtr, len_data, nb_batches, norm, norm_dev.data(), bmode_devPtr);
}

void rf2bmode_host(
        float* rf_data, size_t len_data, size_t nb_batches,
        std::vector<std::complex<float>> fir_filter, std::string norm,
        float* norm_factors, float* bmode
)
{
    // Allocate intermediate iq data
    auto iq_data = std::vector<std::complex<float>>(len_data * nb_batches);

    // Compute Hilbert transform
    auto hilbert_transform = HilbertFIRHost(len_data, nb_batches, fir_filter);
    hilbert_transform.analytic_signal(rf_data, iq_data.data());

    // Compute B-mode
    iq2bmode(iq_data.data(), len_data, nb_batches, norm, norm_factors, bmode);
}

void rf2bmode_device(
        float* rf_data, size_t len_data, size_t nb_batches,
        std::vector<std::complex<float>> fir_filter, std::string norm,
        float* norm_factors, float* bmode
)
{
    // Allocate intermediate iq data
    auto iq_data = thrust::device_vector<thrust::complex<float>>(len_data * nb_batches);
    auto iq_data_ptr = thrust::raw_pointer_cast(iq_data.data());

    // Compute Hilbert transform
    auto hilbert_transform = HilbertFIRDevice(len_data, nb_batches, fir_filter);
    hilbert_transform.analytic_signal(rf_data, (std::complex<float>*)iq_data_ptr);

    // Compute B-mode
    auto bmode_devPtr = thrust::device_pointer_cast(bmode);
    auto norm_dev = thrust::device_vector<float>(norm_factors, norm_factors + nb_batches);

    iq2bmode(iq_data.data(), len_data, nb_batches, norm, norm_dev.data(), bmode_devPtr);
}

} // end namespace pulse
