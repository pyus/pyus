#include "pulse/signal/conv_direct_1d_host.h"
#include <iostream>
#include <cmath>
#include <complex>

namespace pulse {

//! Batched convolution of NxN, Nx1 or 1xN [inputs, filters]
template<typename T>
void ConvolutionDirect1DHost::convolve_full(
        const T* input, int input_size, int nb_inputs,
        const T* filter, int filter_size, int nb_filters,
        T* output)
{
    // assert correct shapes (either input and filters same size or one is 1)
    if (nb_inputs != nb_filters) {
        if ((nb_inputs != 1) && (nb_filters != 1)) {
            std::cerr << "Error: Shape mismatch, only NxN, 1xN and Nx1 batched convolutions are valid" << std::endl;
        }
    }

    // handle proper indices computation with offsets
    int offset_input = nb_inputs == 1 ? 0 : input_size;
    int offset_filter = nb_filters == 1 ? 0 : filter_size;

    int nb_conv = std::max(nb_inputs, nb_filters);

    int const output_size = input_size + filter_size - 1;

#pragma omp parallel for collapse(2)
    for (int r = 0; r < nb_conv; ++r) {
        for (int i = 0; i < output_size; ++i) {
            T sum = 0;
            int j_min = std::max(0, i - (filter_size - 1));
            int j_max = std::min(input_size - 1, i);
            for (int j = j_min; j <= j_max; ++j) {
                sum += (input[j + r * offset_input] * filter[i - j + r * offset_filter]);
            }
            output[i + r * output_size] = sum;
        }
    }
}


template<typename T>
void ConvolutionDirect1DHost::convolve_same(
        const T* input, int input_size, int nb_inputs,
        const T* filter, int filter_size, int nb_filters,
        T* output
)
{
    // The output will be the same size as the bigger input
    int in_gteq_filt = input_size >= filter_size;
    const T* signal = in_gteq_filt ? input : filter;
    const T* kernel = in_gteq_filt ? filter : input;
    int signal_size = in_gteq_filt ? input_size : filter_size;
    int kernel_size = in_gteq_filt ? filter_size : input_size;
    int nb_signals = in_gteq_filt ? nb_inputs : nb_filters;
    int nb_kernels = in_gteq_filt ? nb_filters : nb_inputs;

    // assert correct shapes (either input and filters same size or one is 1)
    if (nb_signals != nb_kernels) {
        if ((nb_signals != 1) && (nb_kernels != 1)) {
            std::cerr << "Error: Shape mismatch, only NxN, 1xN and Nx1 batched convolutions are valid" << std::endl;
        }
    }

    // handle proper indices computation with offsets
    int offset_signal = nb_signals == 1 ? 0 : signal_size;
    int offset_kernel = nb_kernels == 1 ? 0 : kernel_size;

    int nb_conv = std::max(nb_signals, nb_kernels);

    // Precompute some constants so we dont compute unnecessary integer divisions
    int kernel_size_2 = kernel_size / 2;
    int kernel_radius = (kernel_size - 1) / 2;
    int even_offset = (kernel_size + 1) % 2;

#pragma omp parallel for collapse(2)
    for (int r = 0; r < nb_conv; ++r) {
        for (int i = 0; i < signal_size; ++i) {
            T sum = 0;
            int j_min = std::max(0, i - kernel_radius - even_offset);
            int j_max = std::min(signal_size - 1,
                                 i + kernel_size_2 - even_offset);
            for (int j = j_min; j <= j_max; ++j) {
                sum += signal[j + r * offset_signal] *
                       kernel[i - j + kernel_size_2 - even_offset + r * offset_kernel];
            }
            output[i + r * signal_size] = sum;
        }
    }
}


template<typename T>
void ConvolutionDirect1DHost::convolve_valid(
        const T* input, int input_size, int nb_inputs,
        const T* filter, int filter_size, int nb_filters,
        T* output
)
{
    int in_gteq_filt = input_size >= filter_size;
    const T* signal = in_gteq_filt ? input : filter;
    const T* kernel = in_gteq_filt ? filter : input;
    int signal_size = in_gteq_filt ? input_size : filter_size;
    int kernel_size = in_gteq_filt ? filter_size : input_size;
    int output_size = signal_size - kernel_size + 1;
    int nb_signals = in_gteq_filt ? nb_inputs : nb_filters;
    int nb_kernels = in_gteq_filt ? nb_filters : nb_inputs;

    // assert correct shapes (either input and filters same size or one is 1)
    if (nb_signals != nb_kernels) {
        if ((nb_signals != 1) && (nb_kernels != 1)) {
            std::cerr << "Error: Shape mismatch, only NxN, 1xN and Nx1 batched convolutions are valid" << std::endl;
        }
    }

    // handle proper indices computation with offsets
    int offset_signal = nb_signals == 1 ? 0 : signal_size;
    int offset_kernel = nb_kernels == 1 ? 0 : kernel_size;

    int nb_conv = std::max(nb_signals, nb_kernels);

#pragma omp parallel for collapse(2)
    for (int r = 0; r < nb_conv; r++) {
        for (int i = 0; i < output_size; ++i) {
            T sum = 0;
            for (int j = kernel_size - 1, k = i; j >= 0; --j) {
                sum += signal[k + r * offset_signal] * kernel[j + r * offset_kernel];
                ++k;
            }
            output[i + r * output_size] = sum;
        }
    }
}

// Explicit instantiations for float and std::complex<float>
template void ConvolutionDirect1DHost::convolve_full<float>(const float*, int, int, const float*, int, int, float*);
template void ConvolutionDirect1DHost::convolve_same<float>(const float*, int, int, const float*, int, int, float*);
template void ConvolutionDirect1DHost::convolve_valid<float>(const float*, int, int, const float*, int, int, float*);
template void ConvolutionDirect1DHost::convolve_full<std::complex<float>>(const std::complex<float>*, int, int, const std::complex<float>*, int, int, std::complex<float>*);
template void ConvolutionDirect1DHost::convolve_same<std::complex<float>>(const std::complex<float>*, int, int, const std::complex<float>*, int, int, std::complex<float>*);
template void ConvolutionDirect1DHost::convolve_valid<std::complex<float>>(const std::complex<float>*, int, int, const std::complex<float>*, int, int, std::complex<float>*);

} // end namespace pulse
