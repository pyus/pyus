#include "pulse/signal/hilbert_fir_host.h"
#include <vector>
#include <complex>

namespace pulse {

HilbertFIRHost::HilbertFIRHost(
        int len_signal, int batch, int n_taps, float beta
): HilbertFIR(len_signal, batch, n_taps, beta)
{}

HilbertFIRHost::HilbertFIRHost(
        int len_signal, int batch,
        std::vector<std::complex<float>> fir_filter
): HilbertFIR(len_signal, batch, fir_filter)
{}

HilbertFIRHost::~HilbertFIRHost() = default;

void HilbertFIRHost::analytic_signal(float* x_mat, std::complex<float>* x_a_mat)
{
    // handle proper indices computation with offsets
    int offset_signal = batch_ == 1 ? 0 : len_signal_;

    // Precompute some constants so we dont compute unnecessary integer divisions
    int kernel_size_2 = n_taps_ / 2;
    int kernel_radius = (n_taps_ - 1) / 2;
    int even_offset = (n_taps_ + 1) % 2;

#pragma omp parallel for collapse(2)
    for (int r = 0; r < batch_; ++r) {
        for (int i = 0; i < len_signal_; ++i) {
            std::complex<float> sum = 0;
            int j_min = std::max(0, i - kernel_radius - even_offset);
            int j_max = std::min(len_signal_ - 1,
                                 i + kernel_size_2 - even_offset);
            for (int j = j_min; j <= j_max; ++j) {
                sum += x_mat[j + r * offset_signal] *
                       fir_filter_[i - j + kernel_size_2 - even_offset];
            }
            x_a_mat[i + r * len_signal_] = sum;
        }
    }
}

} // end namespace pulse
