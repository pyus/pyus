#include "pulse/signal/bmode_device.h"
#include "pulse/signal/hilbert_fft_device.h"
#include "../helper/cuda.h"
#include "pulse/helper/math.h"
#include <cuda_runtime_api.h>
#include <thrust/device_ptr.h>
#include <thrust/reduce.h>

namespace pulse {

__global__ void abs_kernel_(const cufftComplex* hilbert, float* env)
{
    const int threadID = blockIdx.x * blockDim.x + threadIdx.x;
    env[threadID] = length(hilbert[threadID]);
}

__global__ void logCompression_kernel_(float* bmode, float normalization)
{
    const int threadID = blockIdx.x * blockDim.x + threadIdx.x;
    bmode[threadID] = 20.f * log10f(bmode[threadID] / normalization);
}

__global__ void absLogCompression_kernel_(const cufftComplex* hilbert,
                                          float normalization,
                                          float* bmode)
{
    const int threadID = blockIdx.x * blockDim.x + threadIdx.x;

    float env = length(hilbert[threadID]);
    bmode[threadID] = 20.f * log10f(env / normalization);
}

BmodeDevice::BmodeDevice(Size3D size_image): Bmode(size_image)
{
    hilbert_ = new HilbertFFTDevice(size_image.z, size_image.x * size_image.y);

    checkCudaErrors(cudaMalloc((void**) &envelope_,
                               size_image.len * sizeof(cufftComplex)));

    checkCudaErrors(cudaMemset(envelope_, 0, size_image.len * sizeof(float)));
}

BmodeDevice::~BmodeDevice(){
    checkCudaErrors(cudaFree(envelope_));
}

void BmodeDevice::RF2Bmode(float* rf_image)
{
    // Compute envelope
    hilbert_->analytic_signal(rf_image, (std::complex<float>*) envelope_);

    // Compute rf_image = abs(envelope)
    abs_kernel_ <<<(this->size_image_.len + 255) / 256, 256>>>(envelope_, rf_image);

    thrust::device_ptr<float> devPtr = thrust::device_pointer_cast(rf_image);

    // Compute normalization_factor = max(abs(envelope))
    float normalization_factor = thrust::reduce(devPtr, devPtr + this->size_image_.len,
                                                0, thrust::maximum<float>());

    // Compute 20 * log10(abs(envelope) / normalization_factor)
    logCompression_kernel_ <<<(this->size_image_.len + 255) / 256, 256>>>(
            rf_image, normalization_factor);

    checkCudaErrors(cudaDeviceSynchronize());
    checkCudaErrors(cudaGetLastError());
}

void BmodeDevice::RF2Bmode(float* rf_image, float normalization_factor)
{
    // Compute envelope
    hilbert_->analytic_signal(rf_image, (std::complex<float>*) envelope_);

    // Compute 20 * log10(abs(envelope) / normalization_factor)
    absLogCompression_kernel_ <<<(this->size_image_.len + 255) / 256, 256>>>(
            envelope_, normalization_factor, rf_image);

    checkCudaErrors(cudaDeviceSynchronize());
    checkCudaErrors(cudaGetLastError());
}

} // end namespace pulse
