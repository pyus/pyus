include(${CMAKE_SOURCE_DIR}/cmake/select_compute_arch.cmake)
include(${CMAKE_SOURCE_DIR}/cmake/GetGitRevisionDescription.cmake)

# Get commit id
get_git_head_revision(GIT_REFSPEC GIT_SHA1)
git_describe(GIT_SHA1_SHORT --always)
#set(GIT_SHA1_SHORT ${GIT_SHA1})

# Get time stamp
string(TIMESTAMP TIME_STAMP)

# Configure a header file to pass some of the CMake settings to the source code:
# version, commit-id, time stamp
configure_file (
        "${CMAKE_SOURCE_DIR}/include/pulse/version.h.in"
        "${CMAKE_SOURCE_DIR}/include/pulse/version.h"
)

# Create target
add_library(pulse SHARED "")

# We rely on generated code for template explicit instantiations. To generate
# those, we define a custom command that will be run by a custom target at
# every build
# Files to generate
set(BMF_TEMPLATE_INSTANTIATIONS
        "${CMAKE_SOURCE_DIR}/beamformer/beamformer_instantiation.cpp"
        "${CMAKE_SOURCE_DIR}/beamformer/beamformer_host_instantiation.cpp"
        )

# Add CUDA implementations
list(APPEND BMF_TEMPLATE_INSTANTIATIONS
        "${CMAKE_SOURCE_DIR}/beamformer/beamformer_device_instantiation.cu")

set(INTERP_TEMPLATE_INSTANTIATIONS
        "${CMAKE_SOURCE_DIR}/interpolation/interpolation_instantiation.cpp"
        )

# Define commands
#https://stackoverflow.com/a/51881170
add_custom_command(
        OUTPUT ${BMF_TEMPLATE_INSTANTIATIONS}
        COMMAND ${Python_EXECUTABLE} ${CMAKE_SOURCE_DIR}/src/beamformer/generate_beamformer.py ARGS "${CMAKE_SOURCE_DIR}/src/beamformer"
        DEPENDS ${CMAKE_SOURCE_DIR}/src/beamformer/generate_beamformer.py
        COMMENT "Generating code for beamformer instantiations"
)

add_custom_command(
        OUTPUT ${INTERP_TEMPLATE_INSTANTIATIONS}
        COMMAND ${Python_EXECUTABLE} ${CMAKE_SOURCE_DIR}/src/interpolation/generate_interpolation.py ARGS "${CMAKE_SOURCE_DIR}/src/interpolation"
        DEPENDS ${CMAKE_SOURCE_DIR}/src/interpolation/generate_interpolation.py
        COMMENT "Generating code for interpolation instantiations"
)

# Define target that will run previous commands
add_custom_target(gen_template_instantiations DEPENDS ${BMF_TEMPLATE_INSTANTIATIONS} ${INTERP_TEMPLATE_INSTANTIATIONS})

# Add dependency on main target so that the files are generated before the build
add_dependencies(pulse gen_template_instantiations)

# Add CXX sources to target
target_sources(pulse
        PRIVATE
            # Beamformer
            ${CMAKE_SOURCE_DIR}/src/beamformer/beamformer.cpp
            ${CMAKE_SOURCE_DIR}/src/beamformer/beamformer_host.cpp
            # Interpolation
            ${CMAKE_SOURCE_DIR}/src/interpolation/prefilter_host.cpp
            ${CMAKE_SOURCE_DIR}/src/interpolation/interpolation.cpp
            # Signal
            ${CMAKE_SOURCE_DIR}/src/signal/conv_direct_1d_host.cpp
            ${CMAKE_SOURCE_DIR}/src/signal/hilbert.cpp
            ${CMAKE_SOURCE_DIR}/src/signal/hilbert_fft.cpp
            ${CMAKE_SOURCE_DIR}/src/signal/hilbert_fft_host.cpp
            ${CMAKE_SOURCE_DIR}/src/signal/hilbert_fir.cpp
            ${CMAKE_SOURCE_DIR}/src/signal/hilbert_fir_host.cpp
            ${CMAKE_SOURCE_DIR}/src/signal/bmode.cpp
            ${CMAKE_SOURCE_DIR}/src/signal/bmode_host.cpp
            ${CMAKE_SOURCE_DIR}/src/signal/kaiser.cpp

        PUBLIC
            # Beamformer
            ${CMAKE_SOURCE_DIR}/include/pulse/beamformer/beamformer.h
            ${CMAKE_SOURCE_DIR}/include/pulse/beamformer/beamformer_host.h
            # Delay
            ${CMAKE_SOURCE_DIR}/include/pulse/delay_law/delay_law.h
            # Interpolation
            ${CMAKE_SOURCE_DIR}/include/pulse/interpolation/prefilter.h
            ${CMAKE_SOURCE_DIR}/include/pulse/interpolation/interpolation.h
            ${CMAKE_SOURCE_DIR}/include/pulse/interpolation/interpolation.inl
            ${CMAKE_SOURCE_DIR}/include/pulse/interpolation/interp_common.h
            ${CMAKE_SOURCE_DIR}/include/pulse/interpolation/interp_common.inl
            ${CMAKE_SOURCE_DIR}/include/pulse/interpolation/interp_kernels.h
            ${CMAKE_SOURCE_DIR}/include/pulse/interpolation/interp_kernels.inl
            # Signal
            ${CMAKE_SOURCE_DIR}/include/pulse/signal/conv_direct_1d.h
            ${CMAKE_SOURCE_DIR}/include/pulse/signal/conv_direct_1d_host.h
            ${CMAKE_SOURCE_DIR}/include/pulse/signal/bmode.h
            ${CMAKE_SOURCE_DIR}/include/pulse/signal/bmode_host.h
            ${CMAKE_SOURCE_DIR}/include/pulse/signal/hilbert.h
            ${CMAKE_SOURCE_DIR}/include/pulse/signal/hilbert_fft.h
            ${CMAKE_SOURCE_DIR}/include/pulse/signal/hilbert_fft_host.h
            ${CMAKE_SOURCE_DIR}/include/pulse/signal/hilbert_fir.h
            ${CMAKE_SOURCE_DIR}/include/pulse/signal/hilbert_fir_host.h
            ${CMAKE_SOURCE_DIR}/include/pulse/signal/kaiser.h
            ${CMAKE_SOURCE_DIR}/include/pulse/signal/utils.h
            # Helpers
            ${CMAKE_SOURCE_DIR}/include/pulse/helper/functor.h
            ${CMAKE_SOURCE_DIR}/include/pulse/helper/math.h
            ${CMAKE_SOURCE_DIR}/include/pulse/helper/shape.h
            # Window
            ${CMAKE_SOURCE_DIR}/include/pulse/window/window_functor.h
            # Version
            ${CMAKE_SOURCE_DIR}/include/pulse/version.h
        )

# Add CUDA sources to target
target_sources(pulse
        PRIVATE
            # Beamformer
            ${CMAKE_SOURCE_DIR}/src/beamformer/beamformer_device.cu
            # Helper
            ${CMAKE_SOURCE_DIR}/src/helper/cuda.h
            # Interpolation
            ${CMAKE_SOURCE_DIR}/src/interpolation/prefilter_device.cu
            # Signal
            ${CMAKE_SOURCE_DIR}/src/signal/conv_direct_1d_device.cu
            ${CMAKE_SOURCE_DIR}/src/signal/bmode_device.cu
            ${CMAKE_SOURCE_DIR}/src/signal/hilbert_fft_device.cu
            ${CMAKE_SOURCE_DIR}/src/signal/hilbert_fir_device.cu
            ${CMAKE_SOURCE_DIR}/src/signal/utils.cu

        PUBLIC
            # Beamformer
            ${CMAKE_SOURCE_DIR}/include/pulse/beamformer/beamformer_device.h
            # Signal
            ${CMAKE_SOURCE_DIR}/include/pulse/signal/conv_direct_1d_device.h
            ${CMAKE_SOURCE_DIR}/include/pulse/signal/bmode_device.h
            ${CMAKE_SOURCE_DIR}/include/pulse/signal/hilbert_fft_device.h
            ${CMAKE_SOURCE_DIR}/include/pulse/signal/hilbert_fir_device.h
        )

# contains useful helpers
set(CUDA_SAMPLES_INCLUDE_DIRECTORIES ${CMAKE_CUDA_TOOLKIT_INCLUDE_DIRECTORIES}/../../../samples/common/inc)

target_include_directories(pulse
        PUBLIC
            $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/include>
            $<INSTALL_INTERFACE:include>
        PRIVATE
            ${CMAKE_SOURCE_DIR}/src
            ${CMAKE_CUDA_TOOLKIT_INCLUDE_DIRECTORIES})

set_target_properties(pulse PROPERTIES
        CUDA_SEPARABLE_COMPILATION ON
        )

# Get common GPU archs
set(CUDA_ARCH Common)
cuda_select_nvcc_arch_flags(NVCC_ARCH_FLAGS ${CUDA_ARCH})
# Join the different arch flags in one string for the SHELL: prefix to work on
# all architectures (instead of adding it for each arch)
list(JOIN NVCC_ARCH_FLAGS " " NVCC_ARCH_FLAGS)

# Gather CUDA libs
set(CUDA_LINK_LIBRARIES "-lcufft -lcuda")

set_target_properties(pulse PROPERTIES
        DEBUG_POSTFIX -debug
        VERSION "${pulse_VERSION}-${GIT_SHA1_SHORT}")
#        SOVERSION ${pulse_VERSION}) # controls the soname of the library as well as the 2nd symlink: libpulse.so -> libpulse.so.SOVERSION -> libpulse.so.VERSION

# Define variables for generator expressions
set(CXX_LANG "$<COMPILE_LANGUAGE:CXX>")
set(CUDA_LANG "$<COMPILE_LANGUAGE:CUDA>")
set(RELEASE_CXX_LANG "$<AND:$<CONFIG:RELEASE>,${CXX_LANG}>")
set(RELEASE_CUDA_LANG "$<AND:$<CONFIG:RELEASE>,${CUDA_LANG}>")
set(DEBUG_CXX_LANG "$<AND:$<CONFIG:DEBUG>,${CXX_LANG}>")
set(DEBUG_CUDA_LANG "$<AND:$<CONFIG:DEBUG>,${CUDA_LANG}>")

target_compile_options(pulse PRIVATE
        # Build flags for all CXX builds
        $<$<BUILD_INTERFACE:${CXX_LANG}>:-Wall;-fopenmp>
        # Build flags for all CUDA builds
        $<$<BUILD_INTERFACE:${CUDA_LANG}>:-Xcompiler=-Wall,-fopenmp> #--default-stream per-thread
        $<$<BUILD_INTERFACE:${CUDA_LANG}>:SHELL:${NVCC_ARCH_FLAGS}>

        # Build flags for CXX release build
        $<$<BUILD_INTERFACE:${RELEASE_CXX_LANG}>:-Wno-unknown-pragmas;-Wno-unused-function>
        # Build flags for CUDA release build
        $<$<BUILD_INTERFACE:${RELEASE_CUDA_LANG}>:-lineinfo;-use_fast_math;-Xcompiler=-Wno-unknown-pragmas,-Wno-unused-function>

        # Build flags for CXX debug build
        $<$<BUILD_INTERFACE:${DEBUG_CXX_LANG}>:-O0;-DDEBUG_BUILD>
        # Build flags for CUDA debug build
        $<$<BUILD_INTERFACE:${DEBUG_CUDA_LANG}>:-G;-O0>
        )

#target_link_libraries(pulse -lgomp -lm -lcufft -lfftw3f -lfftw3f_omp)
target_link_libraries(pulse -lgomp -lm ${CUDA_LINK_LIBRARIES} ${FFTW_LIBRARIES})

# TODO: EXPORT
