#include "pulse/interpolation/prefilter.h"
#include "pulse/interpolation/prefilter.inl"
#include "pulse/interpolation/interp_common.h"
#include <complex>
#include <cuda_runtime_api.h>

namespace pulse {

template<InterpScheme scheme>
void prefilterChannelHost(float* raw_data, int nb_elem, int nb_samples,
                          char bc_type)
{
#pragma omp parallel for
    for (int i = 0; i < nb_elem; ++i) {
        float* channel = &raw_data[i * nb_samples];
        samplesToCoeffs<float, scheme>(channel, nb_samples, bc_type);
    }
}

template<InterpScheme scheme>
void prefilterChannelHost(std::complex<float>* raw_data, int nb_elem,
                          int nb_samples, char bc_type
)
{
#pragma omp parallel for
    for (int i = 0; i < nb_elem; ++i) {
        float2* raw_data_f2 = (float2*)raw_data;
        float2* channel = &raw_data_f2[i * nb_samples];
        samplesToCoeffs<float2, scheme>(channel, nb_samples, bc_type);
    }
}

// Template specialization with empty function when no prefiltering is required
template<> void prefilterChannelHost<NEAREST>(float*, int, int, char){}
template<> void prefilterChannelHost<LINEAR>(float*, int, int, char){}
template<> void prefilterChannelHost<KEYS>(float*, int, int, char){}
template<> void prefilterChannelHost<NEAREST>(std::complex<float>*, int, int, char){}
template<> void prefilterChannelHost<LINEAR>(std::complex<float>*, int, int, char){}
template<> void prefilterChannelHost<KEYS>(std::complex<float>*, int, int, char){}

// Explicit instantiations of the remaining interpolation schemes
template void prefilterChannelHost<BSPLINE_3>(float*, int, int, char);
template void prefilterChannelHost<BSPLINE_4>(float*, int, int, char);
template void prefilterChannelHost<BSPLINE_5>(float*, int, int, char);
template void prefilterChannelHost<OMOMS_3>(float*, int, int, char);
template void prefilterChannelHost<BSPLINE_3>(std::complex<float>*, int, int, char);
template void prefilterChannelHost<BSPLINE_4>(std::complex<float>*, int, int, char);
template void prefilterChannelHost<BSPLINE_5>(std::complex<float>*, int, int, char);
template void prefilterChannelHost<OMOMS_3>(std::complex<float>*, int, int, char);

} // end namespace pulse
