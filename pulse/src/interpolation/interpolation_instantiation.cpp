// File automatically generated with generate_interpolation.py

#include "pulse/interpolation/interpolation.h"
#include "pulse/interpolation/interp_common.h"


namespace pulse {

template struct InterpFunctor<NEAREST>;
template struct InterpFunctor<LINEAR>;
template struct InterpFunctor<KEYS>;
template struct InterpFunctor<BSPLINE_3>;
template struct InterpFunctor<BSPLINE_4>;
template struct InterpFunctor<BSPLINE_5>;
template struct InterpFunctor<OMOMS_3>;

} // end namespace pulse
