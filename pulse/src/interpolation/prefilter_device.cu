#include "pulse/interpolation/prefilter.h"
#include "pulse/interpolation/prefilter.inl"
#include "pulse/helper/math.h"
#include "../helper/cuda.h"
#include "pulse/interpolation/interp_common.h"
#include <thrust/complex.h>
#include <cuda_runtime.h>

namespace pulse {

template<typename T, InterpScheme scheme>
__global__
void prefilteringKernel(T* raw_data, int nb_elem, int nb_samples,
                        char bc_type)
{
    // process channel in z-direction
    int x = blockIdx.y * blockDim.y + threadIdx.y;
    if (x < nb_elem) {
        T* channel = &raw_data[x * nb_samples];
        samplesToCoeffs<T, scheme>(channel, nb_samples, bc_type);
    }
}

template<InterpScheme scheme>
void prefilterChannelDevice(float* raw_data, int nb_elem, int nb_samples,
                            char bc_type, cudaStream_t stream)
{
//    dim3 dimBlock(1, std::min(PowTwoDivider(nb_elem), 64), 1);
    dim3 dimBlock(1, 1, 1);
    dim3 dimGrid(1, (nb_elem + dimBlock.y - 1) / dimBlock.y);

    void* args[] = {&raw_data, &nb_elem, &nb_samples, &bc_type};
    checkCudaErrors(cudaLaunchKernel(
            (void*)prefilteringKernel<float, scheme>, dimGrid, dimBlock, args
    ));
//    prefilteringKernel<scheme> <<< dimGrid, dimBlock, 0, stream >>> (
//            raw_data, nb_elem, nb_samples, bc_type);
//    checkCudaErrors(cudaDeviceSynchronize());
//    checkCudaErrors(cudaGetLastError());
}

template<InterpScheme scheme>
void prefilterChannelDevice(thrust::complex<float>* raw_data, int nb_elem,
                            int nb_samples, char bc_type, cudaStream_t stream)
{
//    dim3 dimBlock(1, std::min(PowTwoDivider(nb_elem), 64), 1);
    dim3 dimBlock(1, 1, 1);
    dim3 dimGrid(1, (nb_elem + dimBlock.y - 1) / dimBlock.y);

    // Cast complex to float2 for the prefiltering step to apply identically
    // to the real and complex component. We want to use real arithmetics on both
    // channels instead of complex
    float2* raw_data_f2 = (float2*)raw_data;
    void* args[] = {&raw_data_f2, &nb_elem, &nb_samples, &bc_type};
    checkCudaErrors(cudaLaunchKernel(
            (void*)prefilteringKernel<float2, scheme>, dimGrid, dimBlock, args
    ));
//    prefilteringKernel<scheme> <<< dimGrid, dimBlock, 0, stream >>> (
//            raw_data, nb_elem, nb_samples, bc_type);
//    checkCudaErrors(cudaDeviceSynchronize());
//    checkCudaErrors(cudaGetLastError());
}

// Template specialization with empty function when no prefiltering is required
template<> void prefilterChannelDevice<NEAREST>(float*, int, int, char, cudaStream_t){}
template<> void prefilterChannelDevice<LINEAR>(float*, int, int, char, cudaStream_t){}
template<> void prefilterChannelDevice<KEYS>(float*, int, int, char, cudaStream_t){}
template<> void prefilterChannelDevice<NEAREST>(thrust::complex<float>*, int, int, char, cudaStream_t){}
template<> void prefilterChannelDevice<LINEAR>(thrust::complex<float>*, int, int, char, cudaStream_t){}
template<> void prefilterChannelDevice<KEYS>(thrust::complex<float>*, int, int, char, cudaStream_t){}

// Explicit instantiations of the remaining interpolation schemes
template void prefilterChannelDevice<BSPLINE_3>(float*, int, int, char, cudaStream_t);
template void prefilterChannelDevice<BSPLINE_4>(float*, int, int, char, cudaStream_t);
template void prefilterChannelDevice<BSPLINE_5>(float*, int, int, char, cudaStream_t);
template void prefilterChannelDevice<OMOMS_3>(float*, int, int, char, cudaStream_t);
template void prefilterChannelDevice<BSPLINE_3>(thrust::complex<float>*, int, int, char, cudaStream_t);
template void prefilterChannelDevice<BSPLINE_4>(thrust::complex<float>*, int, int, char, cudaStream_t);
template void prefilterChannelDevice<BSPLINE_5>(thrust::complex<float>*, int, int, char, cudaStream_t);
template void prefilterChannelDevice<OMOMS_3>(thrust::complex<float>*, int, int, char, cudaStream_t);

} // end namespace pulse
