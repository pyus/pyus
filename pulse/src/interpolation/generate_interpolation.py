import sys
import os

base_path = sys.argv[1]

interp = ["NEAREST", "LINEAR", "KEYS", "BSPLINE_3", "BSPLINE_4", "BSPLINE_5", "OMOMS_3"]

interp_functor = "template struct InterpFunctor<{T}>;\n"

file = open(os.path.join(base_path, "interpolation_instantiation.cpp"), "w")
file.write("// File automatically generated with generate_interpolation.py\n\n")

file.write('#include "pulse/interpolation/interpolation.h"\n'
           '#include "pulse/interpolation/interp_common.h"\n'
           '\n\n'
           'namespace pulse {\n'
           '\n')

for t in interp:
    file.write(interp_functor.format(T=t))

file.write('\n} // end namespace pulse\n')

file.close()
