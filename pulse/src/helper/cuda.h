#ifndef PULSE_CUDA_H
#define PULSE_CUDA_H

#include <cuda.h>
#include <cuda_runtime.h>
#include <cufft.h>
#include <dlfcn.h>
#include <iostream>
#include <sstream>

namespace pulse {

// CUDA Runtime error messages
static const char* _cudaGetErrorEnum(cudaError_t error) {
    return cudaGetErrorName(error);
}

// CUDA Driver API errors
static const char* _cudaGetErrorEnum(CUresult error) {
  static char unknown[] = "<unknown>";
  const char* ret = nullptr;
  cuGetErrorName(error, &ret);
  return ret ? ret : unknown;
}

// cuFFT API errors
static const char* _cudaGetErrorEnum(cufftResult error) {
  switch (error) {
    case CUFFT_SUCCESS:
      return "CUFFT_SUCCESS";

    case CUFFT_INVALID_PLAN:
      return "CUFFT_INVALID_PLAN";

    case CUFFT_ALLOC_FAILED:
      return "CUFFT_ALLOC_FAILED";

    case CUFFT_INVALID_TYPE:
      return "CUFFT_INVALID_TYPE";

    case CUFFT_INVALID_VALUE:
      return "CUFFT_INVALID_VALUE";

    case CUFFT_INTERNAL_ERROR:
      return "CUFFT_INTERNAL_ERROR";

    case CUFFT_EXEC_FAILED:
      return "CUFFT_EXEC_FAILED";

    case CUFFT_SETUP_FAILED:
      return "CUFFT_SETUP_FAILED";

    case CUFFT_INVALID_SIZE:
      return "CUFFT_INVALID_SIZE";

    case CUFFT_UNALIGNED_DATA:
      return "CUFFT_UNALIGNED_DATA";

    case CUFFT_INCOMPLETE_PARAMETER_LIST:
      return "CUFFT_INCOMPLETE_PARAMETER_LIST";

    case CUFFT_INVALID_DEVICE:
      return "CUFFT_INVALID_DEVICE";

    case CUFFT_PARSE_ERROR:
      return "CUFFT_PARSE_ERROR";

    case CUFFT_NO_WORKSPACE:
      return "CUFFT_NO_WORKSPACE";

    case CUFFT_NOT_IMPLEMENTED:
      return "CUFFT_NOT_IMPLEMENTED";

    case CUFFT_LICENSE_ERROR:
      return "CUFFT_LICENSE_ERROR";

    case CUFFT_NOT_SUPPORTED:
      return "CUFFT_NOT_SUPPORTED";
  }

  return "<unknown>";
}

//template <typename T>
//void check(T result, char const *const func, const char *const file,
//           int const line) {
//    if (result) {
//        fprintf(stderr, "CUDA error at %s:%d code=%d(%s) \"%s\" \n", file, line,
//                static_cast<unsigned int>(result), _cudaGetErrorEnum(result), func);
//        cudaDeviceReset();
//        // Make sure we call CUDA Device Reset before exiting
//        exit(EXIT_FAILURE);
//    }
//}

// TODO | checkCudaErrors prints file and line where error happened, we should
// TODO | use it in debug mode but remove file and line info in release
// TODO | See if we use thrust or https://github.com/eyalroz/cuda-api-wrappers/blob/master/examples/by_runtime_api_module/error_handling.cu
// TODO | for C++ style error handling

template <typename T>
void check_debug(T result, char const *const func, const char *const file,
                 int const line) {
    if (result) {
        std::stringstream what_arg;
        what_arg << "CUDA error at " << file << ":" << line << " code=" <<
                 static_cast<unsigned int>(result) << "(" <<
                 _cudaGetErrorEnum(result) << ")" << "\n '" << func << "'";
        // Make sure we call CUDA Device Reset before exiting
        cudaDeviceReset();
        throw std::runtime_error(what_arg.str());
    }
}

template <typename T>
void check_release(T result, char const *const func) {
    if (result) {
        std::stringstream what_arg;
        what_arg << "CUDA error with code=" <<
                 static_cast<unsigned int>(result) << "(" <<
                 _cudaGetErrorEnum(result) << ")" << "\n '" << func << "'";
        // Make sure we call CUDA Device Reset before exiting
        cudaDeviceReset();
        throw std::runtime_error(what_arg.str());
    }
}


#ifdef DEBUG_BUILD
#define checkCudaErrors(val) check_debug((val), #val, __FILE__, __LINE__)
#else
#define checkCudaErrors(val) check_release((val), #val)
#endif

inline int nvidia_driver_version()
{
    int (*nvml_init)()  = nullptr;
    int (*nvml_finish)()  = nullptr;
    int (*nvml_driver)(char *f, unsigned int len) = nullptr;
    int dri_ver  = 0;
    void *handle = nullptr;
    char driver_string[81];

    handle  = dlopen("libnvidia-ml.so.1", RTLD_NOW);
    if (!handle) {
        handle  = dlopen("libnvidia-ml.so", RTLD_NOW);
        if (!handle) {
            goto end;
        }
    }

    nvml_driver = (int(*)(char *, unsigned int)) dlsym(handle,  "nvmlSystemGetDriverVersion");
    nvml_init = (int(*)(void)) dlsym(handle,  "nvmlInit");
    nvml_finish = (int(*)(void)) dlsym(handle,  "nvmlShutdown");
    if (!nvml_driver || !nvml_init || !nvml_finish) goto end;

    if (nvml_init()) goto end;
    if (nvml_driver(driver_string, 80)) goto end;
    dri_ver = (int) (100. * atof(driver_string));

    end:
    if (nvml_finish) nvml_finish();
    if (handle) dlclose(handle);
    return dri_ver;
}

} // end namespace pulse

#endif //PULSE_CUDA_H
