import sys
import os
import itertools
import math

base_path = sys.argv[1]

data_type_cpu = ["float", "std::complex<float>"]
data_type_gpu = ["float", "thrust::complex<float>"]
delay = ["PlaneWave2D", "PlaneWave3D", "DivergingWave"]
interp = ["NEAREST", "LINEAR", "KEYS", "BSPLINE_3", "BSPLINE_4", "BSPLINE_5", "OMOMS_3"]
apod = ["Selfridge2D", "Selfridge3D", "Boxcar", "Hanning", "Hamming", "Tukey", "IdentityWindow"]

bmf_base = "template void Beamformer::reconstruct<{T}, InterpFunctor<{U}>, {V}>(const float*, int, std::vector<{T}>, InterpFunctor<{U}>, {V}, float*);\n"

bmf_host = "template void BeamformerHost::reconstruct<{type}, {T}, InterpFunctor<{U}>, {V}>(const {type}*, int, std::vector<{T}>, InterpFunctor<{U}>, {V}, {type}*);\n"

bmf_device = "template void BeamformerDevice::reconstruct<{type}, {T}, InterpFunctor<{U}>, {V}>(const {type}*, int, std::vector<{T}>, InterpFunctor<{U}>, {V}, {type}*);\n"

# Base class instantiations
file = open( os.path.join(base_path, "beamformer_instantiation.cpp"), "w")
file.write("// File automatically generated with generate_beamformer.py\n\n")

file.write('#include "pulse/beamformer/beamformer.h"\n'
           '#include "pulse/delay_law/delay_law.h"\n'
           '#include "pulse/window/window_functor.h"\n'
           '#include "pulse/interpolation/interpolation.h"\n'
           '\n\n'
           'namespace pulse {\n'
           '\n')

for (t, u, v) in itertools.product(delay, interp, apod):
    file.write(bmf_base.format(T=t, U=u, V=v))

file.write('\n} // end namespace pulse\n')

file.close()

# Host beamformer instantiations
file = open( os.path.join(base_path, "beamformer_host_instantiation.cpp"), "w")
file.write("// File automatically generated with generate_beamformer.py\n\n")

file.write('#include "pulse/beamformer/beamformer_host.h"\n'
           '#include "pulse/delay_law/delay_law.h"\n'
           '#include "pulse/window/window_functor.h"\n'
           '#include "pulse/interpolation/interpolation.h"\n'
           '#include <complex>\n'
           '\n\n'
           'namespace pulse {\n'
           '\n')

for (type, t, u, v) in itertools.product(data_type_cpu, delay, interp, apod):
    file.write(bmf_host.format(type=type, T=t, U=u, V=v))

file.write('\n} // end namespace pulse\n')

file.close()

# Device beamformer instantiations
file = open( os.path.join(base_path, "beamformer_device_instantiation.cu"), "w")
file.write("// File automatically generated with generate_beamformer.py\n\n")

file.write('#include "pulse/beamformer/beamformer_device.h"\n'
           '#include "pulse/delay_law/delay_law.h"\n'
           '#include "pulse/window/window_functor.h"\n'
           '#include "pulse/interpolation/interpolation.h"\n'
           '#include <thrust/complex.h>\n'
           '\n\n'
           'namespace pulse {\n'
           '\n')

for (type, t, u, v) in itertools.product(data_type_gpu, delay, interp, apod):
    file.write(bmf_device.format(type=type, T=t, U=u, V=v))

file.write('\n} // end namespace pulse\n')

file.close()
