#include "pulse/beamformer/beamformer_host.h"
#include "pulse/interpolation/prefilter.h"
#include <vector>
#include <iostream>
#include <chrono>

namespace pulse {

// Constructor
BeamformerHost::BeamformerHost(
        std::vector<float> element_positions,
        std::vector<float> t_axis,
        std::vector<float> x_im_axis,
        std::vector<float> y_im_axis,
        std::vector<float> z_im_axis)
        :Beamformer(
        element_positions, t_axis,
        x_im_axis, y_im_axis, z_im_axis)
{}


// Destructor
BeamformerHost::~BeamformerHost() = default;

template<typename T, typename Delay, typename Interp, typename Apod>
void BeamformerHost::reconstruct(const T* input_raw_data, int frame_number,
                                 std::vector<Delay> delays, Interp interp, Apod apod,
                                 T* rf_image)
{
//    auto start = std::chrono::system_clock::now();

    shape_rawdata_.event_number = delays.size();
    shape_rawdata_.frame_number = frame_number;
    shape_image_.frame_number = shape_rawdata_.frame_number;

    // Copy data since they will be prefiltered
    auto raw_data = new std::vector<T>(input_raw_data, input_raw_data + shape_rawdata_.len());

    // Prefilter data to compute spline coefficients ck from data samples fk
    char bc = 'm';
    prefilterChannelHost<Interp::getScheme()>(
            raw_data->data(),
            shape_rawdata_.element_number * shape_rawdata_.event_number * shape_rawdata_.frame_number,
            shape_rawdata_.sample_number, bc);

    // Loop on frames
    for (size_t frame = 0; frame < shape_rawdata_.frame_number; frame++) {

        // Loop on events
        for (size_t event = 0; event < shape_rawdata_.event_number; ++event) {

            // Get the corresponding delay law
            Delay delay = delays[event];

            // Get a pointer to the acquisition corresponding to the current event
            T* raw_data_ptr = raw_data->data() +
                                   shape_rawdata_.element_number *
                                   shape_rawdata_.sample_number *
                                   (event + shape_rawdata_. event_number * frame);

// Parallelize the 3 nested loops
#pragma omp parallel for collapse(3)

            // Loop on image pixels
            for (unsigned y_idx = 0; y_idx < shape_image_.pixels_y; ++y_idx) {
                for (unsigned x_idx = 0; x_idx < shape_image_.pixels_x; ++x_idx) {
                    for (unsigned z_idx = 0; z_idx < shape_image_.pixels_z; ++z_idx) {

                        float3 img_point = make_float3(this->x_im_axis_[x_idx],
                                                       this->y_im_axis_[y_idx],
                                                       this->z_im_axis_[z_idx]);
                        T sum = 0.f;

                        // Loop on elements
                        for (size_t el = 0; el < this->el_pos_.size(); ++el) {

                            float3 el_pos = this->el_pos_[el];

                            // Time of flight
                            float t_tot = delay(img_point, el_pos);

                            // Interpolation
                            T interp_val = interp(t_tot, raw_data_ptr, el);

                            // Apodization
                            float apod_factor = apod(img_point, el_pos);

                            // Sum
                            sum += interp_val * apod_factor;
                        }

                        size_t out_idx = z_idx + shape_image_.pixels_z *
                                         (x_idx + shape_image_.pixels_y *
                                         (y_idx + shape_image_.pixels_x * frame));

                        rf_image[out_idx] += sum;
                    }
                }
            }
        }
    }

//    auto end = std::chrono::system_clock::now();
//    std::chrono::duration<double> elapsed_seconds = end-start;
//    std::cout << "C++ elapsed time: " << elapsed_seconds.count() << "s" << std::endl;
}

} // end namespace pulse

#include "beamformer_host_instantiation.cpp"
