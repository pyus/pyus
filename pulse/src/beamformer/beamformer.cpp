#include "pulse/beamformer/beamformer.h"
#include "pulse/helper/math.h"
#include <iostream>

namespace pulse {

Beamformer::Beamformer(
        std::vector<float> element_positions,
        std::vector<float> t_axis,
        std::vector<float> x_im_axis,
        std::vector<float> y_im_axis,
        std::vector<float> z_im_axis):
        size_image_(x_im_axis.size(), y_im_axis.size(), z_im_axis.size()),
        t_axis_(t_axis),
        x_im_axis_(x_im_axis), y_im_axis_(y_im_axis), z_im_axis_(z_im_axis)
{
    auto nb_el = element_positions.size() / 3;
    this->el_pos_ = std::vector<float3>(nb_el);
    for (int el = 0; el < nb_el; ++el) {
        this->el_pos_[el] = make_float3(element_positions[3 * el],
                                        element_positions[3 * el + 1],
                                        element_positions[3 * el + 2]);
    }

    shape_rawdata_ = {1, 1, nb_el, t_axis.size()};
    shape_image_ = {1, x_im_axis.size(), y_im_axis.size(), z_im_axis.size()};
}

Beamformer::~Beamformer() = default;

template<typename Delay, typename Interp, typename Apod>
void Beamformer::reconstruct(const float* raw_data, int frame_number,
                             std::vector<Delay> delays, Interp interp, Apod apod,
                             float* rf_image)
{
    std::cerr << "Error: Not Implemented, this is a base class, use one of the derived class instead" << std::endl;
    throw;
}


} // end namespace pulse

#include "beamformer_instantiation.cpp"
