#include "pulse/beamformer/beamformer_device.h"
#include "pulse/interpolation/prefilter.h"
#include "pulse/helper/math.h"
#include "../helper/cuda.h"
#include <thrust/device_vector.h>
#include <cuda_runtime.h>
#include <chrono>
#include <cmath>


namespace pulse {

// Constructor
BeamformerDevice::BeamformerDevice(
        std::vector<float> element_positions,
        std::vector<float> t_axis,
        std::vector<float> x_im_axis,
        std::vector<float> y_im_axis,
        std::vector<float> z_im_axis)
        :Beamformer(
        element_positions, t_axis,
        x_im_axis, y_im_axis, z_im_axis)
{
    x_im_axis_d_ = thrust::device_vector<float>(x_im_axis_);
    y_im_axis_d_ = thrust::device_vector<float>(y_im_axis_);
    z_im_axis_d_ = thrust::device_vector<float>(z_im_axis_);
    el_pos_d_ = thrust::device_vector<float3>(el_pos_);
    t_axis_d_ = thrust::device_vector<float>(t_axis_);
    raw_data_d_1 = thrust::device_vector<float>(shape_rawdata_.len(), 0);
    raw_data_d_2 = thrust::device_vector<float>(shape_rawdata_.len(), 0);
}

// Destructor
BeamformerDevice::~BeamformerDevice() = default;

// In the following kernels, the axis convention changes between ultrasound
// imaging and block/thread as follows:
// ─────────────────────────────────────────────────────────────────
//   Image in US  |  Memory layout (col major like)  |  Block axes
// ─────────────────────────────────────────────────────────────────
//     y          |         y                        |     z
//    /           |        /                         |    /
//   /            |       /                          |   /
//  +─────── x    |      +─────── z                  |  +─────── x
//  |             |      |                           |  |
//  |             |      |                           |  |
//  |             |      |                           |  |
//  z             |      x                           |  y

template<typename T, typename Delay, typename Interp, typename Apod>
__global__
void reconstruct_kernel(const T* __restrict__ raw_data,
                        const float* __restrict__ x_im_axis, int len_xim,
                        const float* __restrict__ y_im_axis, int len_yim,
                        const float* __restrict__ z_im_axis, int len_zim,
                        const float3* __restrict__ td_pos, int len_td,
                        Delay delay, Interp interp, Apod apod,
                        T* __restrict__ rf_image)
{
    const int3 tid_3D = make_int3(blockIdx.x * blockDim.x + threadIdx.x,
                                  blockIdx.y * blockDim.y + threadIdx.y,
                                  blockIdx.z * blockDim.z + threadIdx.z);

    const int tid_1D = tid_3D.x + len_zim * (tid_3D.y + len_xim * tid_3D.z);


    // Avoid out of range memory accesses
    if (tid_3D.x >= len_zim || tid_3D.y >= len_xim || tid_3D.z >= len_yim)
        return;

    float3 img_point = make_float3(x_im_axis[tid_3D.y],
                                   y_im_axis[tid_3D.z],
                                   z_im_axis[tid_3D.x]);

    // Temporary sum
    T sum = 0.f;

    // Loop on transducers
    for (unsigned td = 0; td < len_td; ++td) {

        // TODO: this one could be on shared memory
        float3 xdcr = td_pos[td];

        // Time of flight
        float t_tot = delay(img_point, xdcr);

        // Interpolation
        T interp_val = interp(t_tot, raw_data, td);

        // Apodization
        float apod_factor = apod(img_point, xdcr);

        // Sum
        sum += interp_val * apod_factor;
    }
    rf_image[tid_1D] += sum;
}

template<typename T, typename Delay, typename Interp, typename Apod>
void BeamformerDevice::reconstruct(
        const T* raw_data, int frame_number, std::vector<Delay> delays,
        Interp interp, Apod apod, T* rf_image)
{
    // Since data is const T*, we need to copy it as we will modify it in the
    // prefiltering step
    // TODO: Test if more efficient to copy the whole data and process it (prefilter + DAS)

//    auto start = std::chrono::system_clock::now();

    shape_rawdata_.event_number = delays.size();
    shape_rawdata_.frame_number = frame_number;
    shape_image_.frame_number = shape_rawdata_.frame_number;
    size_t event_size = shape_rawdata_.element_number *
                        shape_rawdata_.sample_number;

    // Get raw pointers to pass to kernel launch
    float3* el_pos_ptr = thrust::raw_pointer_cast(el_pos_d_.data());
    float* x_im_axis_ptr = thrust::raw_pointer_cast(x_im_axis_d_.data());
    float* y_im_axis_ptr = thrust::raw_pointer_cast(y_im_axis_d_.data());
    float* z_im_axis_ptr = thrust::raw_pointer_cast(z_im_axis_d_.data());

    // Erase output since we do not rewrite but add values to it
    checkCudaErrors(cudaMemset(
            rf_image, 0, shape_image_.pixels_xyz() * sizeof(float)
    ));

    // Create 2 streams, one for copy and one for computation
    cudaStream_t stream[2];
    cudaStreamCreate(&stream[0]);
    cudaStreamCreate(&stream[1]);

    // Use 2 tmp arrays, one for copy and one for computation
    T* raw_data_tmp[2];
//    raw_data_tmp[0] = thrust::raw_pointer_cast(raw_data_d_1.data());
//    raw_data_tmp[1] = thrust::raw_pointer_cast(raw_data_d_2.data());
    auto raw_data_thrust_1 = thrust::device_vector<T>(event_size);
    auto raw_data_thrust_2 = thrust::device_vector<T>(event_size);
    raw_data_tmp[0] = thrust::raw_pointer_cast(raw_data_thrust_1.data());
    raw_data_tmp[1] = thrust::raw_pointer_cast(raw_data_thrust_2.data());

    //Set reasonable block size (i.e., number of threads per block)
    const dim3 block_size(512, 1, 1);

    //Compute correct grid size (i.e., number of blocks per kernel launch)
    //from the image size and block size.
    const dim3 grid_size((shape_image_.pixels_z + block_size.x - 1) / block_size.x,
                         (shape_image_.pixels_x + block_size.y - 1) / block_size.y,
                         (shape_image_.pixels_y + block_size.z - 1) / block_size.z);

    for (size_t frame = 0; frame < shape_rawdata_.frame_number; ++frame) {

        // Reset output image to 0 for each frame
        // No need for it because we no longer use tmp array for image reconstruction
        // as the output is already on the GPU
        // checkCudaErrors(cudaMemset(rf_image_d_, 0, shape_image_.pixels_xyz() * sizeof(float)));

        // Update pointer
        T* rf_img_frame = rf_image + frame * shape_image_.pixels_xyz();

        for (size_t event = 0; event < shape_rawdata_.event_number; ++event) {

            size_t stream_idx = event % 2;
            const T* event_ptr = raw_data + event_size * (
                    event + shape_rawdata_.event_number * frame);

            checkCudaErrors(cudaMemcpyAsync(
                    raw_data_tmp[stream_idx],
                    event_ptr,
                    event_size * sizeof(T),
                    cudaMemcpyDeviceToDevice,
                    stream[stream_idx]
            ));

            // Prefilter data to compute spline coefficients ck from data samples fk
            char bc = 'm';
            prefilterChannelDevice<Interp::getScheme()>(
                    raw_data_tmp[stream_idx],
                    shape_rawdata_.element_number,
                    shape_rawdata_.sample_number,
                    bc,
                    stream[stream_idx]
            );

            Delay delay = delays[event];

            // Prepare kernel args
            void* kernel_args[] = {
                    &raw_data_tmp[stream_idx],
                    &x_im_axis_ptr,
                    &shape_image_.pixels_x,
                    &y_im_axis_ptr,
                    &shape_image_.pixels_y,
                    &z_im_axis_ptr,
                    &shape_image_.pixels_z,
                    &el_pos_ptr,
                    &shape_rawdata_.element_number,
                    &delay,
                    &interp,
                    &apod,
                    &rf_img_frame
            };

            // Launch kernel
            checkCudaErrors(cudaLaunchKernel(
                    (void*)reconstruct_kernel<T, Delay, Interp, Apod>,
                    grid_size, block_size, kernel_args, 0, stream[stream_idx]
            ));

//            reconstruct_kernel <<< grid_size, block_size, 0,
//                    stream[stream_idx] >>> (
//                            raw_data_tmp[stream_idx],
//                            x_im_axis_ptr, shape_image_.pixels_x,
//                            y_im_axis_ptr, shape_image_.pixels_y,
//                            z_im_axis_ptr, shape_image_.pixels_z,
//                            el_pos_ptr, shape_rawdata_.element_number,
//                            delay, interp, apod,
//                            rf_image + frame * shape_image_.pixels_xyz()
//            );

//        checkCudaErrors(cudaDeviceSynchronize());
            checkCudaErrors(cudaGetLastError()); // not sure if needed with cudaLaunchKernel
        }

//        // Should not be necessary if rf_image is created on the GPU
//        checkCudaErrors(cudaMemcpy(
//                rf_image + frame * shape_image_.pixels_xyz(),
//                rf_image_d_,
//                shape_image_.pixels_xyz() * sizeof(float),
//                cudaMemcpyDeviceToDevice
//        ));
    }

    // Synchronize before leaving function
    checkCudaErrors(cudaDeviceSynchronize());

    cudaStreamDestroy(stream[0]);
    cudaStreamDestroy(stream[1]);

//    auto end = std::chrono::system_clock::now();
//    std::chrono::duration<double> elapsed_seconds = end-start;
//    std::cout << "elapsed time: " << elapsed_seconds.count() << "s" << std::endl;
}

} // end namespace pulse

#include "beamformer_device_instantiation.cu"
