#ifndef PULSE_DELAY_LAW_INL
#define PULSE_DELAY_LAW_INL

#include "pulse/delay_law/delay_law.h"
#include "pulse/helper/math.h"
#include <cuda_runtime.h>
#include <iostream>
#include <cmath>

namespace pulse {

inline PlaneWave2D::PlaneWave2D() = default;

inline PlaneWave2D::PlaneWave2D(float mean_sound_speed, float angle, float offset)
{
    i_sound_speed_ = 1.f / mean_sound_speed;
    cos_angle_ = cosf(angle);
    sin_angle_ = sinf(angle);
    offset_ = offset;
}

__host__ __device__
inline float PlaneWave2D::operator()(float3 pos_im, float3 pos_td)
{
    // Transmit distance
    float r_tx = pos_im.z * cos_angle_ + pos_im.x * sin_angle_;

    // Receive distance
//        float r_rx = length(pos_im - pos_td);
    float r_rx = sqrtf(
            (pos_im.x - pos_td.x) * (pos_im.x - pos_td.x) +
            (pos_im.z - pos_td.z) * (pos_im.z - pos_td.z));

    // Round-trip time-of-flight (time)
    float t_tot = (r_tx + r_rx) * i_sound_speed_ + offset_;

    return t_tot;
}


inline PlaneWave3D::PlaneWave3D() = default;

inline PlaneWave3D::PlaneWave3D(float mean_sound_speed, float angle_x,
                                float angle_y, float offset)
{
    i_sound_speed_ = 1.f / mean_sound_speed;
    cos_angle_ = {cosf(angle_x), cosf(angle_y)};
    sin_angle_ = {sinf(angle_x), sinf(angle_y)};
    offset_ = offset;
}

__host__ __device__
inline float PlaneWave3D::operator()(float3 pos_im, float3 pos_td)
{
    // Transmit distance
    float r_tx = pos_im.x * sin_angle_.x * cos_angle_.y +
                 pos_im.y * cos_angle_.x * sin_angle_.y +
                 pos_im.z * cos_angle_.x * cos_angle_.y;

    // Receive distance
    float r_rx = length(pos_im - pos_td);

    // Round-trip time-of-flight (time)
    float t_tot = (r_tx + r_rx) * i_sound_speed_ + offset_;

    return t_tot;
}


inline DivergingWave::DivergingWave() = default;

inline DivergingWave::DivergingWave(float mean_sound_speed,
                                    float3 virtual_source, float offset)
        : i_sound_speed_(1.f / mean_sound_speed), vs_(virtual_source),
          offset_(offset) {
    if (vs_.z > 0) {
        std::cerr << "Not implemented (DW virtual source with z > 0)" << std::endl;
        throw;
    }

}

__host__ __device__
inline float DivergingWave::operator()(float3 pos_im, float3 pos_td)
{
    float r_tx = length(pos_im - vs_);
    float r_rx = length(pos_im - pos_td);
    float t_tot = (r_tx + r_rx) * i_sound_speed_ + offset_;
    return t_tot;
}

} // end namespace pulse

#endif //PULSE_DELAY_LAW_INL
