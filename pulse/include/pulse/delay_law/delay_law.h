#ifndef PULSE_DELAY_LAW_H
#define PULSE_DELAY_LAW_H

#include <cuda_runtime.h>

namespace pulse {

struct PlaneWave2D {
    PlaneWave2D();

    PlaneWave2D(float mean_sound_speed, float angle, float offset=0);

    __host__ __device__
    float operator()(float3 pos_im, float3 pos_td);

private:
    float i_sound_speed_;
    float cos_angle_;
    float sin_angle_;
    float offset_;
};

struct PlaneWave3D {
    PlaneWave3D();

    PlaneWave3D(float mean_sound_speed, float angle_x, float angle_y, float offset=0);

    __host__ __device__
    float operator()(float3 pos_im, float3 pos_td);

private:
    float i_sound_speed_;
    float2 cos_angle_;
    float2 sin_angle_;
    float offset_;
};

struct DivergingWave {
    DivergingWave();

    DivergingWave(float mean_sound_speed, float3 virtual_source, float offset=0);

    __host__ __device__
    float operator()(float3 pos_im, float3 pos_td);

private:
    float i_sound_speed_;
    float3 vs_;
    float offset_;
};

} // end namespace pulse

#include "delay_law.inl"

#endif //PULSE_DELAY_LAW_H
