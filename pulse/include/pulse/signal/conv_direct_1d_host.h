#ifndef PULSE_CONV_DIRECT_1D_HOST_H
#define PULSE_CONV_DIRECT_1D_HOST_H

#include "conv_direct_1d.h"

namespace pulse {

class ConvolutionDirect1DHost: public ConvolutionDirect1D {
public:
    ConvolutionDirect1DHost() = default;
    ~ConvolutionDirect1DHost() override = default;

    template<typename T>
    void convolve_full(const T* input, int input_size, int nb_inputs,
                       const T* filter, int filter_size, int nb_filters,
                       T* output);

    template<typename T>
    void convolve_same(const T* input, int input_size, int nb_inputs,
                       const T* filter, int filter_size, int nb_filters,
                       T* output);

    template<typename T>
    void convolve_valid(const T* input, int input_size, int nb_inputs,
                        const T* filter, int filter_size, int nb_filters,
                        T* output);
};

} // end namespace pulse

#endif //PULSE_CONV_DIRECT_1D_HOST_H
