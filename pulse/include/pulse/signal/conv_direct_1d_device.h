#ifndef PULSE_CONV_DIRECT_1D_DEVICE_H
#define PULSE_CONV_DIRECT_1D_DEVICE_H

#include "conv_direct_1d.h"

namespace pulse {

class ConvolutionDirect1DDevice: public ConvolutionDirect1D {
public:
    ConvolutionDirect1DDevice() = default;
    ~ConvolutionDirect1DDevice() override = default;

    template<typename T>
    void convolve_full(const T* input, int input_size, int nb_inputs,
                       const T* filter, int filter_size, int nb_filters,
                       T* output);

    template<typename T>
    void convolve_same(const T* input, int input_size, int nb_inputs,
                       const T* filter, int filter_size, int nb_filters,
                       T* output);

    template<typename T>
    void convolve_valid(const T* input, int input_size, int nb_inputs,
                        const T* filter, int filter_size, int nb_filters,
                        T* output);
};

} // end namespace pulse

#endif //PULSE_CONV_DIRECT_1D_DEVICE_H
