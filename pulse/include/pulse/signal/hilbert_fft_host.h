#ifndef PULSE_HILBERT_FFT_HOST_H
#define PULSE_HILBERT_FFT_HOST_H

#include "hilbert_fft.h"
#include <complex>

namespace pulse {

class HilbertFFTHost : public HilbertFFT {
public:

    HilbertFFTHost(int len_signal, int batch);

    ~HilbertFFTHost() override;

    void analytic_signal(float *x_mat, std::complex<float> *x_a_mat) override;

private:
    void hilbert_fftw_r2c(const float* x_mat, std::complex<float>* x_a_mat);
    void hilbert_fftw_omp_r2c(const float* x_mat, std::complex<float>* x_a_mat);
};

} // end namespace pulse

#endif //PULSE_HILBERT_FFT_HOST_H
