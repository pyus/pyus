#ifndef PULSE_HILBERT_FIR_DEVICE_H
#define PULSE_HILBERT_FIR_DEVICE_H

#include "hilbert_fir.h"
#include <vector>
#include <thrust/device_vector.h>
#include <thrust/complex.h>

namespace pulse {

class HilbertFIRDevice: public HilbertFIR {
public:

    HilbertFIRDevice(int len_signal, int batch, int n_taps, float beta);

    HilbertFIRDevice(
            int len_signal, int batch,
            std::vector<std::complex<float>> fir_filter
    );

    ~HilbertFIRDevice() override;

    void analytic_signal(float* x_mat, std::complex<float>* x_a_mat) override;

private:
    thrust::device_vector<thrust::complex<float>> fir_filter_d_;
};

} // end namespace pulse

#endif //PULSE_HILBERT_FIR_DEVICE_H
