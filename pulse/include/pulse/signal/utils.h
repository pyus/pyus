#ifndef PULSE_SIGNAL_UTILS_H
#define PULSE_SIGNAL_UTILS_H

#include <thrust/complex.h>
#include <string>
#include <vector>

namespace pulse {

void magnitude_host(std::complex<float>* input, size_t size, float* output);

void magnitude_device(thrust::complex<float>* input, size_t size, float* output);

void env2bmode_host(float* env, size_t len_env, size_t nb_batches,
                      std::string norm, float* norm_factors, float* bmode);

void env2bmode_device(float* env, size_t len_env, size_t nb_batches,
                        std::string norm, float* norm_factors, float* bmode);

void rf2bmode_host(
        float* rf_data, size_t len_data, size_t nb_batches,
        std::vector<std::complex<float>> fir_filter, std::string norm,
        float* norm_factors, float* bmode
);

void rf2bmode_device(
        float* rf_data, size_t len_data, size_t nb_batches,
        std::vector<std::complex<float>> fir_filter, std::string norm,
        float* norm_factors, float* bmode
);

void iq2bmode_device(
        thrust::complex<float>* iq_data, size_t len_data, size_t nb_batches,
        std::string norm, float* norm_factors, float* bmode
);

void iq2bmode_host(
        std::complex<float>* iq_data, size_t len_data, size_t nb_batches,
        std::string norm, float* norm_factors, float* bmode
);

} // end namespace pulse

#endif //PULSE_SIGNAL_UTILS_H
