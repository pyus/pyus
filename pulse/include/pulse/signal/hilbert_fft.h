#ifndef PULSE_HILBERT_FFT_H
#define PULSE_HILBERT_FFT_H

#include "hilbert.h"
#include <complex>

namespace pulse {

class HilbertFFT : public Hilbert {
public:

    HilbertFFT(int len_signal, int batch);

    virtual ~HilbertFFT();

    virtual void analytic_signal(float* x_mat,
                                 std::complex<float>* x_a_mat) = 0;

protected:

    // cufft plan with advanced data layout
    // --- Advanced data layout
    //     input[b * idist + x * istride]
    //     output[b * odist + x * ostride]
    //     b = signal number
    //     x = element of the b-th signal
    struct {
        int rank;     // --- 1-2-3D FFTs
        int* n;       // --- Size of the Fourier analytic_signal
        int istride;  // --- Distance between 2 successive elements (input)
        int ostride;  // ---                                        (output)
        int idist;    // --- Distance between batches (input)
        int odist;    // ---                          (output)
        int* inembed; // --- Input size with pitch (ignored for 1D transforms)
        int* onembed; // --- Output size with pitch (ignored for 1D transforms)
        int batch;    // --- Number of batched executions
    } plan_setup_;
};

} // end namespace pulse

#endif //PULSE_HILBERT_FFT_H
