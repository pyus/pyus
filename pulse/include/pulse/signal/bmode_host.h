#ifndef PULSE_BMODE_HOST_H
#define PULSE_BMODE_HOST_H

#include "bmode.h"
#include "pulse/helper/shape.h"
#include <vector>

namespace pulse {

class BmodeHost : public Bmode {
public:
    BmodeHost(Size3D size_image);
    ~BmodeHost();

    void RF2Bmode(float* rf_image);
    void RF2Bmode(float* rf_image, float normalization_factor);

private:
    std::vector<std::complex<float>> envelope_;
};

} // end namespace pulse

#endif //PULSE_BMODE_HOST_H
