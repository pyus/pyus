#ifndef PULSE_CONV_DIRECT_1D_H
#define PULSE_CONV_DIRECT_1D_H

namespace pulse {

class ConvolutionDirect1D {
public:
    ConvolutionDirect1D() = default;
    virtual ~ConvolutionDirect1D() = default;
};

} // end namespace pulse


#endif //PULSE_CONV_DIRECT_1D_H
