#ifndef PULSE_HILBERT_H
#define PULSE_HILBERT_H

#include <complex>

namespace pulse {

class Hilbert {
public:

    Hilbert(int len_signal, int batch);

    virtual ~Hilbert();

    // TODO: fix 2 ways of implementing complex numbers
    // std::complex and fftwf_complex -> float[2]
    // CufftComplex -> float2
    // same size and same memory alignment = binary compatible
    // -> two overloads of the function ?
    // -> unions?
    // casting -> only use std::complex to keep float2 in cuda specific files

    virtual void analytic_signal(float *x_mat, std::complex<float> *x_a_mat) = 0;

//    virtual void hilbert_transform(float* x, float* h_x) = 0;

protected:
    int fft_size_ = 0;
    int batch_ = 0;
};

} // end namespace pulse

#endif //PULSE_HILBERT_H
