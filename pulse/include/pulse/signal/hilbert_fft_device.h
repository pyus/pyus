#ifndef PULSE_HILBERT_FFT_DEVICE_H
#define PULSE_HILBERT_FFT_DEVICE_H

#include "hilbert_fft.h"
#include <cufft.h>
#include <complex>

namespace pulse {

class HilbertFFTDevice : public HilbertFFT {
public:

    HilbertFFTDevice(int len_signal, int batch);

    ~HilbertFFTDevice() override;

    void analytic_signal(float *x_mat, std::complex<float> *x_a_mat) override;

private:
    void hilbert_cufft_r2c(cufftReal* d_x_mat, cufftComplex* d_x_a_mat);

    void hilbert_cufft_c2c(const float* d_x_mat, cufftComplex* d_x_a_mat);

    cufftHandle cufft_handle_c2c_;
    cufftHandle cufft_handle_r2c_;
};

} // end namespace pulse

#endif //PULSE_HILBERT_FFT_DEVICE_H
