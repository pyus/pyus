#ifndef PULSE_HILBERT_FIR_H
#define PULSE_HILBERT_FIR_H

#include "hilbert.h"
#include <vector>
#include <complex>

namespace pulse {

class HilbertFIR: public Hilbert {
public:

    HilbertFIR(int len_signal, int batch, int n_taps, float beta);

    HilbertFIR(
            int len_signal, int batch,
            std::vector<std::complex<float>> fir_filter
    );

    virtual ~HilbertFIR();

    virtual void analytic_signal(float* x_mat,
                                 std::complex<float>* x_a_mat) = 0;

protected:
    std::vector<std::complex<float>> fir_filter_;
    int len_signal_;
    int n_taps_;
};

} // end namespace pulse

#endif //PULSE_HILBERT_FIR_H
