#ifndef PULSE_KAISER_H
#define PULSE_KAISER_H

#include <vector>

namespace pulse {

std::vector<float> kaiser_window(int M, float beta);

} // end namespace pulse

#endif //PULSE_KAISER_H
