#ifndef PULSE_BMODE_DEVICE_H
#define PULSE_BMODE_DEVICE_H

#include "bmode.h"
#include "pulse/helper/shape.h"
#include <cufft.h>

namespace pulse {

class BmodeDevice : public Bmode {
public:
    BmodeDevice(Size3D size_image);
    ~BmodeDevice();

    void RF2Bmode(float* rf_image);
    void RF2Bmode(float* rf_image, float normalization_factor);

private:
    cufftComplex* envelope_;
};

} // end namespace pulse

#endif //PULSE_BMODE_DEVICE_H
