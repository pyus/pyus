#ifndef PULSE_HILBERT_FIR_HOST_H
#define PULSE_HILBERT_FIR_HOST_H

#include "hilbert_fir.h"
#include <vector>
#include <complex>

namespace pulse {

class HilbertFIRHost: public HilbertFIR {
public:

    HilbertFIRHost(int len_signal, int batch, int n_taps, float beta);

    HilbertFIRHost(
            int len_signal, int batch,
            std::vector<std::complex<float>> fir_filter
    );

    virtual ~HilbertFIRHost();

    void analytic_signal(float* x_mat, std::complex<float>* x_a_mat) override;
};

} // end namespace pulse

#endif //PULSE_HILBERT_FIR_HOST_H
