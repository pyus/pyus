#ifndef PULSE_BMODE_H
#define PULSE_BMODE_H

#include "pulse/helper/shape.h"
#include "hilbert_fft.h"
#include <vector>

namespace pulse {

class Bmode {
public:
    Bmode(Size3D size_image);
    virtual ~Bmode();

protected:
    HilbertFFT* hilbert_;
    Size3D size_image_;
};

} // end namespace pulse

#endif //PULSE_BMODE_H
