#ifndef PULSE_WINDOW_FUNCTOR_INL
#define PULSE_WINDOW_FUNCTOR_INL

#include "pulse/helper/math.h"
#include "pulse/helper/shape.h"
#include <cuda_runtime.h>

namespace pulse {

//TODO: remove test on aperture if z_im == 0 (why is it here btw)

// ─── Selfridge 2D ────────────────────────────────────────────────────────────

inline Selfridge2D::Selfridge2D() = default;

inline Selfridge2D::Selfridge2D(float wavelength, float el_width)
        : wavelength_(wavelength), el_width_x_(el_width)
{
    factor_ = el_width_x_ / wavelength_;
}

__host__ __device__
inline float Selfridge2D::operator()(float3 pos_im, float3 pos_td)
{
    float idist = 1.f / (sqrtf((pos_im.x - pos_td.x) * (pos_im.x - pos_td.x) +
                              (pos_im.z - pos_td.z) * (pos_im.z - pos_td.z)) +
                        EPS_0);

    float out = sinc(factor_ * (pos_im.x - pos_td.x) * idist) *
                (pos_im.z + EPS_0) * idist;

    return out;
}

// ─── Selfridge 3D ────────────────────────────────────────────────────────────

inline Selfridge3D::Selfridge3D() = default;

inline Selfridge3D::Selfridge3D(float wavelength, float el_width_x,
                                float el_width_y)
        : wavelength_(wavelength), el_width_x_(el_width_x),
          el_width_y_(el_width_y)
{
    factor_x_ = el_width_x_ / wavelength_;
    factor_y_ = el_width_y_ / wavelength_;
}

__host__ __device__
inline float Selfridge3D::operator()(float3 pos_im, float3 pos_td)
{
    float proj_xy = EPS_0 + sqrtf(pow2(pos_im.x - pos_td.x) +
                                  pow2(pos_im.y - pos_td.y));
    float dist = length(pos_im - pos_td) + EPS_0;
    float cos_theta = pos_im.z / dist;
    float sin_theta = proj_xy / dist;
    float cos_phi = (pos_im.x - pos_td.x) / proj_xy;
    float sin_phi = (pos_im.y - pos_td.y) / proj_xy;
    float u = sin_theta * cos_phi;
    float v = sin_theta * sin_phi;
//        float factor = sinc(el_width_x_ * u / wavelength_) *
//                       sinc(el_width_y_ * v / wavelength_) * cos_theta;
    float out = sinc(factor_x_ * u) * sinc(factor_y_ * v) * cos_theta;

    return out;
}

// ─── Boxcar ──────────────────────────────────────────────────────────────────

inline Boxcar::Boxcar() = default;

inline Boxcar::Boxcar(float f_number) : f_number_(f_number) {}

__host__ __device__
inline float Boxcar::operator()(float3 pos_im, float3 pos_td)
{
    float distance = fabsf(pos_im.x - pos_td.x);
    float aperture = pos_im.z != 0 ? pos_im.z / f_number_ : EPS_1;

    float out = 0.f;
    if (distance < aperture * 0.5f) {
        out = 1.f;
    }

    return out;
}

// ─── Hanning ─────────────────────────────────────────────────────────────────

inline Hanning::Hanning() = default;

inline Hanning::Hanning(float f_number) : f_number_(f_number) {}

__host__ __device__
inline float Hanning::operator()(float3 pos_im, float3 pos_td)
{
    // Compute lateral relative distance and aperture w.r.t. depth
    float distance = fabsf(pos_im.x - pos_td.x);
    float aperture = pos_im.z != 0 ? pos_im.z / f_number_ : EPS_1;

    float out = 0.f;
    if (distance < aperture * 0.5f) {
        out = 0.5f + 0.5f * cosf(2.f * (float) M_PI * distance / aperture);
    }

    return out;
}

// ─── Hamming ─────────────────────────────────────────────────────────────────

inline Hamming::Hamming() = default;

inline Hamming::Hamming(float f_number) : f_number_(f_number) {}

__host__ __device__
inline float Hamming::operator()(float3 pos_im, float3 pos_td)
{
    // Compute lateral relative distance and aperture w.r.t. depth
    float distance = fabsf(pos_im.x - pos_td.x);
    float aperture = pos_im.z != 0 ? pos_im.z / f_number_ : EPS_1;

    float out = 0.f;
    if (distance < aperture * 0.5f) {
        out = 0.53836f +
              0.46164f * cosf(2.f * (float) M_PI * distance / aperture);
    }

    return out;
}

// ─── Tukey ───────────────────────────────────────────────────────────────────

inline Tukey::Tukey() = default;

inline Tukey::Tukey(float alpha, float f_number) : alpha_(alpha),
                                                   f_number_(f_number)
{
    if (alpha_ > 1) {alpha_ = alpha_ / 100.f;}
}

__host__ __device__
inline float Tukey::operator()(float3 pos_im, float3 pos_td)
{
    float distance = fabsf(pos_im.x - pos_td.x);
    float aperture = pos_im.z != 0 ? pos_im.z / f_number_ : EPS_1;

    float factor = 0.f;
    if (distance < 0.5f * aperture) {
        factor = 0.5f * (1.f + cosf(2.f * (float) M_PI / alpha_ *
                                    (distance / aperture - 0.5f * alpha_ -
                                     0.5f)));
    }
    if (distance < 0.5f * aperture * (1.f - alpha_)) {
        factor = 1.f;
    }

    return factor;
}

// ─── Identity ────────────────────────────────────────────────────────────────

inline IdentityWindow::IdentityWindow() = default;

__host__ __device__
inline float IdentityWindow::operator()(float3 pos_im, float3 pos_td)
{
    return 1.f;
}

} // end namespace pulse

#endif //PULSE_WINDOW_FUNCTOR_INL
