#ifndef PULSE_WINDOW_FUNCTOR_H
#define PULSE_WINDOW_FUNCTOR_H

#include <cuda_runtime.h>

namespace pulse {

struct Selfridge2D {

    Selfridge2D();

    Selfridge2D(float wavelength, float el_width);

    __host__ __device__
    float operator()(float3 pos_im, float3 pos_td);

private:
    float wavelength_;
    float el_width_x_;
    float el_width_y_ = 0;
    float factor_;
};


struct Selfridge3D {

    Selfridge3D();

    Selfridge3D(float wavelength, float el_width_x, float el_width_y);

    __host__ __device__
    float operator()(float3 pos_im, float3 pos_td);

private:
    float wavelength_;
    float el_width_x_;
    float el_width_y_;
    float factor_x_;
    float factor_y_;
};

struct Boxcar {

    Boxcar();

    Boxcar(float f_number);

    __host__ __device__
    float operator()(float3 pos_im, float3 pos_td);

private:
    float f_number_;
};

struct Hanning {

    Hanning();

    Hanning(float f_number);

    __host__ __device__
    float operator()(float3 pos_im, float3 pos_td);

private:
    float f_number_;
};

struct Hamming {

    Hamming();

    Hamming(float f_number);

    __host__ __device__
    float operator()(float3 pos_im, float3 pos_td);

private:
    float f_number_;
};

struct Tukey {

    Tukey();

    Tukey(float alpha, float f_number);

    __host__ __device__
    float operator()(float3 pos_im, float3 pos_td);

private:
    float alpha_;
    float f_number_;
};

struct IdentityWindow {

    IdentityWindow();

    __host__ __device__
    float operator()(float3 pos_im, float3 pos_td);
};

} // end namespace pulse

#include "window_functor.inl"

#endif //PULSE_WINDOW_FUNCTOR_H
