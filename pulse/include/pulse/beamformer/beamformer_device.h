#ifndef PULSE_BEAMFORMER_DEVICE_H
#define PULSE_BEAMFORMER_DEVICE_H

#include "beamformer.h"
#include <vector>
#include <thrust/device_vector.h>

namespace pulse {

class BeamformerDevice : public Beamformer {
public:
    //! Constructor
    BeamformerDevice(
            std::vector<float> element_positions,
            std::vector<float> t_axis,
            std::vector<float> x_im_axis,
            std::vector<float> y_im_axis,
            std::vector<float> z_im_axis);

    //! Destructor
    ~BeamformerDevice() override;

    //! Beamform data
    template<typename T, typename Delay, typename Interp, typename Apod>
    void reconstruct(const T* raw_data, int frame_number,
                     std::vector<Delay> delays, Interp interp, Apod apod,
                     T* rf_image);

private:
    // Owner (device)
    thrust::device_vector<float3> el_pos_d_;
    thrust::device_vector<float> t_axis_d_;
    thrust::device_vector<float> x_im_axis_d_;
    thrust::device_vector<float> y_im_axis_d_;
    thrust::device_vector<float> z_im_axis_d_;
    thrust::device_vector<float> raw_data_d_1;
    thrust::device_vector<float> raw_data_d_2;
};

} // end namespace pulse

#endif //PULSE_BEAMFORMER_DEVICE_H
