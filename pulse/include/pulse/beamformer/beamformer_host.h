#ifndef PULSE_BEAMFORMER_HOST_H
#define PULSE_BEAMFORMER_HOST_H

#include "beamformer.h"
#include <vector>

namespace pulse {

class BeamformerHost : public Beamformer {
public:
    //! Constructor
    BeamformerHost(
            std::vector<float> element_positions,
            std::vector<float> t_axis,
            std::vector<float> x_im_axis,
            std::vector<float> y_im_axis,
            std::vector<float> z_im_axis);

    //! Destructor
    ~BeamformerHost() override;

    //! Beamform data
    template<typename T, typename Delay, typename Interp, typename Apod>
    void reconstruct(const T* raw_data, int frame_number,
                     std::vector<Delay> delays, Interp interp, Apod apod,
                     T* rf_image);
};

} // end namespace pulse

#endif //PULSE_BEAMFORMER_HOST_H
