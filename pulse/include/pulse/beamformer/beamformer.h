#ifndef PULSE_BEAMFORMER_H
#define PULSE_BEAMFORMER_H

#include "pulse/helper/shape.h"
#include <cuda_runtime_api.h>
#include <vector>

namespace pulse {

class Beamformer {
public:
    //! Constructor
    Beamformer(
            std::vector<float> element_positions,
            std::vector<float> t_axis,
            std::vector<float> x_im_axis,
            std::vector<float> y_im_axis,
            std::vector<float> z_im_axis);

    // Empty constructor
    Beamformer() = default;

    //! Destructor
    virtual ~Beamformer();

    //! Reconstruct RF image
    template<typename Delay, typename Interp, typename Apod>
    void reconstruct(const float* input_raw_data, int frame_number,
                             std::vector<Delay> delays, Interp interp, Apod apod,
                             float* rf_image);

protected:
    Size3D size_image_;

    ShapeData shape_rawdata_;
    ShapeImage shape_image_;

    std::vector<float3> el_pos_;
    std::vector<float> t_axis_;
    std::vector<float> x_im_axis_;
    std::vector<float> y_im_axis_;
    std::vector<float> z_im_axis_;
};

} // end namespace pulse

#endif //PULSE_BEAMFORMER_H
