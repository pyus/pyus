#ifndef PULSE_SHAPE_H
#define PULSE_SHAPE_H

#include <cstddef>

namespace pulse {

struct Size3D {
    Size3D() = default;
    Size3D(size_t x, size_t y, size_t z):x(x), y(y), z(z), len(x*y*z){}

    size_t x;
    size_t y;
    size_t z;
    size_t len;
};

struct ShapeData {
    ShapeData() = default;
    ShapeData(size_t frame_number, size_t event_number, size_t element_number,
            size_t sample_number):
            frame_number(frame_number), event_number(event_number),
            element_number(element_number), sample_number(sample_number) {}

    size_t len() {return frame_number * event_number * element_number * sample_number;}

    size_t frame_number;
    size_t event_number;
    size_t element_number;
    size_t sample_number;
};

struct ShapeImage {
    ShapeImage() = default;
    ShapeImage(size_t frame_number, size_t pixels_x, size_t pixels_y,
               size_t pixels_z):
               frame_number(frame_number), pixels_x(pixels_x),
               pixels_y(pixels_y), pixels_z(pixels_z) {}

    size_t len() {return frame_number * pixels_x * pixels_y * pixels_z;}
    size_t pixels_xyz() {return pixels_x * pixels_y * pixels_z;}

    size_t frame_number;
    size_t pixels_x;
    size_t pixels_y;
    size_t pixels_z;
};

} // end namespace pulse

#endif //PULSE_SHAPE_H
