#ifndef PULSE_FUNCTOR_H
#define PULSE_FUNCTOR_H

#include <cuda_runtime_api.h>
#include <vector>
#include <algorithm>

template<typename T>
void apply_functor(std::vector<float3>* input_1, std::vector<float3>* input_2,
                   std::vector<float>* output, T functor)
{
    std::transform(input_1->begin(), input_1->end(), input_2->begin(), output->begin(), functor);
}


#endif //PULSE_FUNCTOR_H
