#ifndef PULSE_INTERP_KERNELS_HH
#define PULSE_INTERP_KERNELS_HH

#include <cuda_runtime.h>

namespace pulse {

// Piecewise polynomials
__host__ __device__
inline float keysPiecewisePolynomial(float x);

template<int degree>
__host__ __device__
inline float bsplinePiecewisePolynomial(float x);

template<int deg>
__host__ __device__
inline float omomsPiecewisePolynomial(float x);

// Interpolation weights
__host__ __device__
inline void keysWeights(float x, float* weights);

template<int degree>
__host__ __device__
inline void bsplineWeights(float x, float* weights);

template<int deg>
__host__ __device__
inline void omomsWeights(float x, float* weights);

} // end namespace pulse

#include "interp_kernels.inl"

#endif //PULSE_INTERP_KERNELS_HH
