#ifndef PULSE_INTERP_COMMON_INL
#define PULSE_INTERP_COMMON_INL

#include "pulse/interpolation/interp_kernels.h"
#include <cuda_runtime_api.h>
#include <cmath>

namespace pulse {

__host__ __device__
inline int mirrorExtension(int index, int len_axis)
{
    int len2 = 2 * len_axis - 2;

/*
    // Note: the modulo operator could be faster on unsigned int
    // (https://stackoverflow.com/a/2661758/8237847)

//    idx_new = abs(index) % len2;
    int idx_new = (unsigned)abs(index) % len2;

    if (len_axis <= idx_new) {
        idx_new = len2 - idx_new;
    }
*/
    // Using if else instead of heavy modulo operator is faster
    int idx_new = index;

    if (index < 0) {
        idx_new = - index - len2 * (-index / len2);
    }
    if (index >= len_axis) {
        idx_new = len2 - index;
    }

    return idx_new;
}

template<int support>
__host__ __device__
inline float interpIndices(float abscissa, float first_sample,
                           int length_signal, float sampling_frequency,
                           int* indices)
{
    static const int interp_offset = (support - 1) / 2;

    // Compute 'insertion' index (float)
    float idx_f = (abscissa - first_sample) * sampling_frequency;

    // Compute floored index (int)
    int idx_i = 0;
    if ((support - 1) & 1) {
        idx_i = (int) floorf(idx_f) - interp_offset;
    }
    else {
        idx_i = (int) floorf(idx_f + 0.5f) - interp_offset;
    }

    // Compute frac
    float frac = idx_f - (idx_i + interp_offset);
    // A more accurate computation would use the interpolation axis:
    // float frac = (abscissa - axis[idx_i]) * sampling_frequency;

    //Compute indexes
//#pragma unroll
//    for (int i = 0; i < support; ++i) {
//        // Compute index (mirror extension mode)
//        indices[i] = mirrorExtension(idx_i + i, length_signal);
//    }

    // Save 1 call to 'mirror_extension'
#pragma unroll
    for (int i = 0; i < interp_offset; ++i) {
        indices[i] = mirrorExtension(idx_i + i, length_signal);
    }

    indices[interp_offset] = idx_i + interp_offset;

#pragma unroll
    for (int i = interp_offset + 1; i < support; ++i) {
        indices[i] = mirrorExtension(idx_i + i, length_signal);
    }

    return frac;
}

template<InterpScheme scheme>
__host__ __device__
inline void interpWeights(float frac, float* weights){

    switch(scheme){
        case NEAREST:
            weights[0] = 1;
            break;
        case LINEAR:
            weights[0] = 1 - frac;
            weights[1] = frac;
            break;
        case KEYS:
            keysWeights(frac, weights);
            break;
        case BSPLINE_3:
            bsplineWeights<support(scheme)-1>(frac, weights);
            break;
        case BSPLINE_4:
            bsplineWeights<support(scheme)-1>(frac, weights);
            break;
        case BSPLINE_5:
            bsplineWeights<support(scheme)-1>(frac, weights);
            break;
        case OMOMS_3:
            omomsWeights<support(scheme)-1>(frac, weights);
            break;
    }
}

} // end namespace pulse

#endif //PULSE_INTERP_COMMON_INL
