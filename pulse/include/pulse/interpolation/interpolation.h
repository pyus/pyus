#ifndef PULSE_INTERPOLATION_H
#define PULSE_INTERPOLATION_H

#include "interp_common.h"
#include <cuda_runtime_api.h>

namespace pulse {

template<InterpScheme scheme>
struct InterpFunctor {

    InterpFunctor() = default; // Necessary to be a literal type
                               // TODO investigate literal types
    InterpFunctor(float first_sample, float last_sample, size_t nb_samples,
                  float sampling_freq);

    template<typename T>
    __host__ __device__
    T operator()(float abscissa, const T* data, size_t channel);

    static constexpr InterpScheme getScheme() {return scheme;}

private:
    float first_;
    float last_;
    size_t nb_samples_;
    float fs_;
};

} // end namespace pulse

#include "interpolation.inl"

#endif //PULSE_INTERPOLATION_H
