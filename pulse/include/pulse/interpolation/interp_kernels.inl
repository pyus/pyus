#ifndef PULSE_INTERP_KERNELS
#define PULSE_INTERP_KERNELS

#include "pulse/helper/math.h"
#include <cuda_runtime.h>
#include <cmath>

namespace pulse {

// Interpolation
// For out of bounds abscissas, no extrapolation is done so the return value is 0
// For in bounds abscissas, if part of the the support is out of bounds,
// mirror extension mode is applied

// Interpolating kernels -------------------------------------------------------
// Piecewise polynomial implementations

__host__ __device__
inline float keysPiecewisePolynomial(float x)
{
    static const float a = -0.5f;

    float abs_x = fabsf(x);
    float res = 0.f;

    if (1.0f > abs_x) {
        res = 1.f + abs_x * abs_x * (-(a + 3.f) + abs_x * (a + 2.f));
    }
    else if (2.0f > abs_x) {
        res = a * (-4.f + abs_x * (8.f + abs_x * (-5.f + abs_x)));
    }

    return res;
}

__host__ __device__
inline void keysWeights(float x, float* weights)
{
    static const int offset = 3 / 2;

#pragma unroll
    for (int i = 0; i < 4; ++i) {
        weights[i] = keysPiecewisePolynomial(float(i - offset) - x);
    }
}

// Non interpolating kernels (needs prefiltering) ------------------------------
// Piecewise polynomials implementations

// default
template<int degree>
__host__ __device__
inline float bsplinePiecewisePolynomial(float x){return 0;}

// specializations
template<>
__host__ __device__
inline float bsplinePiecewisePolynomial<3>(float x)
{
    const float abs_x = fabsf(x);
    float res = 0.f;

    if (1.f > abs_x) {
        res = 2.f / 3.f + abs_x * abs_x * (-1.f + 0.5f * abs_x);
    }
    else if (2.f > abs_x) {
        res = 4.f / 3.f + abs_x * (-2.f + abs_x * (1.f - (1.f / 6.f) * abs_x));
    }

    return res;
}

template<>
__host__ __device__
inline float bsplinePiecewisePolynomial<4>(float x)
{
    const float abs_x = fabsf(x);
    float res = 0.f;

    if (0.5f > abs_x) {
        float x2 = abs_x * abs_x;
        res = 1.f / 192.f * (115.f + x2 * (-120.f + 48.f * x2));
    }
    else if (1.5f > abs_x) {
        res = 1.f / 96.f * (55.f + abs_x * (20.f + abs_x * (-120.f + abs_x *
                                                                     (80.f - 16.f * abs_x ))));
    }
    else if (2.5f > abs_x) {
        res = 1.f / 384.f * (625.f + abs_x * (-1000.f + abs_x * (600.f + abs_x *
                                                                         (-160.f + 16.f * abs_x))));
    }

    return res;
}

template<>
__host__ __device__
inline float bsplinePiecewisePolynomial<5>(float x)
{
    const float abs_x = fabsf(x);
    float res = 0.f;

    if (1.f > abs_x) {
        float x2 = abs_x * abs_x;
        res = 1.f / 60.f * (33.f + x2 * (-30.f + x2 * (15.f - 5.f * abs_x )));
    }
    else if (2.f > abs_x) {
        res = 1.f / 120.f * (51.f + abs_x * (75.f + abs_x * (-210.f + abs_x *
                                                                      (150.f + abs_x * (-45.f + abs_x * 5.f)))));
    }
    else if (3.f > abs_x) {
        res = 1.f / 120.f * (243.f + abs_x * (-405.f + abs_x * (270.f + abs_x *
                                                                        (-90.f + abs_x * (15.f - abs_x)))));
    }

    return res;
}

// default
template<int deg>
__host__ __device__
inline float omomsPiecewisePolynomial(float x){return 0;}

// specializations
template<>
__host__ __device__
inline float omomsPiecewisePolynomial<3>(float x)
{
    const float abs_x = fabsf(x);
    float res = 0.f;

    if (1.f > abs_x) {
        res = 13.f / 21.f + abs_x * (1.f / 14.f + abs_x * (-1.f + 0.5f * abs_x));
    }
    else if (2.f > abs_x) {
        res = 29.f / 21.f + abs_x * (-85.f / 42.f + abs_x * (1.f - 1.f / 6.f * abs_x));
    }

    return res;
}

// Direct computation, adapted from Philippe Thevenaz's implementation

// default
template<int degree>
__host__ __device__
inline void bsplineWeights(float x, float* weights){}

// specializations
template<>
__host__ __device__
inline void bsplineWeights<3>(float x, float* weights)
{
    weights[3] = (1.f / 6.f) * x * x * x;
    weights[0] = (1.f / 6.f) + (1.f / 2.f) * x * (x - 1.f) - weights[3];
    weights[2] = x + weights[0] - 2.f * weights[3];
    weights[1] = 1.f - weights[0] - weights[2] - weights[3];
}

template<>
__host__ __device__
inline void bsplineWeights<4>(float x, float* weights)
{
    float x2 = x * x;
    float t = (1.f / 6.f) * x2;
    weights[0] = 1.f / 2.f - x;
    weights[0] *= weights[0];
    weights[0] *= (1.f / 24.f) * weights[0];
    float t0 = x * (t - 11.f / 24.f);
    float t1 = 19.f / 96.f + x2 * (1.f / 4.f - t);
    weights[1] = t1 + t0;
    weights[3] = t1 - t0;
    weights[4] = weights[0] + t0 + (1.f / 2.f) * x;
    weights[2] = 1.f - weights[0] - weights[1] - weights[3] - weights[4];
}

template<>
__host__ __device__
inline void bsplineWeights<5>(float x, float* weights)
{
    float x2 = x * x;
    weights[5] = (1.f / 120.f) * x * x2 * x2;
    x2 -= x;
    float x4 = x2 * x2;
    x -= 1.f / 2.f;
    float t = x2 * (x2 - 3.f);
    weights[0] = (1.f / 24.f) * (1.f / 5.f + x2 + x4) - weights[5];
    float t0 = (1.f / 24.f) * (x2 * (x2 - 5.f) + 46.f / 5.f);
    float t1 = (-1.f / 12.f) * x * (t + 4.f);
    weights[2] = t0 + t1;
    weights[3] = t0 - t1;
    t0 = (1.f / 16.f) * (9.f / 5.f - t);
    t1 = (1.f / 24.f) * x * (x4 - x2 - 5.f);
    weights[1] = t0 + t1;
    weights[4] = t0 - t1;
}

// No direct computations for O-moms in Thevenaz code so just call piecewise
// polynomial

// default
template<int deg>
__host__ __device__
inline void omomsWeights(float x, float* weights){}

// specializations
template<>
__host__ __device__
inline void omomsWeights<3>(float x, float* weights)
{
    static const int offset = 3 / 2;

#pragma unroll
    for (int i = 0; i < 4; ++i) {
        weights[i] = omomsPiecewisePolynomial<3>(float(i - offset) - x);
    }
}

} // end namespace pulse

#endif //PULSE_INTERP_KERNELS
