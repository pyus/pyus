#ifndef PULSE_INTERPOLATION_INL
#define PULSE_INTERPOLATION_INL

#include "interp_common.h"
#include <cuda_runtime.h>
#include <cstddef>
#include <cmath>

namespace pulse {

template<InterpScheme scheme>
InterpFunctor<scheme>::InterpFunctor(float first_sample, float last_sample,
                                     size_t nb_samples, float sampling_freq)
        : first_(first_sample), last_(last_sample),
          nb_samples_(nb_samples), fs_(sampling_freq) {}

template<InterpScheme scheme>
template<typename T>
__host__ __device__
T InterpFunctor<scheme>::operator()(float abscissa, const T* data, size_t channel)
{
    static constexpr int sup = support(scheme);

    T interp_value = 0.f;
    if (abscissa >= first_ && abscissa < last_) {

        // Compute interpolation indices
        int indices_[sup] = {};
        float frac = interpIndices<sup>(abscissa, first_, nb_samples_, fs_, indices_);

        // Compute interpolation coefficients
        float weights_[sup] = {};
        interpWeights<scheme>(frac, weights_);

        // Compute interpolation value
#pragma unroll
        for (int i = 0; i < support(scheme); ++i) {
            interp_value += data[indices_[i] + channel * nb_samples_] * weights_[i];
        }
    }

    return interp_value;
}


} // end namespace pulse

#endif //PULSE_INTERPOLATION_INL
