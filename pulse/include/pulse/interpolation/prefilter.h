#ifndef PULSE_PREFILTER_H
#define PULSE_PREFILTER_H

#include "interp_common.h"
#include <cuda_runtime_api.h>
#include <complex>
#include <thrust/complex.h>

namespace pulse {

template<InterpScheme scheme>
void prefilterChannelHost(
        float* raw_data, int nb_elem, int nb_samples, char bc_type);

template<InterpScheme scheme>
void prefilterChannelHost(
        std::complex<float>* raw_data, int nb_elem, int nb_samples,
        char bc_type);

template<InterpScheme scheme>
void prefilterChannelDevice(
        float* raw_data, int nb_elem, int nb_samples, char bc_type,
        cudaStream_t stream = 0);

template<InterpScheme scheme>
void prefilterChannelDevice(
        thrust::complex<float>* raw_data, int nb_elem, int nb_samples,
        char bc_type, cudaStream_t stream = 0);

} // end namespace pulse

#endif //PULSE_PREFILTER_H
