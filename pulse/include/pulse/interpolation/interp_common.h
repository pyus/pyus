#ifndef PULSE_INTERP_COMMON
#define PULSE_INTERP_COMMON

#include <cuda_runtime.h>
#include <cstdio>

namespace pulse {

enum InterpScheme{
    NEAREST, LINEAR, KEYS, BSPLINE_3, BSPLINE_4, BSPLINE_5, OMOMS_3
};

enum FuncFamily {
    BSPLINE, OMOMS
};


//! Get support of interpolation scheme at compile time
__host__ __device__
constexpr int support(InterpScheme scheme)
{
    switch(scheme) {
    case NEAREST:
        return 1;
    case LINEAR:
        return 2;
    case KEYS:
    case BSPLINE_3:
    case OMOMS_3:
        return 4;
    case BSPLINE_4:
        return 5;
    case BSPLINE_5:
        return 6;
    default:
        return 0;
    }
}

//! Know at compile time if scheme is interpolant
__host__ __device__
constexpr bool isInterpolant(InterpScheme scheme)
{
    switch(scheme) {
    case BSPLINE_3:
    case BSPLINE_4:
    case BSPLINE_5:
    case OMOMS_3:
        return false;
    default:
        return true;
    }
}

__host__ __device__
constexpr FuncFamily family(InterpScheme scheme)
{
    switch(scheme) {
    case BSPLINE_3:
    case BSPLINE_4:
    case BSPLINE_5:
        return BSPLINE;
    case OMOMS_3:
        return OMOMS;
    default:
        printf("Error, no family for scheme provided\n");
        return BSPLINE;
    }
}

//! Compute index for mirror extension boundaries
__host__ __device__
inline int mirrorExtension(int index, int len_axis);

//! Compute interpolation indices
template<int support>
__host__ __device__
inline float interpIndices(float abscissa, float first_sample,
                           int length_signal, float sampling_frequency,
                           int* indices);

//! Compute interpolation weights
template<InterpScheme scheme>
__host__ __device__
inline void interpWeights(float frac, float* weights);

} // end namespace pulse

#include "interp_common.inl"

//template<InterpScheme scheme>
//__host__ __device__
//float core_interp(float abscissa, const float* axis, float first, float last,
//                  int nb_samples, float fs, const float* data, size_t channel)
//{
//    float interp_value = 0.f;
//    if (abscissa >= first && abscissa < last) {
//
//        int indices_[support(scheme)] = {};
//        float frac = interpIndices<support(scheme)>(
//                abscissa, first, nb_samples, fs, channel, indices_);
//
////        float frac = interpIndicesAxis<support(scheme)>(
////                abscissa, axis, first, nb_samples, fs, channel, indices_);
//
//        float weights_[support(scheme)] = {};
//        interpWeights<scheme>(frac, weights_);
//
//#pragma unroll
//        for (int i = 0; i < support(scheme); ++i) {
//            interp_value += data[indices_[i]] * weights_[i];
//        }
//    }
//    return interp_value;
//}
//
//
//__host__ __device__
//inline float interpolate(float abscissa, const float* axis, float first,
//                         float last, int length, float i_delta,
//                         const float* data, int channel, InterpScheme scheme){
//
//    float interp_val = 0;
//    switch(scheme){
//    case NEAREST:
//        interp_val = core_interp<NEAREST>(abscissa, axis, first, last, length,
//                                           i_delta, data, channel);
//        break;
//    case LINEAR:
//        interp_val = core_interp<LINEAR>(abscissa, axis, first, last, length,
//                                          i_delta, data, channel);
//        break;
//    case KEYS:
//        interp_val = core_interp<KEYS>(abscissa, axis, first, last, length,
//                                        i_delta, data, channel);
//        break;
//    case BSPLINE_3:
//        interp_val = core_interp<BSPLINE_3>(abscissa, axis, first, last, length,
//                                             i_delta, data, channel);
//        break;
//    case BSPLINE_4:
//        interp_val = core_interp<BSPLINE_4>(abscissa, axis, first, last, length,
//                                             i_delta, data, channel);
//        break;
//    case BSPLINE_5:
//        interp_val = core_interp<BSPLINE_5>(abscissa, axis, first, last, length,
//                                             i_delta, data, channel);
//        break;
//    case OMOMS_3:
//        interp_val = core_interp<OMOMS_3>(abscissa, axis, first, last, length, i_delta,
//                                           data, channel);
//        break;
//    default:
//        printf("Invalid interpolation scheme\n");
//        break;
//    }
//
//    return interp_val;
//}

#endif //PULSE_INTERP_COMMON
