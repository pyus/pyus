#ifndef PULSE_PREFILTER_INL
#define PULSE_PREFILTER_INL

#include <pulse/helper/math.h>

namespace pulse {

// The code below is based on the work of Philippe Thevenaz.
// See <http://bigwww.epfl.ch/thevenaz/interpolation/>

// Init ck[0] according to bc_type
template<typename T>
__host__ __device__
inline T initCausalCoeff(T* fk, int data_len, float pole, char bc_type,
                         float tol = 1e-9f)
{
    int horizon = data_len;
    if (tol > 0) {
        horizon = (int) ceilf(logf(tol) / logf(fabsf(pole)));
    }
    else if (tol < 0) {
        horizon = min(12, data_len);
    }

    T c0 = init_vec_float<T>(0);

    switch (bc_type) {
        case 'm':
        case 'M':
            if (horizon < data_len) {
                // Approx (accelerated loop)
                float zn = pole;
                c0 = fk[0];
                for (int n = 1; n < horizon; n++) {
                    c0 += zn * fk[n];
                    zn *= pole;
                }
            }
            else {
                // Exact expression (full loop)
                float zn = pole;
                float iz = 1.f / pole;
                float z2n = powf(pole, data_len - 1);
                c0 = fk[0] + z2n * fk[data_len - 1];
                z2n *= z2n * iz;
                for (int n = 1; n < data_len - 1; n++) {
                    c0 += (zn + z2n) * fk[n];
                    zn *= pole;
                    z2n *= iz;
                }
                c0 /= (1.f - zn * zn);
            }
            return c0;

        case 'c':
        case 'C':
            if (horizon < data_len) {
                // Approx (accelerated loop)
                float zn = pole;
                c0 = fk[0];
                for (int n = 0; n < horizon; n++) {
                    c0 += zn * fk[n];
                    zn *= pole;
                }
            }
            else {
                // Exact expression (full loop)
                float zn = pole;
                float iz = 1.f / pole;
                float pole2n = powf(pole, 2 * data_len);
                float z2n = pole2n;
                c0 = fk[0];
                for (int n = 0; n < data_len; ++n) {
                    c0 += fk[n] * (zn + z2n);
                    zn *= pole;
                    z2n *= iz;
                }
                c0 /= 1 - pole2n;
            }
            return c0;

        case 'z':
        case 'Z':
            if (horizon < data_len) {
                // Approx (accelerated loop)
//            float zn = 1;
                float zn = pole;
                float mul = pole * pole / (1 - pole * pole);
                c0 = fk[0];
                for (int n = 0; n < horizon; n++) {
                    c0 -= mul * zn * fk[n];
                    zn *= pole;
                }
                c0 *= (1 - pole * pole) / (1 - powf(pole, 2 * data_len + 2));
            }
            else {
                // Exact expression (full loop)
//            float zn = 1;
                float zn = pole;
                float zN = powf(pole, data_len + 1);
                float mul = pole * pole / (1 - pole * pole);
                c0 = fk[0] - fk[data_len - 1] * zN;
                for (int n = 1; n < data_len - 1; ++n) {
                    c0 -= mul * zn * (fk[n] - zN * fk[data_len - 1 - n]);
                    zn *= pole;
                }
                c0 *= (1 - pole * pole) / (1 - powf(pole, 2 * data_len + 2));
            }
            return c0;

        default:
            printf("Wrong boundary condition causal\n");
            return init_vec_float<T>(0);
    }
}


// Init ck[N-1] according to bc_type
template<typename T>
__host__ __device__
inline T initAntiCausalCoeff(T* ck_p, int data_len, float pole, char bc_type)
{
    switch (bc_type) {
        case 'm':
        case 'M':// mirror
            return (pole / (pole * pole - 1.f)) * (pole * ck_p[data_len - 2]
                                                   + ck_p[data_len - 1]);

        case 'c':
        case 'C': // clamping
            return (pole / (pole - 1.0f)) * ck_p[data_len - 1];

        case 'z':
        case 'Z': // zero padding
            return (-pole * ck_p[data_len - 1]);

        default:
            printf("Wrong boundary condition anticausal\n");
            return init_vec_float<T>(0);
    }
}

template<typename T, InterpScheme scheme>
__host__ __device__
inline void samplesToCoeffs(T* ck, int len_data, char bc_type)
{
    const int deg = support(scheme) - 1;
    const int nb_poles = deg / 2;
    float poles[nb_poles] = {};

    switch (family(scheme)) {
        case BSPLINE:
            switch (deg) {
                case 3:
                    poles[0] = sqrtf(3.0f) - 2.0f;
                    break;
                case 4:
                    poles[0] = sqrtf(664.f - sqrtf(438976.f)) + sqrtf(304.f) -
                               19.f;
                    poles[1] = sqrtf(664.f + sqrtf(438976.f)) - sqrtf(304.f) -
                               19.f;
                    break;
                case 5:
                    poles[0] = sqrtf(135.f / 2.f - sqrtf(17745.f / 4.f)) +
                               sqrtf(105.f / 4.f) - 13.f / 2.f;
                    poles[1] = sqrtf(135.f / 2.f + sqrtf(17745.f / 4.f)) -
                               sqrtf(105.f / 4.f) - 13.f / 2.f;
                    break;
                default:
                    printf("Invalid spline degree\n");
                    break;
            }
            break;
        case OMOMS:
            switch (deg) {
                case 3:
                    poles[0] = 1.f / 8.f * (-13.f + sqrtf(105.f));
                    break;
                default:
                    printf("Invalid omoms degree\n");
                    break;
            }
    }

    // compute the overall gain
    float lambda = 1.f;
#pragma unroll
    for (int i = 0; i < nb_poles; ++i) {
        lambda *= (1.0f - poles[i]) * (1.0f - 1.0f / poles[i]);
    }

    // apply the gain
    for (int n = 0; n < len_data; n++) {
        ck[n] *= lambda;
    }

#pragma unroll
    for (int i = 0; i < nb_poles; ++i) {
        // causal initialization
        ck[0] = initCausalCoeff(ck, len_data, poles[i], bc_type);

        // causal recursion
        for (int n = 1; n < len_data; n++) {
            ck[n] += poles[i] * ck[n - 1];
        }

        // anticausal initialization
        ck[len_data - 1] = initAntiCausalCoeff(ck, len_data, poles[i], bc_type);

        // anticausal recursion
        for (int n = len_data - 2; 0 <= n; n--) {
            ck[n] = poles[i] * (ck[n + 1] - ck[n]);
        }
    }
}

} // end namespace pulse

#endif //PULSE_PREFILTER_INL
