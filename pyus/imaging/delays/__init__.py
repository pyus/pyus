from pyus.imaging.functors.delays import BaseDelay
from pyus.imaging.functors.delays import PlaneWave2D, PlaneWave3D
from pyus.imaging.functors.delays import DivergingWave as DivergingWave2D
from pyus.imaging.functors.delays import DivergingWave as DivergingWave3D
# TODO(@dperdios): other alias possibility
# from pyus.imaging.functors.delays import DivergingWave
# DivergingWave2D = DivergingWave
# DivergingWave3D = DivergingWave
