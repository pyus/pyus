from libcpp.string cimport string


cpdef magnitude_cpp(input, output):

    cdef:
        int size = input.size
        float complex[::1] in_view = input.ravel()
        float[::1] out_view = output.ravel()

    magnitude_host(&in_view[0], size, &out_view[0])

    return output


cpdef magnitude_cuda(input, output):

    cdef:
        int size = input.size
        size_t in_address = input.ptr
        size_t out_address = output.ptr
        complex[float]* in_ptr = <complex[float]*>in_address
        float* out_ptr = <float*>out_address

    magnitude_device(in_ptr, size, out_ptr)

    return output


cpdef env2bmode_cpp(envelope, str norm, float[::1] norm_factors, bmode):

    cdef:
        int nb_batches = envelope.shape[0]
        int len_env = envelope.size / nb_batches
        string norm_str = norm.encode('UTF-8')
        float[::1] env_view = envelope.ravel()
        float[::1] bmode_view = bmode.ravel()

    env2bmode_host(
        &env_view[0], len_env, nb_batches, norm_str, &norm_factors[0],
        &bmode_view[0]
    )

    return bmode


cpdef env2bmode_cuda(envelope, str norm, float[::1] norm_factors, bmode):

    cdef:
        int nb_batches = envelope.shape[0]
        int len_env = envelope.size / nb_batches
        string norm_str = norm.encode('UTF-8')
        size_t env_address = envelope.ptr
        size_t bmode_address = bmode.ptr
        float* env_ptr = <float*>env_address
        float* bmode_ptr = <float*>bmode_address

    env2bmode_device(
        env_ptr, len_env, nb_batches, norm_str, &norm_factors[0], bmode_ptr
    )

    return bmode


cpdef iq2bmode_cpp(iq_data, str norm, float[::1] norm_factors, bmode):

    cdef:
        int nb_batches = iq_data.shape[0]
        int len_data = iq_data.size / nb_batches
        string norm_str = norm.encode('UTF-8')
        float complex[::1] iq_view = iq_data.ravel()
        float[::1] bmode_view = bmode.ravel()

    iq2bmode_host(
        &iq_view[0], len_data, nb_batches, norm_str, &norm_factors[0],
        &bmode_view[0],
    )

    return bmode


cpdef iq2bmode_cuda(iq_data, str norm, float[::1] norm_factors, bmode):

    cdef:
        int nb_batches = iq_data.shape[0]
        int len_data = iq_data.size / nb_batches
        string norm_str = norm.encode('UTF-8')
        size_t iq_address = iq_data.ptr
        size_t bmode_address = bmode.ptr
        complex[float]* iq_ptr = <complex[float]*>iq_address
        float* bmode_ptr = <float*>bmode_address

    iq2bmode_device(
        iq_ptr, len_data, nb_batches, norm_str, &norm_factors[0], bmode_ptr
    )

    return bmode


cpdef rf2bmode_cpp(rf_data, fir_filter, str norm, float[::1] norm_factors, bmode):

    cdef:
        int nb_batches = rf_data.shape[0]
        int len_data = rf_data.size / nb_batches
        string norm_str = norm.encode('UTF-8')
        float[::1] rf_view = rf_data.ravel()
        float[::1] bmode_view = bmode.ravel()

    rf2bmode_host(
        &rf_view[0], len_data, nb_batches, fir_filter, norm_str,
        &norm_factors[0], &bmode_view[0],
    )

    return bmode


cpdef rf2bmode_cuda(rf_data, fir_filter, str norm, float[::1] norm_factors, bmode):

    cdef:
        int nb_batches = rf_data.shape[0]
        int len_data = rf_data.size / nb_batches
        string norm_str = norm.encode('UTF-8')
        size_t rf_address = rf_data.ptr
        size_t bmode_address = bmode.ptr
        float* rf_ptr = <float*>rf_address
        float* bmode_ptr = <float*>bmode_address

    rf2bmode_device(
        rf_ptr, len_data, nb_batches, fir_filter, norm_str, &norm_factors[0],
        bmode_ptr
    )

    return bmode