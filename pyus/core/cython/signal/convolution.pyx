import numpy as np
cimport numpy as np
cimport cython
from libcpp.string cimport string
from pyus.core.cython.utils.types cimport complex


cdef class _conv_direct_1D_host:
    def __cinit__(self):
        self._thisptr = new ConvolutionDirect1DHost()

    @cython.boundscheck(False)
    @cython.wraparound(False)
    cpdef call_convolve(self,
                        float_complex_cpu[:,::1] input,
                        float_complex_cpu[:,::1] filter,
                        float_complex_cpu[:,::1] output,
                        mode_str
    ):
        cdef:
            string mode = mode_str.lower().encode('UTF-8')
            int nb_inputs = input.shape[0]
            int nb_filters = filter.shape[0]
            int input_size = input.shape[1]
            int filter_size = filter.shape[1]

        if mode == b'full':
            self._thisptr[0].convolve_full(
                &input[0,0], input_size, nb_inputs,
                &filter[0,0], filter_size, nb_filters,
                &output[0,0]
            )
        elif mode == b'same':
            self._thisptr[0].convolve_same(
                &input[0,0], input_size, nb_inputs,
                &filter[0,0], filter_size, nb_filters,
                &output[0,0]
            )

        elif mode == b'valid':
            self._thisptr[0].convolve_valid(
                &input[0,0], input_size, nb_inputs,
                &filter[0,0], filter_size, nb_filters,
                &output[0,0]
            )
        else:
            raise ValueError('Invalide mode: {}'.format(mode))

        return np.asarray(output)


cdef class _conv_direct_1D_device:
    def __cinit__(self):
        self._thisptr = new ConvolutionDirect1DDevice()

    @cython.boundscheck(False)
    @cython.wraparound(False)
    cpdef call_convolve(self, input, filter, output, mode_str):
        cdef:
            string mode = mode_str.lower().encode('UTF-8')
            int nb_inputs = input.shape[0]
            int nb_filters = filter.shape[0]
            int input_size = input.shape[1]
            int filter_size = filter.shape[1]
            size_t input_address = input.ptr
            size_t filter_address = filter.ptr
            size_t output_address = output.ptr
            float* input_flt_ptr
            float* filter_flt_ptr
            float* output_flt_ptr
            complex[float]* input_cplx_ptr
            complex[float]* filter_cplx_ptr
            complex[float]* output_cplx_ptr

        if input.dtype == np.float32:
            input_flt_ptr = <float*>input_address
            filter_flt_ptr = <float*>filter_address
            output_flt_ptr = <float*>output_address

            if mode == b'full':
                self._thisptr[0].convolve_full(
                    input_flt_ptr, input_size, nb_inputs,
                    filter_flt_ptr, filter_size, nb_filters,
                    output_flt_ptr
                )
            elif mode == b'same':
                self._thisptr[0].convolve_same(
                    input_flt_ptr, input_size, nb_inputs,
                    filter_flt_ptr, filter_size, nb_filters,
                    output_flt_ptr
                )
            elif mode == b'valid':
                self._thisptr[0].convolve_valid(
                    input_flt_ptr, input_size, nb_inputs,
                    filter_flt_ptr, filter_size, nb_filters,
                    output_flt_ptr
                )
            else:
                raise ValueError('Invalid mode: {}'.format(mode))

        elif input.dtype == np.complex64:
            input_cplx_ptr = <complex[float]*>input_address
            filter_cplx_ptr = <complex[float]*>filter_address
            output_cplx_ptr = <complex[float]*>output_address

            if mode == b'full':
                self._thisptr[0].convolve_full(
                    input_cplx_ptr, input_size, nb_inputs,
                    filter_cplx_ptr, filter_size, nb_filters,
                    output_cplx_ptr
                )
            elif mode == b'same':
                self._thisptr[0].convolve_same(
                    input_cplx_ptr, input_size, nb_inputs,
                    filter_cplx_ptr, filter_size, nb_filters,
                    output_cplx_ptr
                )
            elif mode == b'valid':
                self._thisptr[0].convolve_valid(
                    input_cplx_ptr, input_size, nb_inputs,
                    filter_cplx_ptr, filter_size, nb_filters,
                    output_cplx_ptr
                )
            else:
                raise ValueError('Invalid mode: {}'.format(mode))

        else:
            raise TypeError('Unsupported type: {}'.format(input.dtype))

        return output
