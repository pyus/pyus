import numpy as np
cimport numpy as np


cdef class _hilbert_fft_host:

    def __cinit__(self, data_shape):
        len_signal = data_shape[-1]
        batch = np.prod(np.array(data_shape[0:-1]))
        self._thisptr = new HilbertFFTHost(len_signal, batch)

    cpdef call_analytic_signal(
            self,
            float[:,::1] input,
            float complex[:,::1] analytic_signal
    ):

        self._thisptr[0].analytic_signal(&input[0,0], <std_complex*>&analytic_signal[0,0])

        return np.asarray(analytic_signal)


cdef class _hilbert_fft_device:

    def __cinit__(self, data_shape):
        len_signal = data_shape[-1]
        batch = np.prod(np.array(data_shape[0:-1]))
        self._thisptr = new HilbertFFTDevice(len_signal, batch)

    cpdef call_analytic_signal(
            self,
            input,
            analytic_signal
    ):

        cdef:
            size_t input_address = input.ptr
            size_t sig_ana_address = analytic_signal.ptr
            float* input_ptr = <float*>input_address
            complex[float]* sig_ana_ptr = <complex[float]*>sig_ana_address

        self._thisptr[0].analytic_signal(input_ptr, sig_ana_ptr)

        return analytic_signal


cdef class _hilbert_fir_host:

    def __cinit__(self, data_shape, fir_filter):
        ndim = len(data_shape)
        len_signal = data_shape[-1]
        batch = np.prod(np.array(data_shape[0:-1])) if ndim >=2 else 1
        self._thisptr = new HilbertFIRHost(len_signal, batch, fir_filter)

    cpdef call_analytic_signal(
            self,
            float[:,::1] input,
            float complex[:,::1] analytic_signal
    ):

        self._thisptr[0].analytic_signal(&input[0,0], <std_complex*>&analytic_signal[0,0])

        return np.asarray(analytic_signal)


cdef class _hilbert_fir_device:

    def __cinit__(self, data_shape, fir_filter):
        ndim = len(data_shape)
        len_signal = data_shape[-1]
        batch = np.prod(np.array(data_shape[0:-1])) if ndim >=2 else 1
        self._thisptr = new HilbertFIRDevice(len_signal, batch, fir_filter)

    cpdef call_analytic_signal(
            self,
            input,
            analytic_signal):

        cdef:
            size_t input_address = input.ptr
            size_t sig_ana_address = analytic_signal.ptr
            float* input_ptr = <float*>input_address
            complex[float]* sig_ana_ptr = <complex[float]*>sig_ana_address

        self._thisptr[0].analytic_signal(input_ptr, sig_ana_ptr)

        return analytic_signal