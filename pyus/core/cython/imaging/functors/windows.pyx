# TODO(@flomz): clean code with class inheritance
#   Could have a common _Functor interface for the __call__ method

cdef class _Selfridge2D:
    def __cinit__(self, wavelength, el_width):
        self._thisptr = new Selfridge2D(wavelength, el_width)

    def __call__(self, float3 pos_im, float3 pos_td):
        return self._thisptr[0](pos_im, pos_td)


cdef class _Selfridge3D:
    def __cinit__(self, wavelength, el_width_x, el_width_y):
        self._thisptr = new Selfridge3D(wavelength, el_width_x, el_width_y)

    def __call__(self, float3 pos_im, float3 pos_td):
        return self._thisptr[0](pos_im, pos_td)


cdef class _Boxcar:
    def __cinit__(self, f_number):
        self._thisptr = new Boxcar(f_number)

    def __call__(self, float3 pos_im, float3 pos_td):
        return self._thisptr[0](pos_im, pos_td)


cdef class _Hanning:
    def __cinit__(self, f_number):
        self._thisptr = new Hanning(f_number)

    def __call__(self, float3 pos_im, float3 pos_td):
        return self._thisptr[0](pos_im, pos_td)


cdef class _Hamming:
    def __cinit__(self, f_number):
        self._thisptr = new Hamming(f_number)

    def __call__(self, float3 pos_im, float3 pos_td):
        return self._thisptr[0](pos_im, pos_td)


cdef class _Tukey:
    def __cinit__(self, alpha, f_number):
        self._thisptr = new Tukey(alpha, f_number)

    def __call__(self, float3 pos_im, float3 pos_td):
        return self._thisptr[0](pos_im, pos_td)


cdef class _IdentityWindow:
    def __cinit__(self):
        self._thisptr = new IdentityWindow()

    def __call__(self, float3 pos_im, float3 pos_td):
        return self._thisptr[0](pos_im, pos_td)
