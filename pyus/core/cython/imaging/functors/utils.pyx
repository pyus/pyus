import numpy as np
cimport numpy as np
cimport cython


@cython.boundscheck(False)
@cython.wraparound(False)
def apply_functor(im_pos not None: np.ndarray,
                  el_pos not None: np.ndarray,
                  binary_op not None):
    # TODO: test on input dimensions: im_pos(3,N) -> el_pos(3,1)
    #                                 im_pos(3,1) -> el_pos(3,N)
    """

    Parameters
    ----------
    im_pos : either a meshgrid (3, N) with N = xaxis.size * y_axis.size * zaxis.size
             or a position (3,) or (3,1)
    el_pos : either a position (3,) or (3,1) or a meshgrid (3, N) with
             N = xaxis.size * y_axis.size * zaxis.size
    binary_op : One of the extension types defined above, binding the window functors

    Returns
    -------
    output : 1D numpy array of size N
    """

    ndim_im = np.squeeze(im_pos).ndim
    ndim_el = np.squeeze(el_pos).ndim
    valid_dim = (ndim_im == 2 and ndim_el == 1) or (ndim_im == 1 and ndim_el == 2)
    # valid_dim = (ndim_im == 2 and ndim_el == 2) or (ndim_im == 1 and ndim_el == 1)

    if not valid_dim:
        raise ValueError('invalid input dimensions')

    if im_pos.shape[0] != el_pos.shape[0]:
        raise ValueError('invalid input dimensions')

    if ndim_im == 2: # im_pos is (3,N) and el_pos is (3,1)
        mg_pos = im_pos
        od_pos = el_pos
    else: # el_pos is (3,N) and im_pos is (3,1)
        mg_pos = el_pos
        od_pos = im_pos

    cdef:
        int size_out = mg_pos.shape[1]

        # Create memory views of inputs
        float[:,::1] mg_pos_view = mg_pos
        float[::1] od_pos_view = od_pos

        # Create output and its memory view
        output = np.zeros(size_out, np.float32)
        float[::1] result = output

        # Declare inputs to functor
        float3 im_f3
        float3 el_f3

        # Declare loop index for efficiency
        int i

    # Loop on each coordinates and call functor
    if ndim_im == 2:

        el_f3.x = od_pos_view[0]
        el_f3.y = od_pos_view[1]
        el_f3.z = od_pos_view[2]

        for i in range(size_out):

            im_f3.x = mg_pos_view[0,i]
            im_f3.y = mg_pos_view[1,i]
            im_f3.z = mg_pos_view[2,i]

            result[i] = binary_op(im_f3, el_f3)

    else:

        im_f3.x = od_pos_view[0]
        im_f3.y = od_pos_view[1]
        im_f3.z = od_pos_view[2]

        for i in range(size_out):

            el_f3.x = mg_pos_view[0,i]
            el_f3.y = mg_pos_view[1,i]
            el_f3.z = mg_pos_view[2,i]

            result[i] = binary_op(im_f3, el_f3)


    return output
