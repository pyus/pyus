# TODO(@flomz): clean code with class inheritance
#   Could have a common _Functor interface for the __call__ method

cdef class _PlaneWave2D:
    def __cinit__(self, mean_sound_speed, angle, offset):
        self._thisptr = new PlaneWave2D(mean_sound_speed, angle, offset)

    def __call__(self, float3 pos_im, float3 pos_td):
        return self._thisptr[0](pos_im, pos_td)

cdef class _PlaneWave3D:
    def __cinit__(self, mean_sound_speed, angle_x, angle_y, offset):
        self._thisptr = new PlaneWave3D(mean_sound_speed, angle_x, angle_y, offset)

    def __call__(self, float3 pos_im, float3 pos_td):
        return self._thisptr[0](pos_im, pos_td)

cdef class _DivergingWave2D:
    def __cinit__(self, mean_sound_speed, virtual_source, offset):
        cdef float3 _source
        _source.x = virtual_source[0]
        _source.y = virtual_source[1]
        _source.z = virtual_source[2]
        self._thisptr = new DivergingWave(mean_sound_speed, _source, float(offset))

    def __call__(self, float3 pos_im, float3 pos_td):
        return self._thisptr[0](pos_im, pos_td)
