import numpy as np
cimport numpy as np

from pyus.core.cython.imaging.interpolator.interpolator cimport (
    interp_type,
    Nearest, Linear, Keys, Bspline3, Bspline4, Bspline5, Omoms3,
    _Nearest, _Linear, _Keys, _Bspline3, _Bspline4, _Bspline5, _Omoms3,
)


###############################################################################
# C++
###############################################################################
# real data (float)
cpdef call_prefilter_host(float[:,:,:,::1] raw_data, boundary, interp_type interpolator):

    cdef:
        int nb_channels = raw_data.shape[0] * raw_data.shape[1] * raw_data.shape[2]
        int nb_samples = raw_data.shape[3]
        char bc = boundary.encode('UTF-8')[0]

    if interp_type is _Nearest:
        prefilterChannelHost[Nearest](
            &raw_data[0,0,0,0], nb_channels, nb_samples, bc
        )
    elif interp_type is _Linear:
        prefilterChannelHost[Linear](
            &raw_data[0,0,0,0], nb_channels, nb_samples, bc
        )
    elif interp_type is _Keys:
        prefilterChannelHost[Keys](
            &raw_data[0,0,0,0], nb_channels, nb_samples, bc
        )
    elif interp_type is _Bspline3:
        prefilterChannelHost[Bspline3](
            &raw_data[0,0,0,0], nb_channels, nb_samples, bc
        )
    elif interp_type is _Bspline4:
        prefilterChannelHost[Bspline4](
            &raw_data[0,0,0,0], nb_channels, nb_samples, bc
        )
    elif interp_type is _Bspline5:
        prefilterChannelHost[Bspline5](
            &raw_data[0,0,0,0], nb_channels, nb_samples, bc
        )
    elif interp_type is _Omoms3:
        prefilterChannelHost[Omoms3](
            &raw_data[0,0,0,0], nb_channels, nb_samples, bc
    )
    else:
        raise TypeError

    return np.asarray(raw_data)


# complex data
cpdef call_prefilter_iq_host(float complex[:,:,:,::1] raw_data, boundary, interp_type interpolator):

    cdef:
        int nb_channels = raw_data.shape[0] * raw_data.shape[1] * raw_data.shape[2]
        int nb_samples = raw_data.shape[3]
        char bc = boundary.encode('UTF-8')[0]

    if interp_type is _Nearest:
        prefilterChannelHost_c[Nearest](
            &raw_data[0,0,0,0], nb_channels, nb_samples, bc
        )
    elif interp_type is _Linear:
        prefilterChannelHost_c[Linear](
            &raw_data[0,0,0,0], nb_channels, nb_samples, bc
        )
    elif interp_type is _Keys:
        prefilterChannelHost_c[Keys](
            &raw_data[0,0,0,0], nb_channels, nb_samples, bc
        )
    elif interp_type is _Bspline3:
        prefilterChannelHost_c[Bspline3](
            &raw_data[0,0,0,0], nb_channels, nb_samples, bc
        )
    elif interp_type is _Bspline4:
        prefilterChannelHost_c[Bspline4](
            &raw_data[0,0,0,0], nb_channels, nb_samples, bc
        )
    elif interp_type is _Bspline5:
        prefilterChannelHost_c[Bspline5](
            &raw_data[0,0,0,0], nb_channels, nb_samples, bc
        )
    elif interp_type is _Omoms3:
        prefilterChannelHost_c[Omoms3](
            &raw_data[0,0,0,0], nb_channels, nb_samples, bc
    )
    else:
        raise TypeError

    return np.asarray(raw_data)

###############################################################################
# CUDA
###############################################################################
# real data (float)
cpdef call_prefilter_device(raw_data, boundary, interp_type interpolator):

    cdef:
        int nb_channels = raw_data.shape[0] * raw_data.shape[1] * raw_data.shape[2]
        int nb_samples = raw_data.shape[3]
        # Get GPU address and cast it to proper pointer type
        size_t raw_data_address = raw_data.ptr
        float* raw_data_ptr = <float*>raw_data_address
        char bc = boundary.encode('UTF-8')[0]

    if interp_type is _Nearest:
        prefilterChannelDevice[Nearest](
            raw_data_ptr, nb_channels, nb_samples, bc
        )
    elif interp_type is _Linear:
        prefilterChannelDevice[Linear](
            raw_data_ptr, nb_channels, nb_samples, bc
        )
    elif interp_type is _Keys:
        prefilterChannelDevice[Keys](
            raw_data_ptr, nb_channels, nb_samples, bc
        )
    elif interp_type is _Bspline3:
        prefilterChannelDevice[Bspline3](
            raw_data_ptr, nb_channels, nb_samples, bc
        )
    elif interp_type is _Bspline4:
        prefilterChannelDevice[Bspline4](
            raw_data_ptr, nb_channels, nb_samples, bc
        )
    elif interp_type is _Bspline5:
        prefilterChannelDevice[Bspline5](
            raw_data_ptr, nb_channels, nb_samples, bc
        )
    elif interp_type is _Omoms3:
        prefilterChannelDevice[Omoms3](
            raw_data_ptr, nb_channels, nb_samples, bc
    )
    else:
        raise TypeError

    return raw_data


# complex data (thrust::complex<float>)
cpdef call_prefilter_iq_device(raw_data, boundary, interp_type interpolator):

    cdef:
        int nb_channels = raw_data.shape[0] * raw_data.shape[1] * raw_data.shape[2]
        int nb_samples = raw_data.shape[3]
        # Get GPU address and cast it to proper pointer type
        size_t raw_data_address = raw_data.ptr
        complex[float]* raw_data_ptr = <complex[float]*>raw_data_address
        char bc = boundary.encode('UTF-8')[0]

    if interp_type is _Nearest:
        prefilterChannelDevice_c[Nearest](
            raw_data_ptr, nb_channels, nb_samples, bc
        )
    elif interp_type is _Linear:
        prefilterChannelDevice_c[Linear](
            raw_data_ptr, nb_channels, nb_samples, bc
        )
    elif interp_type is _Keys:
        prefilterChannelDevice_c[Keys](
            raw_data_ptr, nb_channels, nb_samples, bc
        )
    elif interp_type is _Bspline3:
        prefilterChannelDevice_c[Bspline3](
            raw_data_ptr, nb_channels, nb_samples, bc
        )
    elif interp_type is _Bspline4:
        prefilterChannelDevice_c[Bspline4](
            raw_data_ptr, nb_channels, nb_samples, bc
        )
    elif interp_type is _Bspline5:
        prefilterChannelDevice_c[Bspline5](
            raw_data_ptr, nb_channels, nb_samples, bc
        )
    elif interp_type is _Omoms3:
        prefilterChannelDevice_c[Omoms3](
            raw_data_ptr, nb_channels, nb_samples, bc
    )
    else:
        raise TypeError

    return raw_data
