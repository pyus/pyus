cdef class _Nearest:
    def __cinit__(self, first_sample, last_sample, nb_samples, sampling_freq):
        self._thisptr = new InterpFunctor[Nearest](first_sample, last_sample, nb_samples, sampling_freq)

cdef class _Linear:
    def __cinit__(self, first_sample, last_sample, nb_samples, sampling_freq):
        self._thisptr = new InterpFunctor[Linear](first_sample, last_sample, nb_samples, sampling_freq)

cdef class _Keys:
    def __cinit__(self, first_sample, last_sample, nb_samples, sampling_freq):
        self._thisptr = new InterpFunctor[Keys](first_sample, last_sample, nb_samples, sampling_freq)

cdef class _Bspline3:
    def __cinit__(self, first_sample, last_sample, nb_samples, sampling_freq):
        self._thisptr = new InterpFunctor[Bspline3](first_sample, last_sample, nb_samples, sampling_freq)

cdef class _Bspline4:
    def __cinit__(self, first_sample, last_sample, nb_samples, sampling_freq):
        self._thisptr = new InterpFunctor[Bspline4](first_sample, last_sample, nb_samples, sampling_freq)

cdef class _Bspline5:
    def __cinit__(self, first_sample, last_sample, nb_samples, sampling_freq):
        self._thisptr = new InterpFunctor[Bspline5](first_sample, last_sample, nb_samples, sampling_freq)

cdef class _Omoms3:
    def __cinit__(self, first_sample, last_sample, nb_samples, sampling_freq):
        self._thisptr = new InterpFunctor[Omoms3](first_sample, last_sample, nb_samples, sampling_freq)
