import numpy as np
cimport numpy as np
from libcpp.vector cimport vector
from pyus.core.cython.utils.types cimport complex
from cython.operator cimport dereference as deref
from pyus.core.cython.imaging.functors.delays cimport PlaneWave2D, PlaneWave3D, DivergingWave
from pyus.core.cython.imaging.functors.delays cimport _PlaneWave2D, _PlaneWave3D, _DivergingWave2D


cdef class _das_rf_host:

    def __cinit__(
            self, element_positions, t_axis, x_im_axis, y_im_axis, z_im_axis
    ):
        self._thisptr = new BeamformerHost(
            element_positions, t_axis, x_im_axis, y_im_axis, z_im_axis
        )
        self.size_image = (x_im_axis.size, y_im_axis.size, z_im_axis.size)

    @property
    def shape_image(self):
        return self.size_image

    # RF data
    cpdef call_reconstruct(
            self,
            float[:,:,:,::1] raw_data,
            delay_type dummy,
            delay_type[::1] delay,
            interp_type interp,
            window_type window,
            float[:,:,:,::1] rf_image
    ):
        """
        Reconstruct the RF image on the CPU. Takes as input a numpy array and 
        returns a numpy array
        """
        # Note: see below for more information on the need for a dummy
        # delay_type argument

        cdef:
            int frame_number = raw_data.shape[0]
            # float[:,:,:,::1] rf_image = np.zeros(
            #     (frame_number, *self.size_image), dtype=np.float32
            # )
            vector[PlaneWave2D] domains_pw2d
            vector[PlaneWave3D] domains_pw3d
            vector[DivergingWave] domains_dw
            int i

        # Normally, only one of these branches will be compiled for each
        # specialization.
        # That would work for cython types such as int, float, char etc.
        # Apparently it does not entirely work with our own extension type.
        # Indeed, cython is able to deduce the type and specialize the branches
        # if only one argument of the fused_type is provided, but not with a
        # typed memory view. Hence, we have to give a single delay_type argument
        # so its type is correctly deduced so that Cython specializes correctly
        # for our typed memoryview of delay_types, which it is unable to deduce
        # correctly
        if delay_type is _PlaneWave2D:
            for i in range(delay.size):
                domains_pw2d.push_back(deref(delay[i]._thisptr))

            self._thisptr[0].reconstruct(
                &raw_data[0,0,0,0],
                frame_number,
                domains_pw2d,
                deref(interp._thisptr),
                deref(window._thisptr),
                &rf_image[0,0,0,0]
            )
        elif delay_type is _PlaneWave3D:
            for i in range(delay.size):
                domains_pw3d.push_back(deref(delay[i]._thisptr))

            self._thisptr[0].reconstruct(
                &raw_data[0,0,0,0],
                frame_number,
                domains_pw3d,
                deref(interp._thisptr),
                deref(window._thisptr),
                &rf_image[0,0,0,0]
            )
        elif delay_type is _DivergingWave2D:
            for i in range(delay.size):
                domains_dw.push_back(deref(delay[i]._thisptr))

            self._thisptr[0].reconstruct(
                &raw_data[0,0,0,0],
                frame_number,
                domains_dw,
                deref(interp._thisptr),
                deref(window._thisptr),
                &rf_image[0,0,0,0]
            )
        else:
            raise NotImplementedError()

        # Create array from backend memory view
        # TODO(@dperdios/@flomz): np.asarray(_mem_view) -> pointer
        images = np.asarray(rf_image)  # pointer
        # images = np.array(_mem_view, copy=True)  # force a copy? -> why?

        return images

    # IQ data
    cpdef call_reconstruct_iq(
            self,
            float complex[:,:,:,::1] raw_data,
            delay_type dummy,
            delay_type[::1] delay,
            interp_type interp,
            window_type window,
            float complex[:,:,:,::1] rf_image
    ):
        """
        Reconstruct the IQ image on the CPU. Takes as input a numpy array and 
        returns a numpy array
        """
        # Note: see below for more information on the need for a dummy
        # delay_type argument

        cdef:
            int frame_number = raw_data.shape[0]
            # float complex[:,:,:,::1] rf_image = np.zeros(
            #     (frame_number, *self.size_image), dtype=np.complex64
            # )
            vector[PlaneWave2D] domains_pw2d
            vector[PlaneWave3D] domains_pw3d
            vector[DivergingWave] domains_dw
            int i

        # Normally, only one of these branches will be compiled for each
        # specialization.
        # That would work for cython types such as int, float, char etc.
        # Apparently it does not entirely work with our own extension type.
        # Indeed, cython is able to deduce the type and specialize the branches
        # if only one argument of the fused_type is provided, but not with a
        # typed memory view. Hence, we have to give a single delay_type argument
        # so its type is correctly deduced so that Cython specializes correctly
        # for our typed memoryview of delay_types, which it is unable to deduce
        # correctly
        if delay_type is _PlaneWave2D:
            for i in range(delay.size):
                domains_pw2d.push_back(deref(delay[i]._thisptr))

            self._thisptr[0].reconstruct(
                &raw_data[0,0,0,0],
                frame_number,
                domains_pw2d,
                deref(interp._thisptr),
                deref(window._thisptr),
                &rf_image[0,0,0,0]
            )
        elif delay_type is _PlaneWave3D:
            for i in range(delay.size):
                domains_pw3d.push_back(deref(delay[i]._thisptr))

            self._thisptr[0].reconstruct(
                &raw_data[0,0,0,0],
                frame_number,
                domains_pw3d,
                deref(interp._thisptr),
                deref(window._thisptr),
                &rf_image[0,0,0,0]
            )
        elif delay_type is _DivergingWave2D:
            for i in range(delay.size):
                domains_dw.push_back(deref(delay[i]._thisptr))

            self._thisptr[0].reconstruct(
                &raw_data[0,0,0,0],
                frame_number,
                domains_dw,
                deref(interp._thisptr),
                deref(window._thisptr),
                &rf_image[0,0,0,0]
            )
        else:
            raise NotImplementedError()

        # Create array from backend memory view
        # TODO(@dperdios/@flomz): np.asarray(_mem_view) -> pointer
        images = np.asarray(rf_image)  # pointer
        # images = np.array(_mem_view, copy=True)  # force a copy? -> @dperdios why?

        return images

cdef class _das_rf_device:

    def __cinit__(
            self, element_positions, t_axis, x_im_axis, y_im_axis, z_im_axis
    ):
        self._thisptr = new BeamformerDevice(
            element_positions, t_axis, x_im_axis, y_im_axis, z_im_axis
        )
        self.size_image = (x_im_axis.size, y_im_axis.size, z_im_axis.size)

    cpdef call_reconstruct(
            self,
            raw_data_gpu,
            delay_type dummy,
            delay_type[::1] delay,
            interp_type interp,
            window_type window,
            rf_image_gpu
    ):
        """
        Reconstruct the RF image on the GPU. Takes as input a GPUarray and 
        returns a GPUarray
        """

        cdef:
            int frame_number = raw_data_gpu.shape[0]
            vector[PlaneWave2D] domains_pw2d
            vector[PlaneWave3D] domains_pw3d
            vector[DivergingWave] domains_dw
            int i

            # Get GPU address and cast it to proper pointer type
            size_t raw_data_address = raw_data_gpu.ptr
            size_t rf_image_address = rf_image_gpu.ptr
            float* raw_data_ptr = <float*>raw_data_address
            float* rf_image_ptr = <float*>rf_image_address

        # Normally, only one of these branches will be compiled for each specialization
        if delay_type is _PlaneWave2D:
            for i in range(delay.size):
                domains_pw2d.push_back(deref(delay[i]._thisptr))

            self._thisptr[0].reconstruct(
                raw_data_ptr,
                frame_number,
                domains_pw2d,
                deref(interp._thisptr),
                deref(window._thisptr),
                rf_image_ptr
            )
        elif delay_type is _PlaneWave3D:
            for i in range(delay.size):
                domains_pw3d.push_back(deref(delay[i]._thisptr))

            self._thisptr[0].reconstruct(
                raw_data_ptr,
                frame_number,
                domains_pw3d,
                deref(interp._thisptr),
                deref(window._thisptr),
                rf_image_ptr
            )
        elif delay_type is _DivergingWave2D:
            for i in range(delay.size):
                domains_dw.push_back(deref(delay[i]._thisptr))

            self._thisptr[0].reconstruct(
                raw_data_ptr,
                frame_number,
                domains_dw,
                deref(interp._thisptr),
                deref(window._thisptr),
                rf_image_ptr
            )
        else:
            raise NotImplementedError()

        return rf_image_gpu


    cpdef call_reconstruct_iq(
            self,
            raw_data_gpu,
            delay_type dummy,
            delay_type[::1] delay,
            interp_type interp,
            window_type window,
            rf_image_gpu
    ):
        """
        Reconstruct the IQ image on the GPU. Takes as input a GPUarray and 
        returns a GPUarray
        """

        cdef:
            int frame_number = raw_data_gpu.shape[0]
            vector[PlaneWave2D] domains_pw2d
            vector[PlaneWave3D] domains_pw3d
            vector[DivergingWave] domains_dw
            int i

            # Get GPU address and cast it to proper pointer type
            size_t raw_data_address = raw_data_gpu.ptr
            size_t rf_image_address = rf_image_gpu.ptr
            complex[float]* raw_data_ptr = <complex[float]*>raw_data_address
            complex[float]* rf_image_ptr = <complex[float]*>rf_image_address

        # Normally, only one of these branches will be compiled for each specialization
        if delay_type is _PlaneWave2D:
            for i in range(delay.size):
                domains_pw2d.push_back(deref(delay[i]._thisptr))

            self._thisptr[0].reconstruct(
                raw_data_ptr,
                frame_number,
                domains_pw2d,
                deref(interp._thisptr),
                deref(window._thisptr),
                rf_image_ptr
            )
        elif delay_type is _PlaneWave3D:
            for i in range(delay.size):
                domains_pw3d.push_back(deref(delay[i]._thisptr))

            self._thisptr[0].reconstruct(
                raw_data_ptr,
                frame_number,
                domains_pw3d,
                deref(interp._thisptr),
                deref(window._thisptr),
                rf_image_ptr
            )
        elif delay_type is _DivergingWave2D:
            for i in range(delay.size):
                domains_dw.push_back(deref(delay[i]._thisptr))

            self._thisptr[0].reconstruct(
                raw_data_ptr,
                frame_number,
                domains_dw,
                deref(interp._thisptr),
                deref(window._thisptr),
                rf_image_ptr
            )
        else:
            raise NotImplementedError()

        return rf_image_gpu
